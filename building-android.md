# Building for use with Android

See below for a Docker-based build process. One can also inspect the contents of Dockerfile.android to better understand the build process.

1. Checkout branch `android`

2. Follow HPVM documentation to install all requirements.
   Not that CUDA enabled GPU is not required when building and using ApproxHPVM for compiling Android compatible binaries.
   However, the CUDA Toolkit is needed to complete the compilation of ApproxHPVM successfully.

3. It is recommended that you setup a virtual environment for Python 3.6 to use exclusively for ApproxHPVM.
   You can do that easily via [pyenv](https://github.com/pyenv/pyenv):
    ```
    pyenv virtualenv 3.6.13 hpvm
    pyenv activate hpvm
    ```

   Furthermore, you will likely have to update `pip` afterwards, otherwise the installation script might fail:
    ```
    python -m pip install --upgrade pip
    ```


4. Use script `hpvm/install-android.sh` to build ApproxHPVM for Android.
   Before running, make a local copy of `hpvm/env-android.example.sh` and set appropriate environment variables.
   This script also contains information how to setup additional requirements.

   **Make sure to manually build target `tensor_runtime` afterwards, otherwise ApproxHPVM compilation will not work.**

   For example, the whole build process could look like the following snippet:
    ```bash
    cd hpvm
    cp env-android.example.sh env-android.sh
    # ... Set environment variables within env-android.sh
    source env-android.sh
    ./install-android.sh DCMAKE_BUILD_TYPE="Release" --no-params
    cd path/to/build/dir && ninja tensor_runtime # Important!
    ```

    This script will also install `hpvm-profiler-android` into your current Python environment.

5. Use [hpvm-tuning-android (GitHub)](https://github.com/MatevzFa/hpvm-tuning-android) for profiling ApproxTuner configurations on Android devices.

# Docker

**Note: currently the Android App will fail to build if you install a NN into it with Docker -- the static libraries will include paths to shared objects within the Android NDK that resides in the container (linking will fail). A possible solution to this is extending the Docker image with the Android SDK and Gradle, which would allow producing installable APKs within the container.**

You can also use provided Dockerfile to build ApproxHPVM for Android completely independently from this repository. It will handle code download, building and installing all required dependencies.

The built Docker container can serve as a compiler for building and installing ApproxHPVM binaries into Android applications. However, tuning and profiling approximation configurations on Android devices is not available this way.

You can download a ZIP file containing the `model_params` directory with some trained neural networks (including `mobilenet_uci-har`) and an approximation configuration file for `mobilenet_uci-har` from [Google Drive](https://drive.google.com/uc?export=download&id=1xjU21SlOA2stzEY3KutHteUqRCwlOmC_).

```bash
docker build -f Dockerfile.android -t hpvm-android:1.0 .
docker run --rm -it --entrypoint bash \
   -v host/path/to/android-app-root:/var/android-app-root \
   -v host/path/to/model_params:/var/model_params \
   hpvm-android:1.0
```

Once the shell has started, run the following script. File `mobilenet_uci-har-configs.txt` must be in the container and must contain approximation configurations for `mobilenet_uci-har`.

```bash
# Build the Tensor Runtime and install it into the mounted Android application project
build-tensor-rt 
# Shell will start in hpvm-tuning-android directory
python install_x.py \
    -A /var/android-app-root \
    --abi $ANDROID_ABI \
    -J si.fri.matevzfa.approxhpvmdemo.har.ApproxHPVMWrapper \
    -o ./tmpdir \
    mobilenet_uci-har mobilenet_uci-har-configs.txt
```

The Dockerfile contains `ARG` directives which allow specifying the target Android architecture and API version. For further information, inspect the contents of the Dockerfile.
