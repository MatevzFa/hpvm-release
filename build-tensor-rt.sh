#!/usr/bin/env bash

mkdir -p $ANDROID_HPVM_TENSOR_RT_BUILD_DIR
cd $ANDROID_HPVM_TENSOR_RT_BUILD_DIR
$ANDROID_HPVM_TENSOR_RT_DIR/scripts/configure.sh -G Ninja $ANDROID_HPVM_TENSOR_RT_DIR
ninja install
