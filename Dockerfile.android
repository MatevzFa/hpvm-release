

FROM nvidia/cuda:10.2-cudnn7-devel-ubuntu18.04 as base

RUN apt update && \
    apt install -y build-essential python3 python3-pip python3-venv ninja-build curl git

# Required, because otherwise "python" and "pip" are unknonw commands.
RUN python3 -m venv /venv
ENV PATH="/venv/bin:$PATH"

# Upgrade pip
RUN pip install --upgrade pip

# Install CMake
RUN curl -L https://github.com/Kitware/CMake/releases/download/v3.20.0/cmake-3.20.0-linux-x86_64.sh -o cmake.sh && \
    bash ./cmake.sh --skip-license --prefix=/usr && \
    rm cmake.sh

RUN curl -L https://dl.google.com/android/repository/android-ndk-r21e-linux-x86_64.zip -o ndk.zip
RUN apt install -y zip unzip
RUN unzip ndk.zip && rm ndk.zip

#
#
#

FROM base

# Clone hpvm-release
#
ENV HPVM_ROOT="/apps/hpvm-release"
WORKDIR ${HPVM_ROOT}
RUN git clone --depth 1 -b android https://gitlab.com/MatevzFa/hpvm-release.git .
RUN git config submodule."hpvm/projects/predtuner".url https://gitlab.engr.illinois.edu/llvm/predtuner.git
RUN apt install --reinstall ca-certificates
RUN git submodule update --recursive

# The following variables can be overriden at build time
#
# The LLVM target triple to use when building.
# For more information see https://developer.android.com/ndk/guides/other_build_systems
ARG ANDROID_TARGET_TRIPLE="aarch64-linux-android"
# The Android ABI. Required for building hpvm-tensor-rt-android.
# For more information see https://developer.android.com/ndk/guides/other_build_systems
ARG ANDROID_ABI="arm64-v8a"
# The minSdkVersion from your Android project
ARG ANDROID_API_LEVEL="26"

ENV ANDROID_NDK="/android-ndk-r21e" \
    ANDROID_TARGET_TRIPLE="${ANDROID_TARGET_TRIPLE}" \
    ANDROID_ABI="${ANDROID_ABI}" \
    ANDROID_API_LEVEL="${ANDROID_API_LEVEL}"

# Build HPVM RT
#
ENV HPVM_RT_ROOT="/apps/hpvm-rt-android"
WORKDIR ${HPVM_RT_ROOT}
RUN echo
RUN git clone https://github.com/MatevzFa/hpvm-rt-android.git .
ENV TARGET="${ANDROID_TARGET_TRIPLE}${ANDROID_API_LEVEL}"
RUN HPVM_BUILD=unused LIB_INSTALL_PATH=unused INCLUDE_INSTALL_PATH=unused make
ENV ANDROID_HPVM_RT_PATH="/apps/hpvm-rt-android/build.${TARGET}/hpvm-rt.bc"

# Build ApproxHPVM
#
ARG JOBS=8

WORKDIR ${HPVM_ROOT}/hpvm
ENV CUDA_INCLUDE_PATH=/usr/local/cuda-10.2/include \
    CUDA_LIB_PATH=/usr/local/cuda-10.2/lib64/ \
    CUDA_BIN_PATH=/usr/local/cuda-10.2 \
    CUDNN_PATH=$CUDA_LIB_PATH \
    LIBRARY_PATH=$CUDA_LIB_PATH:$LIBRARY_PATH \
    LD_LIBRARY_PATH=$CUDA_LIB_PATH:$LD_LIBRARY_PATH
RUN ./install-android.sh DCMAKE_BUILD_TYPE="Release" -t "X86" --no-params --ninja -j${JOBS}
RUN cd build && ninja tensor_runtime

# Setup mount point for moutning an Android application project
#
VOLUME [ "/var/android-app-root" ]
ENV ANDROID_APP_ROOT="/var/android-app-root"


# Setup hpvm-tuning-android
#
ENV HPVM_TUNING_ANDROID_ROOT="/apps/hpvm-tuning-android"
WORKDIR ${HPVM_TUNING_ANDROID_ROOT}
RUN git clone https://github.com/MatevzFa/hpvm-tuning-android.git .

ENV GLOBAL_KNOBS_PATH="${HPVM_ROOT}/hpvm/projects/hpvm-tensor-rt/global_knobs.txt"

# Setup mount point for moutning an Android application project
#
VOLUME [ "/var/model_params" ]
ENV MODEL_PARAMS_DIR="/var/model_params"

ENV ANDROID_HPVM_TENSOR_RT_DIR=${HPVM_ROOT}/hpvm/projects/hpvm-tensor-rt-android
ENV ANDROID_HPVM_TENSOR_RT_BUILD_DIR=${ANDROID_HPVM_TENSOR_RT_DIR}/build
RUN cd ${ANDROID_HPVM_TENSOR_RT_DIR} && git submodule update --recursive --init

# Setup hpvm-tenosr-rt-android build script
#
COPY ./build-tensor-rt.sh /usr/bin/build-tensor-rt
RUN chmod +x /usr/bin/build-tensor-rt
