import setuptools

setuptools.setup(
    name="hpvm_profiler_android",
    version="0.1",
    author="Matevž Fabjančič, Akash Kothari, Yifan Zhao",
    author_email="matevz.fabjancic@student.uni-lj.si, akashk4@illinois.edu, yifanz16@illinois.edu",
    description="A package for profiling of HPVM approximation configurations on Android devices via adb",
    packages=["hpvm_profiler_android"],
    install_requires=["numpy>=1.19", "matplotlib>=3"],
)
