from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple, Union

import jinja2

from .graph_builder import DFG
from .codegen_hpvm import CodeGen, PathLike
from .graph_ir import DFGNode, TensorNode, WeightTensor

PLAIN_TEMPLATE_FILE = "template_android.cpp.in"
loader = jinja2.FileSystemLoader(searchpath=Path(__file__).parent)
template_env = jinja2.Environment(loader=loader, trim_blocks=True)

PathLike = Union[str, Path]


class AndroidCodeGen(CodeGen):
    # Variable indicator is always int for hpvm gen
    variables: Dict[DFGNode, Tuple[int, bool]]

    def __init__(
        self,
        dfg: DFG,
        prefix: PathLike,
        input_size: int,
        target: str,
    ):
        super().__init__(dfg, prefix, input_size)
        if target not in ("tensor"):
            raise ValueError(f"Unsupported target {target}")
        self.target = target
        self.template = template_env.get_template(PLAIN_TEMPLATE_FILE)

    def _emit_hpvm_node_edges(self, input_vars: List[DFGNode]) -> List[dict]:
        ret = []
        it = 0
        for node in input_vars:
            hpvm_var_idx, is_root_input = self.variables[node]
            if is_root_input:
                assert isinstance(hpvm_var_idx, int)
                ret.append(
                    {"is_bindin": True, "input_idx": hpvm_var_idx, "edge_idx": it}
                )
            else:
                ret.append(
                    {"is_bindin": False, "input_node": hpvm_var_idx, "edge_idx": it}
                )
            it += 1
        return ret

    def emit_hpvm_node_structures(self) -> List[dict]:
        node_envs = []
        for node in self.dfg.traverse_order:
            if isinstance(node, TensorNode):
                continue
            inputs = self.dfg.node_args(node)
            func_name, extra_args = node.hpvm_codegen()
            if func_name == "":  # No code generation
                # Node must have single input, we equate the output to
                # the input and skip code generation.
                assert len(inputs) == 1
                self.variables[node] = self.variables[inputs[0]]
                continue
            var_idx = self._inc_var_count()
            self.variables[node] = var_idx, False  # not root-node arg
            node_envs.append(
                {
                    "idx": var_idx,
                    "input_size": len(inputs),
                    "edges": self._emit_hpvm_node_edges(inputs),
                    "call_name": func_name,
                    "call_args": extra_args,
                }
            )
        return node_envs

    def emit_root_io(self) -> Tuple[List[str], int]:
        input_args = [
            self.make_c_identifier(node.name)
            for node, (_, is_root) in self.variables.items()
            if is_root
        ]
        output_var_idx = self.variables[self.dfg.output][0]
        return input_args, output_var_idx

    def compile(self, output: PathLike, java_package: str) -> None:

        parts = java_package.split(".")
        jni_function_basename = "Java_" + ("_".join(parts)) + "_"
        jni_function_names = {
            "jniFunctionHpvmInit": jni_function_basename + "hpvmInit",
            "jniFunctionHpvmCleanup": jni_function_basename + "hpvmCleanup",
            "jniFunctionHpvmInference": jni_function_basename + "hpvmInference",
            "jniFunctionHpvmApproximateMore": jni_function_basename + "hpvmAdaptiveApproximateMore",
            "jniFunctionHpvmApproximateLess": jni_function_basename + "hpvmAdaptiveApproximateLess",
            "jniFunctionHpvmAdaptiveGetConfigIndex": jni_function_basename + "hpvmAdaptiveGetConfigIndex",
            "jniFunctionHpvmAdaptiveSetConfigIndex": jni_function_basename + "hpvmAdaptiveSetConfigIndex",
        }

        nodes = self.emit_hpvm_node_structures()
        inputs, output_var_idx = self.emit_root_io()
        weights = self.emit_weights(self.weights)
        with Path(output).open("w") as f:
            f.write(
                self.template.render(
                    nodes=nodes,
                    input_name=self.input_name,
                    input_shape=self.input_shape,
                    root_inputs=inputs,
                    root_output_idx=output_var_idx,
                    weights=weights,
                    prefix=self.prefix,
                    target=self.target,
                    **jni_function_names
                )
            )
