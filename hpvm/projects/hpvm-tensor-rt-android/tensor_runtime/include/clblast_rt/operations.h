#ifndef __CLBLAST_RT_OPERATIONS_H__
#define __CLBLAST_RT_OPERATIONS_H__

#include "clblast_rt/op_add_bias.h"
#include "clblast_rt/op_convolution.h"
#include "clblast_rt/op_convolution_approx.h"
#include "clblast_rt/op_gemm.h"
#include "clblast_rt/op_pooling.h"

#endif // __CLBLAST_RT_OPERATIONS_H__
