#ifndef __CLBLAST_RT_OP_CONVOLUTION_H__
#define __CLBLAST_RT_OP_CONVOLUTION_H__

#include "clblast_rt/tensor.h"

#include "CL/cl.h"

namespace clblast_rt {

void convolution(cl_command_queue &queue, Tensor &image, Tensor &filter,
                 Tensor &output, cl_mem workspace, size_t stride_w,
                 size_t stride_h, size_t pad_w, size_t pad_h,
                 size_t *workspace_size_ret = nullptr,
                 cl_event *e_im2col = nullptr, cl_event *e_gemm = nullptr);

void convolution_half(cl_command_queue &queue, Tensor &image, Tensor &filter,
                      Tensor &output, cl_mem workspace, size_t stride_w,
                      size_t stride_h, size_t pad_w, size_t pad_h,
                      size_t *workspace_size_ret = nullptr,
                      cl_event *e_im2col = nullptr, cl_event *e_gemm = nullptr);

} // namespace clblast_rt

#endif // __CLBLAST_RT_OP_CONVOLUTION_H__
