#ifndef __CLBLAST_RT_OP_POOLING_H__
#define __CLBLAST_RT_OP_POOLING_H__

#include "clblast_rt/tensor.h"

#include "CL/cl.h"

namespace clblast_rt {

enum class PoolMode {
  AVG,
  MIN,
  MAX,
};

void pooling(cl_command_queue queue, PoolMode mode, Tensor &in, Tensor &out, const size_t pool_h,
             const size_t pool_w, const size_t pad_h, const size_t pad_w, const size_t stride_h,
             const size_t stride_w, cl_event *event = nullptr);

void pooling_half(cl_command_queue queue, PoolMode mode, Tensor &in, Tensor &out,
                  const size_t pool_h, const size_t pool_w, const size_t pad_h, const size_t pad_w,
                  const size_t stride_h, const size_t stride_w, cl_event *event = nullptr);

}  // namespace clblast_rt

#endif  // __CLBLAST_RT_OP_POOLING_H__
