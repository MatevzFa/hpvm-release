#ifndef __UTILITY_H__
#define __UTILITY_H__

#include <ostream>
#include <vector>

#include "clblast.h"

#include "CL/cl.h"
#include "CL/cl_platform.h"
#include "clblast_rt/tensor.h"

namespace clblast_rt {

void print(const std::string &name, std::ostream &out, cl_command_queue queue,
           const DataType &dtype, const Shape &shape, cl_mem buf, size_t offset,
           size_t max_dim = 16);

void clChk(cl_int status);

void blastChk(const clblast::StatusCode &status);

size_t data_type_element_size(const DataType &dt);

size_t perf_index_restore(size_t idx, size_t perf_offset, size_t perf_stride);

size_t perf_out_size(size_t dim_size, size_t perf_offset, size_t perf_stride);

size_t conv_size(size_t size, size_t kernel_size, size_t pad_both,
                 size_t stride);

} // namespace clblast_rt

#endif // __UTILITY_H__
