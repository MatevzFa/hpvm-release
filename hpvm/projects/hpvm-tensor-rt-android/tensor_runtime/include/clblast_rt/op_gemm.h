#ifndef __OP_GEMM_H__
#define __OP_GEMM_H__

#include "clblast_rt/tensor.h"

#include "CL/cl.h"

namespace clblast_rt {

void gemm(cl_command_queue &queue, Tensor &a, Tensor &b, Tensor &output);

void gemm_half(cl_command_queue &queue, Tensor &a, Tensor &b, Tensor &output);

} // namespace clblast_rt

#endif // __OP_GEMM_H__
