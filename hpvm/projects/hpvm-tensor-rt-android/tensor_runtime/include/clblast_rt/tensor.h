

#ifndef __CLBLAST_RT_TENSOR_H__
#define __CLBLAST_RT_TENSOR_H__

#include <cstdint>
#include <new>
#include <stdexcept>
#include <array>

#define CL_TARGET_OPENCL_VERSION 120
#define CL_USE_DEPRECATED_OPENCL_1_1_APIS // to disable deprecation warnings
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS // to disable deprecation warnings
#define CL_USE_DEPRECATED_OPENCL_2_0_APIS // to disable deprecation warnings
#include "CL/cl.h"
#include "CL/cl_platform.h"

namespace clblast_rt {

class Shape {
public:
  Shape(size_t n, size_t c, size_t h, size_t w);

  size_t area();

  inline size_t n() const { return _n; };
  inline size_t c() const { return _c; };
  inline size_t h() const { return _h; };
  inline size_t w() const { return _w; };

private:
  size_t _n, _c, _h, _w;
};

enum class DataType {
  FP16,
  FP32,
  I32,
  U32,
};

enum class DataPlacement {
  DEV,
  DEV_HALF,
  HOST,
  UNALLOCATED,
};

class BufferDev {
public:
  BufferDev(size_t size);

  inline size_t size() { return _size; }

  void allocate(cl_context context, cl_mem_flags flags = CL_MEM_READ_WRITE);

  void release();

  void clear(cl_command_queue queue);

  bool is_allocated();

  cl_mem raw();

private:
  size_t _size;
  cl_mem _buf;
};

class BufferHost {
public:
  BufferHost(size_t size);

  inline size_t size() { return _size; }

  void allocate();

  void release();

  void clear();

  bool is_allocated();

  char *raw();

private:
  size_t _size;
  char *_buf;
};

class Tensor {
public:
  Tensor(DataType type, size_t n, size_t c, size_t h, size_t w);
  Tensor(DataType type, Shape shape);
  Tensor(DataType type, DataType type_half, Shape shape);

  ~Tensor();

  inline void set_data_placement(const DataPlacement &placement) {
    _placement = placement;
  }

  inline bool is_full_used() { return _placement == DataPlacement::DEV; }

  inline bool is_half_used() { return _placement == DataPlacement::DEV_HALF; }

  inline bool is_host_used() { return _placement == DataPlacement::HOST; }

  Shape &shape();

  DataType type() { return _type; }

  DataType type_half() { return DataType::FP16; }

  //
  //
  // Buffer access
  //

  BufferDev &buf_dev();

  BufferDev &buf_dev_half();

  BufferHost &buf_host();

  //
  //
  // Data movement
  //

private:
  Shape _shape;
  DataPlacement _placement;

  DataType _type;
  DataType _type_half;

  BufferDev _buf_dev;
  BufferDev _buf_dev_half;

  BufferHost _buf_host;

public:
  // For compatibility
  struct _dims {
    std::array<size_t, 4> dim_sizes;
    int num_dims;

    _dims(const Shape &shape)
        : dim_sizes({shape.n(), shape.c(), shape.h(), shape.w()}), num_dims(4) {
    }
  };

  const _dims dims;
  const size_t num_elems;
};

} // namespace clblast_rt

#endif // __CLBLAST_RT_TENSOR_H__
