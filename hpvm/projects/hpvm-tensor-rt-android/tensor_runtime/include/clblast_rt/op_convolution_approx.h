#ifndef __CLBLAST_RT_OP_CONVOLUTION_APPROX_H__
#define __CLBLAST_RT_OP_CONVOLUTION_APPROX_H__

#include "clblast_rt/tensor.h"

#include "CL/cl.h"

namespace clblast_rt {

enum class ApproxMode {
  PERF_ROW,
  PERF_COL,
  SAMP_KERNEL,
};

struct ApproxInfo {
  ApproxMode mode;
  size_t perf_offset;
  size_t perf_stride;

  ApproxInfo(ApproxMode _mode, size_t _perf_offset, size_t _perf_stride)
      : mode(_mode), perf_offset(_perf_offset), perf_stride(_perf_stride) {}
};

void convolution_approx(cl_command_queue &queue, Tensor &image, Tensor &filter,
                        Tensor &output, cl_mem workspace, size_t stride_w,
                        size_t stride_h, size_t pad_w, size_t pad_h,
                        const ApproxInfo &approx_info,
                        size_t *workspace_size_ret = nullptr,
                        cl_event *e_kn2row = nullptr,
                        cl_event *e_im2col = nullptr,
                        cl_event *e_gemm = nullptr,
                        cl_event *e_interpolate = nullptr);

void convolution_approx_half(cl_command_queue &queue, Tensor &image,
                             Tensor &filter, Tensor &output, cl_mem workspace,
                             size_t stride_w, size_t stride_h, size_t pad_w,
                             size_t pad_h, const ApproxInfo &approx_info,
                             size_t *workspace_size_ret = nullptr,
                             cl_event *e_kn2row = nullptr,
                             cl_event *e_im2col = nullptr,
                             cl_event *e_gemm = nullptr,
                             cl_event *e_interpolate = nullptr);

} // namespace clblast_rt

#endif // __CLBLAST_RT_OP_CONVOLUTION_APPROX_H__
