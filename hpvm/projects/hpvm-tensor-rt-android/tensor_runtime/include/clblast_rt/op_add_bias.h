#ifndef __CLBLAST_RT_OP_ADD_BIAS_H__
#define __CLBLAST_RT_OP_ADD_BIAS_H__

#include "clblast_rt/tensor.h"

#include "CL/cl.h"

namespace clblast_rt {

void add_bias(cl_command_queue queue, Tensor &x, Tensor &bias);

void add_bias_half(cl_command_queue queue, Tensor &x, Tensor &bias);

} // namespace clblast_rt

#endif // __CLBLAST_RT_OP_ADD_BIAS_H__
