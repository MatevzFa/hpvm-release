#ifndef ARM_RT_RT_CONTEXT_H
#define ARM_RT_RT_CONTEXT_H

#include <cstddef>
#include <memory>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#define CL_TARGET_OPENCL_VERSION 120
#define CL_USE_DEPRECATED_OPENCL_1_1_APIS // to disable deprecation warnings
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS // to disable deprecation warnings
#define CL_USE_DEPRECATED_OPENCL_2_0_APIS // to disable deprecation warnings
#include "CL/cl.h"
#include "approx_knob_utils.h"

#include "RtController.h"
#include "RtProfiler.h"
#include "android_util.h"
#include "stackofstacks.hpp"
#include "clblast_rt/tensor.h"
#include "tensor_runtime.h"

class RtContext {
public:
  RtContext();

  ~RtContext();

  void flag_as_android_app();

  inline cl_context &context() { return _context; }
  inline cl_command_queue &queue() { return _queue; }

  void track_tensor(clblast_rt::Tensor *arm_tensor);
  void tracking_scope_in();
  void tracking_scope_out();

  RtProfiler &profiler();

  ProfilerScope profile(RtProfiler::EventID &event);

  void profile_completed(RtProfiler::EventID &event, unsigned long start_ms,
                         unsigned long end_ms);

  RtController &rtController();

  int get_currentTensorID() { return currentTensorID; }

  void set_currentTensorID(unsigned int id) { currentTensorID = id; }

  bool is_android_app();

private:
  // Android
  bool _is_android_app = false;
  // OpenCL
  cl_context _context;
  cl_command_queue _queue;

  bool runtime_initialized;
  bool approxhpvm_runtime_mode;
  int op_counter;
  int total_ops;
  std::vector<int> op_accuracies;
  stackofstacks::Stack<clblast_rt::Tensor *> _armtensors;
  std::string profile_data;
  unsigned int currentTensorID;

  RtProfiler _profiler;
  RtController _controller;
};

#endif
