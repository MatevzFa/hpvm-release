#ifndef __ARM_RT_DEBUG_H__
#define __ARM_RT_DEBUG_H__

#include "android_util.h"

#define INFO(...) LOGI(__VA_ARGS__)
#define DEBUG(...) LOGD(__VA_ARGS__)
#define ERROR(...) LOGE(__VA_ARGS__)

#define CUSTOM_ASSERT(arg)                                                     \
  do {                                                                         \
    if (!(arg)) {                                                              \
      LOGE(#arg);                                                              \
      throw std::runtime_error(#arg);                                          \
    }                                                                          \
  } while (false)

#endif // __ARM_RT_DEBUG_H__
