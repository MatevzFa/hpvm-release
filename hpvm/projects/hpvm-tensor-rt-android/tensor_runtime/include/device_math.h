/*
device_math.h
Provides pointer to CUDA math function and other properties ofa math operator
(one among MathOp) on a certain scalar type.
*/
#ifndef ARM_RT_DEVICE_MATH_H
#define ARM_RT_DEVICE_MATH_H

#include "android_util.h"

#include <limits>
#include <stdexcept>

enum class MathOp {
  Hypot,
  Atan2,
  Add,
  Sub,
  Mul,
  Div,
  Sqrt,
  Max,
  Min,
  Avg3,
  Blend2,
  AddWeighted,
  PSNR
};

// Find the CUDA function for math operator `op`.
// This is ONLY defined (through template specialization, in device_math.cu) for
// float and half (see below).
template <typename T> void *mathOpToFunc(MathOp op);

template <> void *mathOpToFunc<float>(MathOp op);

// Returns the identity element of math operator `op`, for example, -inf for
// MAX, 0 for ADD.
// Specialization exists for half type.
template <typename T> T reduceOpToIdentity(MathOp op) {
  switch (op) {
  case MathOp::Hypot:
    return T(0.0f);
  case MathOp::Add:
    return T(0.0f);
  case MathOp::Max:
    return -std::numeric_limits<T>::max();
  case MathOp::Min:
    return std::numeric_limits<T>::max();
  default:
    LOGE("device_math", "Operator does not have id value\n");
  }
  return T(); // For some compilers
}

#endif
