#ifndef __ARM_RT_RTPROFILER_H__
#define __ARM_RT_RTPROFILER_H__

#include <chrono>
#include <map>
#include <ostream>
#include <string>

#define CL_TARGET_OPENCL_VERSION 120
#define CL_USE_DEPRECATED_OPENCL_1_1_APIS // to disable deprecation warnings
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS // to disable deprecation warnings
#define CL_USE_DEPRECATED_OPENCL_2_0_APIS // to disable deprecation warnings
#include "CL/cl.h"

#include "android_util.h"

class RtProfiler {
public:
  using EventID = const std::string;
  using Time = std::chrono::milliseconds;

  ~RtProfiler() { LOGD("*** Destroying RtProfiler *** "); }
  class Event {
  public:
    Event();
    void start(Time at_time);
    void end(Time at_time);

  private:
    void update(Time duration);

  public:
    Time last_start;

    size_t count;
    Time total;
    Time min;
    Time max;
  };

  void start(const EventID &event);
  void end(const EventID &event);
  Time now();

  size_t event_duration(const EventID &event) {
    return _events[event].total.count();
  }

  void push(RtProfiler::EventID &event, long start_ms, long end_ms);

  void push_cl_event(const std::string &name, cl_event event);

  void generate_report(std::ostream &out);
  void generate_report_csv(std::ostream &out);
  void generate_report_profiling();

private:
  RtProfiler::Time event_total(const EventID &event_id);

  std::map<EventID, Event> _events;
};

class ProfilerScope {
public:
  ProfilerScope(RtProfiler &profiler, RtProfiler::EventID event)
      : _profiler(profiler), _event(event), _ended(false) {
    _profiler.start(_event);
  }

  void end() {
    if (!_ended) {
      _profiler.end(_event);
      _ended = true;
    }
  }

  ~ProfilerScope() { end(); }

private:
  RtProfiler &_profiler;
  RtProfiler::EventID _event;
  bool _ended;
};

#endif // __ARM_RT_RTPROFILER_H__
