#ifndef __HELPERS_H__
#define __HELPERS_H__

#include "clblast.h"
#include "clblast_half.h"

#include "clblast_rt/tensor.h"
#include "clblast_rt/operations.h"

//
//
//
// Helpers

clblast_rt::DataType data_type_from_internal(int data_type);

clblast_rt::PoolMode pool_mode_from_internal(int pool_mode);

clblast_rt::ApproxInfo approx_mode_from_internal(int row, int col,
                                                 int skip_every, int offset);

/**
 * This function probably isn't needed, since ARM's OpenCL queues beave in
 * FIFO order -- kernels get executed in the same order they were enqueued.
 */
void sync();

/**
 * When create_tensor is called with dtype FP16, the tensor object will have
 * the following types:
 *
 * dev_buf      : FP32
 * dev_buf_half : FP16
 * host_buf     : FP32
 */
clblast_rt::Tensor *create_tensor(const clblast_rt::DataType &dtype,
                                  const clblast_rt::Shape &shape,
                                  const clblast_rt::DataPlacement &placement);

/**
 * If the tensor pointed to by tensor_ptr is placed on DataPlacement::DEV,
 * this operation moves it to DataPlacement::DEV_HALF.
 *
 * Otherwise this operation has no effect.
 */
void full2half(void *tensor_ptr);

/**
 * If the tensor pointed to by tensor_ptr is placed on DataPlacement::DEV_HALF,
 * this operation moves it to DataPlacement::DEV.
 *
 * Otherwise this operation has no effect.
 */
void half2full(void *tensor_ptr);

/**
 * If the tensor pointed to by tensor_ptr is placed on DataPlacement::DEV,
 * this operation moves to DataPlacement::HOST.
 *
 * Otherwise this operation has no effect.
 */
void dev2host(void *tensor_ptr);

/**
 * If the tensor pointed to by tensor_ptr is placed on DataPlacement::HOST,
 * this operation moves to DataPlacement::DEV.
 *
 * Otherwise this operation has no effect.
 */
void host2dev(void *tensor_ptr);

// end Helpers
//
//
//

bool compare_tensors(void *a_ptr, void *b_ptr, float eps);

#endif // __HELPERS_H__
