#ifndef __RTCONTROLLER_H__
#define __RTCONTROLLER_H__

#include "android_util.h"
#include "approx_knob_utils.h"
#include "configuration.h"

#include <android/asset_manager.h>

#include <algorithm>
#include <cstdint>
#include <fstream>
#include <ios>
#include <iterator>
#include <vector>

class ConfidenceTracker {
public:
  ConfidenceTracker();

  void reset();

  void push_correct(float confidence);

  void push_total(float confidence);

  size_t n_correct();
  size_t n_wrong();
  size_t n_all();

  float avg_correct();

  float avg_wrong();

  float avg_all();

private:
  float _sum_correct;
  size_t _count_correct;

  float _sum_all;
  size_t _count_all;
};

class AccuracyTracker {
public:
  AccuracyTracker();

  void reset();

  void add_prediction(bool correct);

  float accuracy();

private:
  size_t _count_correct;
  size_t _count_all;
};

class RtController {
public:
  typedef int32_t LabelType;

  RtController();
  ~RtController();

  void loadLabels(AAssetManager *mgr, const std::string &path,
                  size_t num_labels);
  void loadLabels(const std::string &path, size_t num_labels);

  void loadTunerConfs(AAssetManager *mgr, const std::string &path);
  void loadTunerConfs(const std::string &path);

  void approximateMore();
  void approximateLess();

  /**
   * Change runtime parameters (e.g. configuration) based on results

   * @param result      The softmax tensor
   * @param labels_path Path to binary file with labels
   * @param start       Batch start index
   * @param end         Batch end index
   */
  void invokeRtControl(void *result, long start, long end);

  /**
   *
   */
  bool predict_correctness(LabelType label, float confidence);

  /**
   * Set the index of ApproxHPVM configuration to use
   */
  void setConfigIndex(size_t idx);

  size_t getConfigIndex();

  const Configuration &getConfig(size_t idx);

  const Configuration &getCurrentConfig();

  /**
   * Reset correct and total prediction counters
   */
  void reset();

  NodeConfiguration *getNodeConf(const char *node_id);

  PerfParamSet perfParams;
  SampParamSet sampParams;

  void generate_report_accuracy();

  void generate_report_confidence();

  //
  //
  // Classification logging
  //

  /**
   * Initializes logging classification results to file 'model_log' in cwd.
   * Opens the file in append mode.
   */
  void model_logger_init();

  /**
   * Closes the log file.
   */
  void model_logger_cleanup();

  /**
   * Append classification result into the log file:
   * - true label
   * - predicted label
   * - softmax confidence for each class
   */
  void model_log(RtController::LabelType true_label,
                 RtController::LabelType predicted_label,
                 const std::vector<float> &confidence);

private:
  void parse_config(std::istream &istream);

private:
  std::vector<LabelType> _labels;

  size_t _configuration_idx = 0;

  AccuracyTracker _accuracy;
  std::vector<AccuracyTracker> _perclass_accuracy;

  AccuracyTracker _correctness_predictor_accuracy;
  AccuracyTracker _correctness_predictor_accuracy_true;
  AccuracyTracker _correctness_predictor_accuracy_false;
  std::vector<AccuracyTracker> _correctness_predictor_perclass_accuracy;
  std::vector<AccuracyTracker> _correctness_predictor_perclass_accuracy_true;
  std::vector<AccuracyTracker> _correctness_predictor_perclass_accuracy_false;

  ConfidenceTracker _confidence_tracker;
  std::vector<ConfidenceTracker> _perclass_confidence_trackers;

  std::vector<Configuration> _InitialConfigurations;

  std::ofstream _model_log;
};

#endif // __RTCONTROLLER_H__
