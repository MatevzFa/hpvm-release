#ifndef ARM_RT_CONSTANTS_H
#define ARM_RT_CONSTANTS_H

/** Pooling */
#define POOLING_MAX 0
#define POOLING_AVG 1
#define POOLING_MIN 2

/** Data types */
#define TENSOR_TYPE_FLOAT 0
#define TENSOR_TYPE_DOUBLE 1
#define TENSOR_TYPE_HALF 2
#define TENSOR_TYPE_INT 3
#define TENSOR_TYPE_FLOAT2 4
#define TENSOR_TYPE_HALF2 5

/** Data Layouts */
#define TENSOR_LAYOUT_NCHW 0
// #define TENSOR_LAYOUT_NHWC 1

#endif