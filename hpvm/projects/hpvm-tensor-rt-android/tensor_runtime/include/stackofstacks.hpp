#pragma once

#include <iterator>
#include <stdexcept>
#include <vector>

namespace stackofstacks {

template <typename T> class Stack {

public:
  typedef typename std::vector<T>::iterator iterator;

  class StackIter {
  public:
    StackIter(iterator begin, iterator end) : _begin(begin), _end(end) {}
    iterator begin() {
      return _begin;
    }
    iterator end() {
      return _end;
    }

  private:
    iterator _begin;
    iterator _end;
  };

  class StackScopedResource {
  public:
    StackScopedResource(Stack<T> &stack) : _stack(stack) {
      _stack.scope_in();
    }
    ~StackScopedResource() {
      _stack.scope_out();
    }

  private:
    Stack<T> &_stack;
  };

  /**
   * Push an element to the topmost stack
   *
   * Throws std::runtime_error if no stack has been inititialized via
   * stackofstacks::Stack::scope_in().
   */
  void push(T obj) {
    if (_stack_starts.empty()) {
      std::runtime_error("stackofstacks::Stack::push() with no initialized "
                         "scope (via scope_in())");
    }
    _stacks.push_back(obj);
  }

  /**
   * Remove and return the top element of the topmost stack
   */
  T pop() {
    T obj = std::move(_stacks.back());
    _stacks.pop_back();
    return obj;
  }

  /**
   * Start a new stack
   */
  void scope_in() {
    //
    _stack_starts.push_back(_stacks.size());
  }

  /**
   * Remove all elements from the topmost stack and restore
   */
  void scope_out() {
    while (_stacks.size() > _stack_starts.back()) {
      _stacks.pop_back();
    }
    _stack_starts.pop_back();
  }

  /**
   * Return the size of topmost stack
   */
  size_t top_size() {
    //
    return _stacks.size() - _stack_starts.back();
  }

  /**
   * Return total number of elements in all stack
   */
  size_t total_size() {
    //
    return _stacks.size();
  }

  /**
   * Return an iterator over the topmost stack
   */
  StackIter top_iter() {
    return StackIter(_stacks.begin() + _stack_starts.back(), _stacks.end());
  }

  StackScopedResource auto_scope() {
    return StackScopedResource(*this);
  }

private:
  std::vector<T> _stacks;
  std::vector<size_t> _stack_starts;
};

} // namespace stackofstacks
