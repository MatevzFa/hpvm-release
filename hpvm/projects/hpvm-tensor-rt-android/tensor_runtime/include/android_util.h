#ifndef __ARM_RT_ANDROID_UTIL_H__
#define __ARM_RT_ANDROID_UTIL_H__

#include <cstdint>
#include <sstream>
#include <vector>

#include <android/asset_manager.h>
#include <android/log.h>

#include "clblast_rt/tensor.h"

#define __logger(std_ostream, LEVEL, TAG, ...)                                 \
  do {                                                                         \
    __android_log_print(LEVEL, TAG, __VA_ARGS__);                              \
    fprintf(std_ostream, __VA_ARGS__);                                         \
    fprintf(std_ostream, "\n");                                                \
  } while (false)

#define LOGI(...)                                                              \
  __logger(stderr, ANDROID_LOG_INFO, "android_util", __VA_ARGS__)

#define LOGE(...)                                                              \
  __logger(stderr, ANDROID_LOG_ERROR, "android_util", __VA_ARGS__)

#define LOGW(...)                                                              \
  __logger(stderr, ANDROID_LOG_WARN, "android_util", __VA_ARGS__)

#ifndef NDEBUG
#define LOGD(...)                                                              \
  __logger(stderr, ANDROID_LOG_DEBUG, "android_util", __VA_ARGS__)
#else
#define LOGD(...)                                                              \
  // __logger(stderr, ANDROID_LOG_DEBUG, "android_util", __VA_ARGS__)
#endif

#ifndef NDEBUG
#define LOG_DIMS(TENSOR)                                                       \
  do {                                                                         \
  } while (false)

#define LOG_TENSOR(TENSOR)                                                     \
  do {                                                                         \
  } while (false)
#else
#define LOG_DIMS(TENSOR)

#define LOG_TENSOR(TENSOR)
#endif

#define THROW_ERROR_ON_MSG(COND, MSG)                                          \
  do {                                                                         \
    if (COND)                                                                  \
      throw std::runtime_error(MSG);                                           \
  } while (false)

#define __stringify(a) #a

#define NOT_NULL(PTR)                                                          \
  THROW_ERROR_ON_MSG(PTR == nullptr,                                           \
                     #PTR " was null in " __FILE__ ":" __stringify(__LINE__))

namespace android_util {

class ScopedTensorRtInit {
public:
  ScopedTensorRtInit();
  ~ScopedTensorRtInit();
};

clblast_rt::Tensor *readTrainedWeights(AAssetManager *mgr, const char *path,
                                       int data_type, long int dim1_size,
                                       long int dim2_size, long int dim3_size,
                                       long int dim4_size);

clblast_rt::Tensor *readInputBatch(AAssetManager *mgr, const char *path,
                                   int data_type, long start, long end,
                                   long int dim2_size, long int dim3_size,
                                   long int dim4_size);

} // namespace android_util

#endif
