#include "configuration.h"
#include <stdexcept>

using P_APPROX = PROMISENodeConfiguration::APPROX;
using G_APPROX = GPUNodeConfiguration::APPROX;
using C_APPROX = CPUNodeConfiguration::APPROX;
using G_TENSOR_OP = GPUNodeConfiguration::TENSOR_OP;
using C_TENSOR_OP = CPUNodeConfiguration::TENSOR_OP;

bool NodeConfiguration::isPROMISENodeConfiguration() {
  return NODE_CONFIGURATION_TARGET_ID == PROMISE;
}

bool NodeConfiguration::isGPUNodeConfiguration() {
  return NODE_CONFIGURATION_TARGET_ID == GPU;
}

bool NodeConfiguration::isCPUNodeConfiguration() {
  return NODE_CONFIGURATION_TARGET_ID == CPU;
}

void PROMISENodeConfiguration::pushNewApproximationChoice(
    P_APPROX approx, int u) {
  ApproxChoices.push_back(std::make_pair(approx, u));
}

std::vector<std::pair<P_APPROX, int>> &
PROMISENodeConfiguration::getApproxChoices() {
  return ApproxChoices;
}

PROMISENodeConfiguration::PROMISENodeConfiguration() {
  NODE_CONFIGURATION_TARGET_ID = PROMISE;
}

PROMISENodeConfiguration::~PROMISENodeConfiguration() {}

void GPUNodeConfiguration::pushNewTensorOperation(G_TENSOR_OP top) {
  std::vector<std::pair<G_APPROX, int>> emptyVec;
  ApproxChoices.push_back(std::make_pair(top, emptyVec));
}

void GPUNodeConfiguration::pushNewApproximationChoiceForOperation(
    G_APPROX approx, int u) {
  unsigned size = ApproxChoices.size();
  CUSTOM_ASSERT(
      size >= 1 &&
      "Cannot apply approximation choice to non existent operation.");
  ApproxChoices[size - 1].second.push_back(std::make_pair(approx, u));
}

std::vector<std::pair<G_TENSOR_OP, std::vector<std::pair<G_APPROX, int>>>> &
GPUNodeConfiguration::getApproxChoices() {
  return ApproxChoices;
}

GPUNodeConfiguration::GPUNodeConfiguration() {
  NODE_CONFIGURATION_TARGET_ID = GPU;
}
GPUNodeConfiguration::~GPUNodeConfiguration() {}

void CPUNodeConfiguration::pushNewTensorOperation(C_TENSOR_OP top) {
  std::vector<std::pair<C_APPROX, int>> emptyVec;
  ApproxChoices.push_back(std::make_pair(top, emptyVec));
}

void CPUNodeConfiguration::pushNewApproximationChoiceForOperation(
    C_APPROX approx, int u) {
  unsigned size = ApproxChoices.size();
  CUSTOM_ASSERT(
      size >= 1 &&
      "Cannot apply approximation choice to non existent operation.");
  ApproxChoices[size - 1].second.push_back(std::make_pair(approx, u));
}

std::vector<std::pair<C_TENSOR_OP, std::vector<std::pair<C_APPROX, int>>>> &
CPUNodeConfiguration::getApproxChoices() {
  return ApproxChoices;
}

CPUNodeConfiguration::CPUNodeConfiguration() {
  NODE_CONFIGURATION_TARGET_ID = CPU;
}
CPUNodeConfiguration::~CPUNodeConfiguration() {}

Configuration::Configuration(
    std::string &n, float f, float e, float a, float al)
    : name(n), speedup(f), energy(e), accuracy(a), accuracyLoss(al) {}

void Configuration::addOverallConfidence(float avg, float correctAvg,
                                         float wrongAvg) {
  _overall_confidence.avg = avg;
  _overall_confidence.correctAvg = correctAvg;
  _overall_confidence.wrongAvg = wrongAvg;
}

void Configuration::addPerclassConfidence(size_t label, float avg,
                                          float correctAvg, float wrongAvg) {
  if (_perclass_confidence.size() != label) {
    std::runtime_error(
        "Invalid order of labels when adding perclass confidences");
  }

  auto c = ConfidenceInfo{};
  c.avg = avg;
  c.correctAvg = correctAvg;
  c.wrongAvg = wrongAvg;

  _perclass_confidence.push_back(c);
}

float Configuration::getSpeedup() { return speedup; }

float Configuration::getEnergy() { return energy; }

float Configuration::getAccuracy() { return accuracy; }

float Configuration::getAccuracyLoss() { return accuracyLoss; }
bool ConfigurationLessThan::
operator()(const struct Configuration &a, const struct Configuration &b) const {
  return (a.accuracyLoss < b.accuracyLoss);
}
bool ConfigurationLessThan_AL::
operator()(const struct Configuration *a, const float &b) const {
  return (a->accuracyLoss < b);
}
bool ConfigurationLessThan_SP::
operator()(const struct Configuration *a, const float &b) const {
  return (a->speedup < b);
}
bool ConfigurationLessThan_E::
operator()(const struct Configuration *a, const float &b) const {
  return (a->energy < b);
}

//****** HEADER Ends - Source Starts

// Helper configuration print methods

void PROMISENodeConfiguration::print() {

  LOGD(" promise");
  for (auto &it : ApproxChoices) {
    LOGD(" ");
    switch (it.first) {
    case P_APPROX::SWING_LEVEL:
      LOGD("swing_level");
      break;
      // TODO additional approx methods to be printed here
    default:
      ERROR("Unknown approximation option");
      break;
    }
    LOGD(" %d", it.second);
  }

  LOGD("\n");
}

void GPUNodeConfiguration::print() {

  LOGD(" gpu");
  for (auto &it : ApproxChoices) {

    LOGD(" ");
    switch (it.first) {
    case G_TENSOR_OP::ADD:
      LOGD("add");
      break;
    case G_TENSOR_OP::BATCHNORM:
      LOGD("batchnorm");
      break;
    case G_TENSOR_OP::CONV:
      LOGD("conv");
      break;
    case G_TENSOR_OP::GROUP_CONV:
      LOGD("group_conv");
      break;
    case G_TENSOR_OP::MUL:
      LOGD("mul");
      break;
    case G_TENSOR_OP::RELU:
      LOGD("relu");
      break;
    case G_TENSOR_OP::CLIPPED_RELU:
      LOGD("clipped_relu");
      break;
    case G_TENSOR_OP::TANH:
      LOGD("tanh");
      break;
    case G_TENSOR_OP::POOL_MAX:
      LOGD("pool_max");
      break;
    case G_TENSOR_OP::POOL_MEAN:
      LOGD("pool_mean");
      break;
    case G_TENSOR_OP::POOL_MIN:
      LOGD("pool_min");
      break;
    case G_TENSOR_OP::SOFTMAX:
      LOGD("softmax");
      break;
    case G_TENSOR_OP::FFT:
      LOGD("fft");
      break;
    case G_TENSOR_OP::REDUCE:
      LOGD("reduce");
      break;
    case G_TENSOR_OP::PROJECTIVE_T:
      LOGD("projectiveT");
      break;
    case G_TENSOR_OP::MAP1:
      LOGD("map1");
      break;
    case G_TENSOR_OP::MAP2:
      LOGD("map2");
      break;
    case G_TENSOR_OP::MAP3:
      LOGD("map3");
      break;
      // TODO additional operations to be printed here
    default:
      ERROR("Unknown tensor operation.");
      break;
    }

    auto &approxVec = it.second;
    for (auto &inner_it : approxVec) {
      LOGD(" ");
      switch (inner_it.first) {
      case G_APPROX::FP32:
        LOGD("fp32");
        break;
      case G_APPROX::FP16:
        LOGD("fp16");
        break;
      case G_APPROX::PERFORATION:
        LOGD("perf");
        break;
      case G_APPROX::PERFORATION_HP:
        LOGD("perf_fp16");
        break;
      case G_APPROX::INPUT_SAMPLING:
        LOGD("samp");
        break;
      case G_APPROX::INPUT_SAMPLING_HP:
        LOGD("samp_fp16");
        break;
      case G_APPROX::REDUCTION_SAMPLING:
        LOGD("red_samp");
        break;
        // TODO additional approx methods to be printed here
      default:
        ERROR("Unknown approximation option");
        break;
      }

      LOGD(" %d", inner_it.second);
    }
  }

  LOGD("\n");
}

void CPUNodeConfiguration::print() {

  LOGD(" cpu");
  for (auto &it : ApproxChoices) {

    LOGD(" ");
    switch (it.first) {
    case C_TENSOR_OP::ADD:
      LOGD("add");
      break;
    case C_TENSOR_OP::BATCHNORM:
      LOGD("batchnorm");
      break;
    case C_TENSOR_OP::CONV:
      LOGD("conv");
      break;
    case C_TENSOR_OP::GROUP_CONV:
      LOGD("group_conv");
      break;
    case C_TENSOR_OP::MUL:
      LOGD("mul");
      break;
    case C_TENSOR_OP::RELU:
      LOGD("relu");
      break;
    case C_TENSOR_OP::CLIPPED_RELU:
      LOGD("clipped_relu");
      break;
    case C_TENSOR_OP::TANH:
      LOGD("tanh");
      break;
    case C_TENSOR_OP::POOL_MAX:
      LOGD("pool_max");
      break;
    case C_TENSOR_OP::POOL_MEAN:
      LOGD("pool_mean");
      break;
    case C_TENSOR_OP::POOL_MIN:
      LOGD("pool_min");
      break;
    case C_TENSOR_OP::SOFTMAX:
      LOGD("softmax");
      break;
      // TODO additional operations to be printed here
    default:
      ERROR("Unknown tensor operation.");
      break;
    }

    auto &approxVec = it.second;
    for (auto &inner_it : approxVec) {
      LOGD(" ");
      switch (inner_it.first) {
      case C_APPROX::FP32:
        LOGD("fp32");
        break;
      case C_APPROX::PERFORATION:
        LOGD("perf");
        break;
      case C_APPROX::INPUT_SAMPLING:
        LOGD("samp");
        break;
        // TODO additional approx methods to be printed here
      default:
        ERROR("Unknown approximation option");
        break;
      }

      LOGD(" %d", inner_it.second);
    }
  }

  LOGD("\n");
}

void Configuration::print() {

  LOGD("+++++\n");
  LOGD(
      "%s %f %f %f %f\n", name.c_str(), speedup, energy, accuracy,
      accuracyLoss);
  for (std::map<std::string, NodeConfiguration *>::const_iterator it =
           setup.begin();
       it != setup.end(); ++it) {
    LOGD("%s :", it->first.c_str());

    it->second->print();
  }

  LOGD("-----\n");
}
