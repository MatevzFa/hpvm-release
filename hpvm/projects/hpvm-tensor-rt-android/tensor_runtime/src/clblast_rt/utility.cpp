#include "clblast_rt/utility.h"

#include "clblast.h"
#include "clblast_half.h"
#include "clblast_rt/tensor.h"
#include "fmt/format.h"
#include "fmt/ostream.h"
#include "util.h"
#include "utilities/approxhpvm_util.hpp"

namespace clblast_rt {

template <const DataType dtype>
void print_impl(std::ostream &out, cl_command_queue queue, const Shape &shape,
                cl_mem buf, size_t offset, size_t max_dim) {
  using T = typename data_type_into<dtype>::type;

  auto n = shape.n();
  auto c = shape.c();
  auto h = shape.h();
  auto w = shape.w();

  std::vector<T> data(h * w * c * n);
  read(queue, buf, offset * data_type_element_size(dtype), data);

  const size_t precision = 2;

  size_t longest = 0;

  for (size_t nn = 0; nn < std::min(n, max_dim); nn++) {
    for (size_t cc = 0; cc < std::min(c, max_dim); cc++) {
      for (size_t hh = 0; hh < std::min(h, max_dim); hh++) {
        for (size_t ww = 0; ww < std::min(w, max_dim); ww++) {
          auto size = fmt::formatted_size(
              "{:.{}f}",
              convert<T, float>(
                  data[nn * w * h * c + cc * w * h + hh * w + ww]),
              precision);
          if (longest < size) {
            longest = size;
          }
        }
      }
    }
  }

  for (size_t nn = 0; nn < std::min(n, max_dim); nn++) {
    for (size_t cc = 0; cc < std::min(c, max_dim); cc++) {
      fmt::print(out, "[n={} c={}]\n", nn, cc);
      for (size_t hh = 0; hh < std::min(h, max_dim); hh++) {
        for (size_t ww = 0; ww < std::min(w, max_dim); ww++) {
          fmt::print(out, "{:{}.{}f} ",
                     convert<T, float>(
                         data[nn * w * h * c + cc * w * h + hh * w + ww]),
                     longest, precision);
        }
        fmt::print(out, "\n");
      }
      fmt::print(out, "\n");
    }
    fmt::print(out, "\n");
  }
}

void print(const std::string &name, std::ostream &out, cl_command_queue queue,
           const DataType &dtype, const Shape &shape, cl_mem buf, size_t offset,
           size_t max_dim) {
  fmt::print(out, "[{}]\n", name);

  switch (dtype) {
  case DataType::FP16:
    print_impl<DataType::FP16>(out, queue, shape, buf, offset, max_dim);
    break;
  case DataType::FP32:
    print_impl<DataType::FP32>(out, queue, shape, buf, offset, max_dim);
    break;
  case DataType::I32:
    print_impl<DataType::I32>(out, queue, shape, buf, offset, max_dim);
    break;
  case DataType::U32:
    print_impl<DataType::U32>(out, queue, shape, buf, offset, max_dim);
    break;
  }
}

void clChk(cl_int status) {
  if (status != CL_SUCCESS) {
    throw std::runtime_error(fmt::format("cl error {}", status));
  }
}

void blastChk(const clblast::StatusCode &status) {
  if (status != clblast::StatusCode::kSuccess) {
    throw std::runtime_error(fmt::format("blast error {}", status));
  }
}

size_t data_type_element_size(const DataType &dt) {
  switch (dt) {
  case DataType::FP16:
    return 2;
  case DataType::FP32:
    return 4;
  case DataType::I32:
    return 4;
  case DataType::U32:
    return 4;
  }
}

size_t perf_index_restore(size_t idx, size_t perf_offset, size_t perf_stride) {
  return clblast::approxhpvm_perf_index_restore(idx, perf_offset, perf_stride);
}

size_t perf_out_size(size_t dim_size, size_t perf_offset, size_t perf_stride) {
  return clblast::approxhpvm_perf_out_size(dim_size, perf_offset, perf_stride);
}

size_t conv_size(size_t size, size_t kernel_size, size_t pad_both,
                 size_t stride) {
  return clblast::conv_size(size, kernel_size, pad_both, stride);
}

} // namespace clblast_rt