#include "clblast_rt/op_convolution.h"

#include "clblast_rt/tensor.h"

#include "clblast.h"
#include "clblast_half.h"

#include "CL/cl.h"

#include "util.h"

#include <cassert>
#include <stdexcept>
#include <type_traits>
#include <vector>

namespace clblast_rt {

template <const bool use_half>
void convolution(cl_command_queue &queue, Tensor &image, Tensor &filter,
                 Tensor &output, cl_mem workspace, size_t stride_w,
                 size_t stride_h, size_t pad_w, size_t pad_h,
                 size_t *workspace_size_ret, cl_event *e_im2col,
                 cl_event *e_gemm) {

  using T = typename std::conditional<use_half, half, cl_float>::type;

  if (use_half) {
    assert(image.buf_dev_half().is_allocated());
    assert(filter.buf_dev_half().is_allocated());
    assert(output.buf_dev_half().is_allocated());
  } else {
    assert(image.buf_dev().is_allocated());
    assert(filter.buf_dev().is_allocated());
    assert(output.buf_dev().is_allocated());
  }

  auto w = image.shape().w();
  auto h = image.shape().h();
  auto c = image.shape().c();
  auto n = image.shape().n();
  auto kh = filter.shape().h();
  auto kw = filter.shape().w();
  auto kn = filter.shape().n();

  DataType dtype = use_half ? image.type_half() : image.type();

  size_t conv_h = conv_size(h, kh, 2 * pad_h, stride_h);
  size_t conv_w = conv_size(w, kw, 2 * pad_w, stride_w);

  size_t kernel_area = kw * kh;

  Shape col_shape = Shape(n, 1, kernel_area * c, conv_h * conv_w);

  if (workspace_size_ret != nullptr) {
    *workspace_size_ret = col_shape.area() * data_type_element_size(dtype) * 2;
    return;
  }

  using namespace clblast;

  std::vector<size_t> a_offsets;
  std::vector<size_t> b_offsets;
  std::vector<size_t> c_offsets;
  std::vector<T> alphas;
  std::vector<T> betas;

  for (int batch = 0; batch < n; batch++) {
    a_offsets.push_back(0);
    b_offsets.push_back(batch * conv_h * conv_w * kernel_area * c);
    c_offsets.push_back(batch * conv_h * conv_w * kn);
    alphas.push_back(convert<float, T>(1));
    betas.push_back(convert<float, T>(0));
  }

  size_t mm_k = kernel_area * c;
  size_t mm_m = kn;
  size_t mm_n = conv_h * conv_w;

  cl_mem im_buf = use_half ? image.buf_dev_half().raw() : image.buf_dev().raw();
  cl_mem out_buf =
      use_half ? output.buf_dev_half().raw() : output.buf_dev().raw();
  cl_mem kn_buf =
      use_half ? filter.buf_dev_half().raw() : filter.buf_dev().raw();

  blastChk(Im2col<T>(KernelMode::kCrossCorrelation, c * n, h, w, kh, kw, pad_h,
                     pad_w, stride_h, stride_w, 1, 1, im_buf, 0, workspace, 0,
                     &queue, e_im2col));

  blastChk(GemmBatched<T>(Layout::kRowMajor, Transpose::kNo, Transpose::kNo,
                          mm_m, mm_n, mm_k, alphas.data(), kn_buf,
                          a_offsets.data(), mm_k, workspace, b_offsets.data(),
                          mm_n, betas.data(), out_buf, c_offsets.data(), mm_n,
                          n, &queue, e_gemm));
}

void convolution(cl_command_queue &queue, Tensor &image, Tensor &filter,
                 Tensor &output, cl_mem workspace, size_t stride_w,
                 size_t stride_h, size_t pad_w, size_t pad_h,
                 size_t *workspace_size_ret, cl_event *e_im2col,
                 cl_event *e_gemm) {

  convolution<false>(queue, image, filter, output, workspace, stride_w,
                     stride_h, pad_w, pad_h, workspace_size_ret, e_im2col,
                     e_gemm);
}

void convolution_half(cl_command_queue &queue, Tensor &image, Tensor &filter,
                      Tensor &output, cl_mem workspace, size_t stride_w,
                      size_t stride_h, size_t pad_w, size_t pad_h,
                      size_t *workspace_size_ret, cl_event *e_im2col,
                      cl_event *e_gemm) {

  convolution<true>(queue, image, filter, output, workspace, stride_w, stride_h,
                    pad_w, pad_h, workspace_size_ret, e_im2col, e_gemm);
}

} // namespace clblast_rt