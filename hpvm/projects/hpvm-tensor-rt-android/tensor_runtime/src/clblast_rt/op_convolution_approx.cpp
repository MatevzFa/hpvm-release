#include "clblast_rt/op_convolution_approx.h"

#include <cassert>
#include <iostream>
#include <stdexcept>
#include <type_traits>
#include <vector>

#include "CL/cl.h"
#include "clblast.h"
#include "clblast_half.h"
#include "clblast_rt/operations.h"
#include "clblast_rt/tensor.h"
#include "clblast_rt/utility.h"
#include "fmt/core.h"
#include "util.h"

namespace clblast_rt {

using BlastApproxInfo = clblast::ApproxInfo;
using BlastApproxMode = clblast::ApproxMode;

BlastApproxInfo to_blast(const ApproxInfo &info) {
  BlastApproxInfo blast_info;
  switch (info.mode) {
  case ApproxMode::PERF_COL:
    blast_info.mode = BlastApproxMode::PERF_COL;
    break;
  case ApproxMode::PERF_ROW:
    blast_info.mode = BlastApproxMode::PERF_ROW;
    break;
  case ApproxMode::SAMP_KERNEL:
    blast_info.mode = BlastApproxMode::SAMP_KERNEL;
    break;
  default:
    throw std::runtime_error("unknown mode");
  }
  blast_info.perf_offset = info.perf_offset;
  blast_info.perf_stride = info.perf_stride;

  return blast_info;
}

template <const bool use_half>
void convolution_approx(cl_command_queue &queue, Tensor &image, Tensor &filter, Tensor &output,
                        cl_mem workspace, size_t stride_w, size_t stride_h, size_t pad_w,
                        size_t pad_h, const ApproxInfo &approx_info, size_t *workspace_size_ret,
                        cl_event *e_kn2row, cl_event *e_im2col, cl_event *e_gemm,
                        cl_event *e_interpolate) {
  using T = typename std::conditional<use_half, half, cl_float>::type;

  if (use_half) {
    assert(image.buf_dev_half().is_allocated());
    assert(filter.buf_dev_half().is_allocated());
    assert(output.buf_dev_half().is_allocated());
  } else {
    assert(image.buf_dev().is_allocated());
    assert(filter.buf_dev().is_allocated());
    assert(output.buf_dev().is_allocated());
  }

  DataType dtype = use_half ? image.type_half() : image.type();

  auto h = image.shape().h();
  auto w = image.shape().w();
  auto c = image.shape().c();
  auto n = image.shape().n();
  auto kh = filter.shape().h();
  auto kw = filter.shape().w();
  auto kn = filter.shape().n();

  size_t conv_h = conv_size(h, kh, 2 * pad_h, stride_h);
  size_t conv_w = conv_size(w, kw, 2 * pad_w, stride_w);

  auto out_h = approx_info.mode == ApproxMode::PERF_ROW
                   ? perf_out_size(conv_h, approx_info.perf_offset, approx_info.perf_stride)
                   : conv_h;

  auto out_w = approx_info.mode == ApproxMode::PERF_COL
                   ? perf_out_size(conv_w, approx_info.perf_offset, approx_info.perf_stride)
                   : conv_w;

  auto kernel_area =
      approx_info.mode == ApproxMode::SAMP_KERNEL
          ? perf_out_size(kh * kw * c, approx_info.perf_offset, approx_info.perf_stride)
          : kh * kw * c;

  Shape col_buf_shape = Shape(n, 1, kernel_area, out_h * out_w);
  Shape row_buf_shape = Shape(1, 1, kn, kernel_area);
  Shape gemm_buf_shape = Shape(n, 1, kn, out_h * out_w);

  cl_mem im_buf = use_half ? image.buf_dev_half().raw() : image.buf_dev().raw();
  cl_mem kn_buf = use_half ? filter.buf_dev_half().raw() : filter.buf_dev().raw();
  cl_mem out_buf = use_half ? output.buf_dev_half().raw() : output.buf_dev().raw();

  size_t needed_workspace_nelems = 0;

  // im2col transform
  cl_mem col_buf = workspace;
  size_t col_buf_offset = needed_workspace_nelems;
  needed_workspace_nelems += col_buf_shape.area();

  // kn2row transform
  cl_mem row_buf;
  size_t row_buf_offset;
  if (approx_info.mode == ApproxMode::SAMP_KERNEL) {
    row_buf = workspace;
    row_buf_offset = needed_workspace_nelems;
    needed_workspace_nelems += row_buf_shape.area();
  } else {
    row_buf = kn_buf;
    row_buf_offset = 0;
  }

  // gemm computation
  cl_mem gemm_buf;
  size_t gemm_buf_offset;
  if (approx_info.mode == ApproxMode::PERF_COL || approx_info.mode == ApproxMode::PERF_ROW) {
    gemm_buf = workspace;
    gemm_buf_offset = needed_workspace_nelems;
    needed_workspace_nelems += gemm_buf_shape.area();
  } else {
    gemm_buf = out_buf;
    gemm_buf_offset = 0;
  }

  if (workspace_size_ret != nullptr) {
    *workspace_size_ret = needed_workspace_nelems * data_type_element_size(dtype);
    return;
  }

  using namespace clblast;

  if (approx_info.mode == ApproxMode::SAMP_KERNEL) {
    blastChk(Kn2rowApprox<T>(KernelMode::kCrossCorrelation, kn, c, kh, kw, kn_buf, 0, row_buf,
                             row_buf_offset, to_blast(approx_info), &queue, e_kn2row));
  }

  blastChk(Im2colApprox<T>(KernelMode::kCrossCorrelation, n, c, h, w, kh, kw, pad_h, pad_w,
                           stride_h, stride_w, 1, 1, im_buf, 0, col_buf, col_buf_offset,
                           to_blast(approx_info), &queue, e_im2col));

  // clChk(clFinish(queue));

  std::vector<size_t> a_offsets;
  std::vector<size_t> b_offsets;
  std::vector<size_t> c_offsets;
  std::vector<T> alphas;
  std::vector<T> betas;

  for (int batch = 0; batch < n; batch++) {
    a_offsets.push_back(row_buf_offset + 0);
    b_offsets.push_back(col_buf_offset + batch * out_h * out_w * kernel_area);
    c_offsets.push_back(gemm_buf_offset + batch * out_h * out_w * kn);
    alphas.push_back(convert<float, T>(1));
    betas.push_back(convert<float, T>(0));
  }

  size_t mm_k = kernel_area;
  size_t mm_m = kn;
  size_t mm_n = out_h * out_w;

  blastChk(GemmBatched<T>(Layout::kRowMajor, Transpose::kNo, Transpose::kNo, mm_m, mm_n, mm_k,
                          alphas.data(), row_buf, a_offsets.data(), mm_k, col_buf, b_offsets.data(),
                          mm_n, betas.data(), gemm_buf, c_offsets.data(), out_h * out_w, n, &queue,
                          e_gemm));
  // clChk(clFinish(queue));

  if (approx_info.mode == ApproxMode::PERF_COL || approx_info.mode == ApproxMode::PERF_ROW) {
    blastChk(ApproxInterpolate<T>(n * kn, conv_h, conv_w, gemm_buf, gemm_buf_offset, out_buf, 0,
                                  to_blast(approx_info), &queue, e_interpolate));
  }
}

void convolution_approx(cl_command_queue &queue, Tensor &image, Tensor &filter, Tensor &output,
                        cl_mem workspace, size_t stride_w, size_t stride_h, size_t pad_w,
                        size_t pad_h, const ApproxInfo &approx_info, size_t *workspace_size_ret,
                        cl_event *e_kn2row, cl_event *e_im2col, cl_event *e_gemm,
                        cl_event *e_interpolate) {
  convolution_approx<false>(queue, image, filter, output, workspace, stride_w, stride_h, pad_w,
                            pad_h, approx_info, workspace_size_ret, e_kn2row, e_im2col, e_gemm,
                            e_interpolate);
}

void convolution_approx_half(cl_command_queue &queue, Tensor &image, Tensor &filter, Tensor &output,
                             cl_mem workspace, size_t stride_w, size_t stride_h, size_t pad_w,
                             size_t pad_h, const ApproxInfo &approx_info,
                             size_t *workspace_size_ret, cl_event *e_kn2row, cl_event *e_im2col,
                             cl_event *e_gemm, cl_event *e_interpolate) {
  convolution_approx<true>(queue, image, filter, output, workspace, stride_w, stride_h, pad_w,
                           pad_h, approx_info, workspace_size_ret, e_kn2row, e_im2col, e_gemm,
                           e_interpolate);
}

} // namespace clblast_rt