#include "clblast_rt/op_gemm.h"

#include "clblast_rt/tensor.h"

#include "util.h"

#include "clblast.h"
#include "clblast_half.h"

#include <vector>

namespace clblast_rt {

template <const bool use_half>
void gemm(cl_command_queue &queue, Tensor &a, Tensor &b, Tensor &output) {

  using T = typename std::conditional<use_half, half, cl_float>::type;

  // Batch amount (can only do 2D matmul)
  auto n = a.shape().n() * a.shape().c();

  auto mm_k = a.shape().w();
  auto mm_m = a.shape().h();
  auto mm_n = b.shape().w();

  using namespace clblast;

  std::vector<size_t> a_offsets;
  std::vector<size_t> b_offsets;
  std::vector<size_t> c_offsets;
  std::vector<T> alphas;
  std::vector<T> betas;

  for (int batch = 0; batch < n; batch++) {
    a_offsets.push_back(batch * mm_k * mm_m);
    b_offsets.push_back(0);
    c_offsets.push_back(batch * mm_n * mm_m);
    alphas.push_back(convert<float, T>(1));
    betas.push_back(convert<float, T>(1));
  }

  cl_mem a_buf = use_half ? a.buf_dev_half().raw() : a.buf_dev().raw();
  cl_mem b_buf = use_half ? b.buf_dev_half().raw() : b.buf_dev().raw();
  cl_mem c_buf =
      use_half ? output.buf_dev_half().raw() : output.buf_dev().raw();

  blastChk(GemmBatched<T>(
      Layout::kRowMajor, Transpose::kNo, Transpose::kNo, mm_m, mm_n, mm_k,
      alphas.data(), a_buf, a_offsets.data(), mm_k, b_buf, b_offsets.data(),
      mm_n, betas.data(), c_buf, c_offsets.data(), mm_n, n, &queue));
}

void gemm(cl_command_queue &queue, Tensor &a, Tensor &b, Tensor &output) {
  gemm<false>(queue, a, b, output);
}

void gemm_half(cl_command_queue &queue, Tensor &a, Tensor &b, Tensor &output) {
  gemm<true>(queue, a, b, output);
}

} // namespace clblast_rt
