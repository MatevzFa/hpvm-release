#include "clblast_rt/op_pooling.h"

#include <cassert>

#include "clblast.h"
#include "clblast_half.h"
#include "util.h"

namespace clblast_rt {

using BlastPoolMode = clblast::PoolMode;

BlastPoolMode convert(PoolMode mode) {
  switch (mode) {
    case PoolMode::AVG:
      return BlastPoolMode::AVG;
    case PoolMode::MIN:
      return BlastPoolMode::MIN;
    case PoolMode::MAX:
      return BlastPoolMode::MAX;
  }
}

template <const bool use_half>
void pooling(cl_command_queue queue, PoolMode mode, Tensor &in, Tensor &out, const size_t pool_h,
             const size_t pool_w, const size_t pad_h, const size_t pad_w, const size_t stride_h,
             const size_t stride_w, cl_event *event) {
  using T = typename std::conditional<use_half, half, cl_float>::type;

  if (use_half) {
    assert(in.buf_dev_half().is_allocated());
    assert(out.buf_dev_half().is_allocated());
  } else {
    assert(in.buf_dev().is_allocated());
    assert(out.buf_dev().is_allocated());
  }

  auto channels = in.shape().n() * in.shape().c();
  auto height = in.shape().h();
  auto width = in.shape().w();

  cl_mem in_buf = use_half ? in.buf_dev_half().raw() : in.buf_dev().raw();
  cl_mem out_buf = use_half ? out.buf_dev_half().raw() : out.buf_dev().raw();

  blastChk(clblast::Pooling<T>(convert(mode), channels, height, width, pool_h, pool_w, pad_h, pad_w,
                               stride_h, stride_w, in_buf, 0, out_buf, 0, &queue, event));
}

void pooling(cl_command_queue queue, PoolMode mode, Tensor &in, Tensor &out, const size_t pool_h,
             const size_t pool_w, const size_t pad_h, const size_t pad_w, const size_t stride_h,
             const size_t stride_w, cl_event *event) {
  pooling<false>(queue, mode, in, out, pool_h, pool_w, pad_h, pad_w, stride_h, stride_w, event);
}

void pooling_half(cl_command_queue queue, PoolMode mode, Tensor &in, Tensor &out,
                  const size_t pool_h, const size_t pool_w, const size_t pad_h, const size_t pad_w,
                  const size_t stride_h, const size_t stride_w, cl_event *event) {
  pooling<true>(queue, mode, in, out, pool_h, pool_w, pad_h, pad_w, stride_h, stride_w, event);
}

}  // namespace clblast_rt