#ifndef __UTIL_H__
#define __UTIL_H__

#include <cstddef>
#include <vector>

#include "clblast.h"
#include "clblast_half.h"

#include "clblast_rt/tensor.h"
#include "clblast_rt/utility.h"

#include "CL/cl.h"
#include "CL/cl_platform.h"

namespace clblast_rt {

template <const DataType dtype> struct data_type_into {};

template <> struct data_type_into<DataType::FP16> { typedef half type; };

template <> struct data_type_into<DataType::FP32> { typedef cl_float type; };

template <> struct data_type_into<DataType::U32> { typedef cl_uint type; };

template <> struct data_type_into<DataType::I32> { typedef cl_int type; };

template <typename From, typename To> To convert(From val);

template <> inline float convert<half, float>(half val) {
  return HalfToFloat(val);
}
template <> inline half convert<float, half>(float val) {
  return FloatToHalf(val);
}
template <> inline half convert<half, half>(half val) { return val; }
template <> inline float convert<float, float>(float val) { return val; }

template <> inline float convert<int, float>(int val) { return (float)val; }
template <> inline float convert<unsigned int, float>(unsigned int val) {
  return (float)val;
}

template <typename T>
inline void read(cl_command_queue queue, cl_mem buf, size_t offset,
                 std::vector<T> &host_data) {
  cl_int status = clEnqueueReadBuffer(queue, buf, CL_BLOCKING, offset,
                                      host_data.size() * sizeof(T),
                                      host_data.data(), 0, nullptr, nullptr);
  clChk(status);
}

template <typename T>
inline void fill(cl_command_queue queue, cl_mem buf, size_t offset,
                 const std::vector<T> &host_data) {
  cl_int status = clEnqueueWriteBuffer(queue, buf, CL_BLOCKING, offset,
                                       host_data.size() * sizeof(T),
                                       host_data.data(), 0, nullptr, nullptr);
  clChk(status);
}

} // namespace clblast_rt

#endif // __UTIL_H__