#include "clblast_rt/op_add_bias.h"

namespace clblast_rt {

template <const bool use_half>
void add_bias(cl_command_queue queue, Tensor &x, Tensor &bias) {
  // TOOD
}

void add_bias(cl_command_queue queue, Tensor &x, Tensor &bias) {
  add_bias<false>(queue, x, bias);
}

void add_bias_half(cl_command_queue queue, Tensor &x, Tensor &bias) {
  add_bias<true>(queue, x, bias);
}

} // namespace clblast_rt