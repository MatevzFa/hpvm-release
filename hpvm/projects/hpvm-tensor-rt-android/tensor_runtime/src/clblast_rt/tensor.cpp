#include "clblast_rt/tensor.h"

#include "clblast_rt/utility.h"

#include "helpers.h"
#include "util.h"
#include "android_util.h"

namespace clblast_rt {

////
////
//// Shape

Shape::Shape(size_t n, size_t c, size_t h, size_t w)
    : _n(n), _c(c), _h(h), _w(w) {}

size_t Shape::area() { return _n * _c * _h * _w; }

////
////
//// BufferDev

BufferDev::BufferDev(size_t size) : _size(size), _buf(nullptr) {}

void BufferDev::allocate(cl_context context, cl_mem_flags flags) {
  if (!is_allocated()) {
    cl_int status;
    _buf = clCreateBuffer(context, flags, _size, nullptr, &status);
    clChk(status);
  }
}

void BufferDev::release() {
  if (is_allocated()) {
    clChk(clReleaseMemObject(_buf));
    _buf = nullptr;
  }
}

void BufferDev::clear(cl_command_queue queue) {
  cl_uchar pattern = 0;

  clChk(clEnqueueFillBuffer(queue, _buf, &pattern, 1, 0, _size, 0, nullptr,
                            nullptr));
  sync();
}

bool BufferDev::is_allocated() { return _buf != nullptr; }

cl_mem BufferDev::raw() {
  if (is_allocated()) {
    return _buf;
  } else {
    throw std::runtime_error("buffer not allocated");
  }
}

////
////
//// BufferHost

BufferHost::BufferHost(size_t size) : _size(size), _buf(nullptr) {}

void BufferHost::allocate() {
  if (!is_allocated()) {
    _buf = new char[_size];
  }
}

void BufferHost::release() {
  if (is_allocated()) {
    delete[] _buf;
    _buf = nullptr;
  }
}

void BufferHost::clear() {
  if (is_allocated()) {
    for (int i = 0; i < _size; i++) {
      _buf[i] = 0;
    }
  }
}

bool BufferHost::is_allocated() { return _buf != nullptr; }

char *BufferHost::raw() {
  if (is_allocated()) {
    return _buf;
  } else {
    throw std::runtime_error("buffer not allocated");
  }
}

////
////
//// Tensor

Tensor::Tensor(DataType type, size_t n, size_t c, size_t h, size_t w)
    : Tensor(type, DataType::FP16, Shape(n, c, h, w)) {}

Tensor::Tensor(DataType type, Shape shape)
    : Tensor(type, DataType::FP16, shape) {}

Tensor::Tensor(DataType type, DataType type_half, Shape shape)
    : _buf_dev(data_type_element_size(type) * shape.area()),           //
      _buf_dev_half(data_type_element_size(type_half) * shape.area()), //
      _buf_host(data_type_element_size(type) * shape.area()),          //
      _type(type), _type_half(type_half),                              //
      _shape(shape),                                                   //
      num_elems(shape.area()), dims(shape),                            //
      _placement(DataPlacement::UNALLOCATED)                           //
{}

Tensor::~Tensor() {
  if (buf_dev().is_allocated() || buf_dev_half().is_allocated()) {
    // device memory leak
    LOGW("device memory leak");
  }

  buf_host().release();
}

Shape &Tensor::shape() { return _shape; }

BufferDev &Tensor::buf_dev() { return _buf_dev; }

BufferDev &Tensor::buf_dev_half() { return _buf_dev_half; }

BufferHost &Tensor::buf_host() { return _buf_host; }

} // namespace clblast_rt
