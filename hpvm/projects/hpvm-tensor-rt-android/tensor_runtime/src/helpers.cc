#include "helpers.h"

#include "clblast.h"

#include "CL/cl.h"

#include "fmt/format.h"

#include "clblast_rt/tensor.h"
#include "clblast_rt/utility.h"

#include "android_util.h"
#include "constants.h"
#include "global_data.h"

#include <stdexcept>

using namespace clblast;
using namespace clblast_rt;

DataType data_type_from_internal(int data_type) {
  switch (data_type) {
  case TENSOR_TYPE_FLOAT:
    return DataType::FP32;
  case TENSOR_TYPE_HALF:
    return DataType::FP16;
  case TENSOR_TYPE_INT:
    return DataType::I32;
  default:
    throw std::runtime_error("Unknown internal TENSOR_TYPE");
  }
}

clblast_rt::PoolMode pool_mode_from_internal(int pool_mode) {
  switch (pool_mode) {
  case POOLING_AVG:
    return clblast_rt::PoolMode::AVG;
  case POOLING_MIN:
    return clblast_rt::PoolMode::MIN;
  case POOLING_MAX:
    return clblast_rt::PoolMode::MAX;
  default:
    throw std::runtime_error("Unknown internal POOLING");
  }
}

clblast_rt::ApproxInfo approx_mode_from_internal(int row, int col,
                                                 int skip_every, int offset) {
  using ApproxMode = clblast_rt::ApproxMode;

  if (row > 1) {
    return clblast_rt::ApproxInfo(ApproxMode::PERF_ROW, offset, row);
  }

  if (col > 1) {
    return clblast_rt::ApproxInfo(ApproxMode::PERF_COL, offset, col);
  }

  if (skip_every > 1) {
    return clblast_rt::ApproxInfo(ApproxMode::SAMP_KERNEL, offset, skip_every);
  }

  throw std::runtime_error(fmt::format(
      "Unknown internal approx info row={} col={} skip_every={} offset={}", row,
      col, skip_every, offset));
}

/**
 * This function probably isn't needed, since ARM's OpenCL queues beave in
 * FIFO order -- kernels get executed in the same order they were enqueued.
 */
void sync() {
  clChk(clFlush(rtCtx->queue()));
  clChk(clFinish(rtCtx->queue()));
}

/**
 * When create_tensor is called with dtype FP16, the tensor object will have
 * the following types:
 *
 * dev_buf      : FP32
 * dev_buf_half : FP16
 * host_buf     : FP32
 */
Tensor *create_tensor(const DataType &dtype, const Shape &shape,
                      const clblast_rt::DataPlacement &placement) {
  auto profile_scope = rtCtx->profile("create_tensor");

  bool creating_fp16 = dtype == DataType::FP16;

  auto *tensor = new Tensor(creating_fp16 ? DataType::FP32 : dtype, shape);

  tensor->set_data_placement(placement);
  switch (placement) {
  case DataPlacement::DEV:
    tensor->buf_dev().allocate(rtCtx->context());
    tensor->buf_dev().clear(rtCtx->queue());
    break;
  case DataPlacement::DEV_HALF:
    tensor->buf_dev_half().allocate(rtCtx->context());
    tensor->buf_dev_half().clear(rtCtx->queue());
    break;
  case DataPlacement::HOST:
    tensor->buf_host().allocate();
    tensor->buf_host().clear();
    break;
  default:
    throw std::runtime_error("create_tensor: unknown DataPlacement");
  }

  sync();

  rtCtx->track_tensor(tensor);

  return tensor;
}

void full2half(void *tensor_ptr) {
  auto pr = rtCtx->profile(__func__);

  auto *tensor = reinterpret_cast<Tensor *>(tensor_ptr);

  if (!tensor->is_full_used()) {
    LOGD("%s returning early", __func__);
    return;
  }

  tensor->buf_dev_half().allocate(rtCtx->context());

  blastChk(CastFull2Half(tensor->shape().area(), tensor->buf_dev().raw(), 0,
                tensor->buf_dev_half().raw(), 0, &rtCtx->queue()));
  sync();

  tensor->set_data_placement(DataPlacement::DEV_HALF);

  tensor->buf_dev().release();
}

void half2full(void *tensor_ptr) {
  auto pr = rtCtx->profile(__func__);

  auto *tensor = reinterpret_cast<Tensor *>(tensor_ptr);

  if (!tensor->is_half_used()) {
    LOGD("%s returning early", __func__);
    return;
  }

  tensor->buf_dev().allocate(rtCtx->context());

  blastChk(CastHalf2Full(tensor->shape().area(), tensor->buf_dev_half().raw(), 0,
                tensor->buf_dev().raw(), 0, &rtCtx->queue()));
  sync();

  tensor->set_data_placement(DataPlacement::DEV);

  tensor->buf_dev_half().release();
}

void dev2host(void *tensor_ptr) {
  auto pr = rtCtx->profile(__func__);

  auto *tensor = reinterpret_cast<Tensor *>(tensor_ptr);

  if (!tensor->is_full_used()) {
    LOGD("%s returning early", __func__);
    return;
  }

  tensor->buf_host().allocate();

  sync();
  clChk(clEnqueueReadBuffer(rtCtx->queue(), tensor->buf_dev().raw(),
                            CL_NON_BLOCKING, 0, tensor->buf_dev().size(),
                            tensor->buf_host().raw(), 0, nullptr, nullptr));
  sync();

  tensor->set_data_placement(DataPlacement::HOST);

  tensor->buf_dev().release();
}

void host2dev(void *tensor_ptr) {
  auto pr = rtCtx->profile(__func__);

  auto *tensor = reinterpret_cast<Tensor *>(tensor_ptr);

  if (!tensor->is_host_used()) {
    LOGD("%s returning early", __func__);
    return;
  }

  tensor->buf_dev().allocate(rtCtx->context());

  sync();
  clChk(clEnqueueWriteBuffer(rtCtx->queue(), tensor->buf_dev().raw(),
                             CL_NON_BLOCKING, 0, tensor->buf_dev().size(),
                             tensor->buf_host().raw(), 0, nullptr, nullptr));
  sync();

  tensor->set_data_placement(DataPlacement::DEV);

  tensor->buf_host().release();
}

bool compare_tensors(void *a_ptr, void *baseline_ptr, float eps) {
  auto *a = reinterpret_cast<Tensor *>(a_ptr);
  auto *b = reinterpret_cast<Tensor *>(baseline_ptr);

  assert(a->shape().n() == b->shape().n());
  assert(a->shape().c() == b->shape().c());
  assert(a->shape().h() == b->shape().h());
  assert(a->shape().w() == b->shape().w());

  int n = a->shape().n();
  int c = a->shape().c();
  int h = a->shape().h();
  int w = a->shape().w();

  half2full(a);
  half2full(b);

  dev2host(a);
  dev2host(b);

  float *data_a = (float *)a->buf_host().raw();
  float *data_b = (float *)b->buf_host().raw();

  int nerrors = 0;
  float avg_err = 0;
  float avg_value = 0;

  for (int ni = 0; ni < n; ni++) {
    int n_offset = ni * c * h * w;
    for (int ci = 0; ci < c; ci++) {
      int c_offset = ci * h * w;
      for (int hi = 0; hi < h; hi++) {
        int h_offset = hi * w;
        for (int wi = 0; wi < w; wi++) {
          float abs_err = abs(data_a[n_offset + c_offset + h_offset + wi] -
                              data_b[n_offset + c_offset + h_offset + wi]);
          bool eq = abs_err < eps;
          if (!eq) {
            nerrors++;
          }

          avg_err += abs_err;
          avg_value += abs(data_b[n_offset + c_offset + h_offset + wi]);
        }
      }
    }
  }

  avg_err /= (float)a->shape().area();
  avg_value /= (float)a->shape().area();

  if (nerrors > 0) {
    LOGE("compare_tensors: %d (out of %zd) values less than eps=%f "
         "(avg_err=%f, avg_value=%f)",
         nerrors, a->shape().area(), eps, avg_err, avg_value);
  }

  return nerrors == 0;
}
