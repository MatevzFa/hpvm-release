#include "android_util.h"

#include "clblast_rt/tensor.h"

#include "constants.h"
#include "tensor_runtime.h"
#include "helpers.h"

#include "fmt/format.h"

#include <android/asset_manager.h>
#include <cstdint>
#include <sstream>
#include <vector>

namespace android_util {

using namespace clblast_rt;

ScopedTensorRtInit::ScopedTensorRtInit() { llvm_hpvm_initTensorRt(); }

ScopedTensorRtInit::~ScopedTensorRtInit() { llvm_hpvm_cleanupTensorRt(); }

Tensor *readTrainedWeights(AAssetManager *mgr, const char *path, int data_type,
                           long int dim1_size, long int dim2_size,
                           long int dim3_size, long int dim4_size) {
  LOGI("readTrainedWeights: opening %s", path);

  int type_size = 4;
  long int num_elements = dim1_size * dim2_size * dim3_size * dim4_size;
  long int size_in_bytes = num_elements * type_size;

  AAsset *asset = AAssetManager_open(mgr, path, AASSET_MODE_BUFFER);
  if (asset == nullptr) {
    LOGW("readTrainedWeights: asset is NULL");
  }
  std::vector<uint8_t> buf(size_in_bytes, 0);
  AAsset_read(asset, buf.data(), size_in_bytes);
  AAsset_close(asset);

  auto *tensor = create4DTensor(data_type, TENSOR_LAYOUT_NCHW, dim1_size,
                                dim2_size, dim3_size, dim4_size);
  initTensorData(tensor, buf.data(), size_in_bytes);

  LOGI("END readTrainedWeights");

  return reinterpret_cast<Tensor *>(tensor);
}

Tensor *readInputBatch(AAssetManager *mgr, const char *path, int data_type,
                       long start, long end, long int dim2_size,
                       long int dim3_size, long int dim4_size) {
  LOGI("readInputBatch: opening %s", path);

  long int dim1_size = end - start;

  int type_size = 4;
  long int num_elements = dim1_size * dim2_size * dim3_size * dim4_size;
  long int size_in_bytes = num_elements * type_size;

  AAsset *asset = AAssetManager_open(mgr, path, AASSET_MODE_BUFFER);
  if (asset == nullptr) {
    LOGW("readInputBatch: asset is NULL");
  }
  std::vector<uint8_t> buf(size_in_bytes, 0);
  AAsset_seek(asset, start * dim2_size * dim3_size * dim4_size * type_size,
              SEEK_SET);
  AAsset_read(asset, buf.data(), size_in_bytes);
  AAsset_close(asset);

  auto *tensor = create4DTensor(data_type, TENSOR_LAYOUT_NCHW, dim1_size,
                                dim2_size, dim3_size, dim4_size);
  initTensorData(tensor, buf.data(), size_in_bytes);

  LOGI("END readInputBatch");

  return reinterpret_cast<Tensor *>(tensor);
}

} // namespace android_util