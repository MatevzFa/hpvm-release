#include "RtController.h"

#include "android_util.h"
#include "clblast_rt/tensor.h"
#include "clblast_rt/utility.h"
#include "configuration.h"
#include "global_data.h"
#include "helpers.h"
#include "tensor_runtime.h"

#include <android/asset_manager.h>

#include <algorithm>
#include <bits/seek_constants.h>
#include <cstdint>
#include <cstdio>
#include <fstream>
#include <ios>
#include <sstream>
#include <string>
#include <vector>

RtController::RtController() {
  LOGD("*** Initializing RtController ***");

  reset();

  model_logger_init();
}

RtController::~RtController() {
  LOGD("*** ---- Destroying RtController ***");

  // for (auto &conf : _InitialConfigurations) {
  //   for (auto &pair : conf.setup) {
  //     NodeConfiguration *nodeconf = pair.second;

  //     if (nodeconf->isGPUNodeConfiguration()) {
  //       GPUNodeConfiguration *gpuconf = (GPUNodeConfiguration *)nodeconf;
  //       delete gpuconf;
  //     } else if (nodeconf->isCPUNodeConfiguration()) {
  //       CPUNodeConfiguration *cpuconf = (CPUNodeConfiguration *)nodeconf;
  //       delete cpuconf;
  //     } else if (nodeconf->isPROMISENodeConfiguration()) {
  //       PROMISENodeConfiguration *pconf = (PROMISENodeConfiguration
  //       *)nodeconf; delete pconf;
  //     }
  //   }
  // }

  model_logger_cleanup();
}

void RtController::loadLabels(AAssetManager *mgr, const std::string &path,
                              size_t num_labels) {
  LOGD("RtController::loadLabels via AAssetManager");

  if (num_labels == 0) {
    return;
  }

  auto labels_path = std::string(path) + "/test_labels.bin";
  AAsset *asset =
      AAssetManager_open(mgr, labels_path.c_str(), AASSET_MODE_BUFFER);
  if (asset == nullptr) {
    LOGE("Could not open labels from asset %s", labels_path.c_str());
    return;
  }

  _labels.clear();
  _labels.resize(num_labels, 0);

  AAsset_read(asset, _labels.data(), num_labels * sizeof(LabelType));
  AAsset_close(asset);
}

void RtController::loadLabels(const std::string &path, size_t num_labels) {
  LOGD("RtController::loadLabels via filesystem");

  if (num_labels == 0) {
    return;
  }

  std::ifstream in(path, std::ios::binary);
  if (!in) {
    LOGE("Could not read labels from file %s", path.c_str());
    return;
  }

  _labels.clear();
  _labels.resize(num_labels);

  in.read(reinterpret_cast<char *>(_labels.data()),
          num_labels * sizeof(LabelType));
}

bool RtController::predict_correctness(LabelType label, float confidence) {

  const auto &conf = getCurrentConfig();

  if (conf._perclass_confidence.empty()) {
    return true;
  }

  const auto &confInfo = conf._perclass_confidence[label];

  float thresh = (confInfo.wrongAvg + confInfo.correctAvg) / 2.0;

  return confidence > thresh;
}

template <class T>
void resize_perclass(RtController::LabelType true_label, std::vector<T> &vec) {
  if (vec.size() < (true_label + 1)) {
    vec.resize(true_label + 1, T());
  }
}

void RtController::invokeRtControl(void *result_ptr, long start, long end) {
  LOGD("*** RtController::invokeRtControl ***");

  auto *softmax = reinterpret_cast<Tensor *>(result_ptr);
  auto batch_size = end - start;
  auto n_classes = softmax->shape().area() / batch_size;

  assert(batch_size == softmax->shape().n());

  LOGD("nclasses=%ld batch_size=%ld", n_classes, batch_size);

  //
  // Determine prediction confidence

  dev2host(softmax);

  float *softmax_host = (float *)softmax->buf_host().raw();
  std::vector<float> max_confidence(batch_size);
  for (int bi = 0; bi < batch_size; bi++) {
    float max = softmax_host[bi * n_classes + 0];
    for (int ci = 1; ci < n_classes; ci++) {
      max = std::max(max, softmax_host[bi * n_classes + ci]);
    }
    max_confidence[bi] = max;
    LOGD("max_confidence[%d]=%f", bi, max);
  }

  // TODO act upon max_confidence

  //
  // Determine prediction accuracy
  auto *argmax_ptr = tensorArgMax(result_ptr);
  auto *argmax = reinterpret_cast<Tensor *>(argmax_ptr);

  dev2host(argmax);

  LabelType *data = (LabelType *)argmax->buf_host().raw();

  std::vector<LabelType> labels(data, data + batch_size);

  if (_labels.size() < end) {
    LOGE("invokeRtControl with start=%ld end=%ld, but _labels only has %zd "
         "labels. Returning.",
         start, end, _labels.size());
    return;
  }

  AccuracyTracker batch_accuracy;

  for (int i = 0; i < batch_size; i++) {

    const LabelType true_label = _labels[start + i];
    const LabelType pred_label = labels[i];

    const bool correct = pred_label == true_label;

    model_log(true_label, pred_label,
              std::vector<float>(softmax_host + i * n_classes,
                                 softmax_host + (i + 1) * n_classes));

    // Check if the perclass trackers have to be resized to accomodate new
    // labels.
    resize_perclass(true_label, _perclass_accuracy);
    resize_perclass(true_label, _perclass_confidence_trackers);
    resize_perclass(true_label, _correctness_predictor_perclass_accuracy);
    resize_perclass(true_label, _correctness_predictor_perclass_accuracy_true);
    resize_perclass(true_label, _correctness_predictor_perclass_accuracy_false);

    if (correct) {
      _confidence_tracker.push_correct(max_confidence[i]);
      _perclass_confidence_trackers[true_label].push_correct(max_confidence[i]);
    }

    batch_accuracy.add_prediction(correct);

    // Measure accuracy
    _accuracy.add_prediction(correct);
    _perclass_accuracy[true_label].add_prediction(correct);

    // Measure confidence
    _confidence_tracker.push_total(max_confidence[i]);
    _perclass_confidence_trackers[true_label].push_total(max_confidence[i]);

    // Measure correctness prediction
    const bool correctness_predicted =
        predict_correctness(pred_label, max_confidence[i]);

    const bool prediction_is_correct = correct == correctness_predicted;

    // Measure how often the predictor is correct
    _correctness_predictor_accuracy.add_prediction(prediction_is_correct);
    _correctness_predictor_perclass_accuracy[true_label].add_prediction(
        prediction_is_correct);

    if (correct) {
      _correctness_predictor_accuracy_true.add_prediction(
          prediction_is_correct);
      _correctness_predictor_perclass_accuracy_true[true_label].add_prediction(
          prediction_is_correct);
    } else {
      _correctness_predictor_accuracy_false.add_prediction(
          prediction_is_correct);
      _correctness_predictor_perclass_accuracy_false[true_label].add_prediction(
          prediction_is_correct);
    }
  }

  LOGI("batch_accuracy=%f total_accuracy=%f", batch_accuracy.accuracy(),
       _accuracy.accuracy());
}

#define __GLOBAL_KNOBS "global_knobs.txt"

void RtController::loadTunerConfs(AAssetManager *mgr, const std::string &path) {
  LOGD("RtController::loadTunerConfs from asset %s", path.c_str());

  perfParams.load_from(mgr, __GLOBAL_KNOBS);
  sampParams.load_from(mgr, __GLOBAL_KNOBS);

  AAsset *asset = AAssetManager_open(mgr, path.c_str(), AASSET_MODE_BUFFER);
  if (!asset) {
    LOGE("Could not open tuner confs %s", path.c_str());
    return;
  }

  auto len = AAsset_getLength(asset);
  std::vector<char> buf(len);
  AAsset_read(asset, buf.data(), len);

  std::istringstream istream(std::string(buf.data(), len));
  parse_config(istream);
}

void RtController::loadTunerConfs(const std::string &path) {
  LOGD("RtController::loadTunerConfs from file %s", path.c_str());

  perfParams.load_from(__GLOBAL_KNOBS);
  sampParams.load_from(__GLOBAL_KNOBS);

  std::ifstream in(path);
  parse_config(in);
}

void RtController::parse_config(std::istream &istream) {

  _InitialConfigurations.clear();

  if (!istream) {
    LOGE("Failed to open configuration file.");
    return;
    // abort();
  }

  bool readingFirstLine = false;

  // Read baseline_time from first line of configuration file
  std::string first_line;
  std::getline(istream, first_line);
  LOGD("first_line: %s\n", first_line.c_str());

  double baseline_time = 1;

  try {
    baseline_time = std::stod(first_line);
    LOGD("Baseline time: %lf\n\n", baseline_time);
  } catch (...) {
    LOGE("Please Add/Fix Baseline Time at Top of Config File.. ");
  }

  unsigned int firstTensorID = 1;
  for (std::string line; std::getline(istream, line);) {
    LOGD("line: %s\n", line.c_str());

    // Tokenize using ' ' as delimiter
    // Vector to store tokens
    std::vector<std::string> tokens;

    for (auto i = strtok(&line[0], " "); i != NULL; i = strtok(NULL, " "))
      tokens.push_back(i);

    for (unsigned i = 0; i < tokens.size(); i++)
      LOGD("t: %s\n", tokens[i].c_str());

    LOGD("\n");

    if (tokens[0] == "+++++") { // Found new configuration start token
      // Mark the start of a new configuration
      readingFirstLine = true;
      continue;
    }

    if (tokens[0] == "-----") { // Found configuration end token
      // Mark the end of current configuration
      continue;
    }

    if (readingFirstLine) {
      // Read first line, to create the new configuration struct
      readingFirstLine = false;
      firstTensorID = 1; // reset first tensor ID for new config

      auto conf =
          Configuration(tokens[0], std::stof(tokens[1]), std::stof(tokens[2]),
                        std::stof(tokens[3]), std::stof(tokens[4]));

      // Check if overall confidence averages exist
      if (tokens.size() >= 8) {
        auto avg = std::stof(tokens[5]);
        auto correctAvg = std::stof(tokens[6]);
        auto wrongAvg = std::stof(tokens[7]);

        conf.addOverallConfidence(avg, correctAvg, wrongAvg);
      }

      // Check if perclass confidence averages exist
      if (tokens.size() > 8) {
        const size_t nlabels = (tokens.size() - 8) / 3;
        LOGD("nlabels=%zd", nlabels);
        for (size_t label = 0, idx = 8; label < nlabels; label++) {
          LOGD("label=%zd idx=%zd", label, idx);

          auto avg = std::stof(tokens[idx++]);
          auto correctAvg = std::stof(tokens[idx++]);
          auto wrongAvg = std::stof(tokens[idx++]);

          conf.addPerclassConfidence(label, avg, correctAvg, wrongAvg);
        }
      }

      LOGD("%s confidence overall: %f %f %f", conf.name.c_str(),
           conf._overall_confidence.avg, conf._overall_confidence.correctAvg,
           conf._overall_confidence.wrongAvg);

      for (int i = 0; i < conf._perclass_confidence.size(); i++) {
        LOGD("%s confidence perclass[%d]: %f %f %f", conf.name.c_str(), i,
             conf._perclass_confidence[i].avg,
             conf._perclass_confidence[i].correctAvg,
             conf._perclass_confidence[i].wrongAvg);
      }

      _InitialConfigurations.push_back(conf);

      continue;
    }

    if (tokens[1] == "gpu") {
      LOGD("Found gpu configuration\n");

      // There must be at least one operation, with an approximation option
      CUSTOM_ASSERT((tokens.size() >= 5) &&
                    "Not enough operations - approximation options.");

      GPUNodeConfiguration *NodeConf = new GPUNodeConfiguration();
      _InitialConfigurations.back().setup.insert(
          std::make_pair(tokens[0], NodeConf));

      // Updating map of visc.node.id ID values to NodeConfigurations
      // FIXME: Do same for CPU and PROMISE configs
      _InitialConfigurations.back().idConfigMap.insert(
          std::make_pair(firstTensorID, NodeConf));
      LOGD("*** firstTensorID = %d \n\n", firstTensorID);

      unsigned idx = 2;
      while (idx < tokens.size()) {
        if (tokens[idx] == "add") {
          LOGD("Found add operation\n");
          NodeConf->pushNewTensorOperation(
              GPUNodeConfiguration::TENSOR_OP::ADD);
          idx++;
        } else if (tokens[idx] == "batchnorm") {
          LOGD("Found batchnorm operation\n");
          NodeConf->pushNewTensorOperation(
              GPUNodeConfiguration::TENSOR_OP::BATCHNORM);
          idx++;
        } else if (tokens[idx] == "conv") {
          LOGD("Found conv operation\n");
          NodeConf->pushNewTensorOperation(
              GPUNodeConfiguration::TENSOR_OP::CONV);
          idx++;
        } else if (tokens[idx] == "group_conv") {
          LOGD("Found group_conv operation\n");
          NodeConf->pushNewTensorOperation(
              GPUNodeConfiguration::TENSOR_OP::GROUP_CONV);
          idx++;
        } else if (tokens[idx] == "mul") {
          LOGD("Found mul operation\n");
          NodeConf->pushNewTensorOperation(
              GPUNodeConfiguration::TENSOR_OP::MUL);
          idx++;
        } else if (tokens[idx] == "relu") {
          LOGD("Found relu operation\n");
          NodeConf->pushNewTensorOperation(
              GPUNodeConfiguration::TENSOR_OP::RELU);
          idx++;
        } else if (tokens[idx] == "clipped_relu") {
          LOGD("Found clipped_relu operation\n");
          NodeConf->pushNewTensorOperation(
              GPUNodeConfiguration::TENSOR_OP::CLIPPED_RELU);
          idx++;
        } else if (tokens[idx] == "tanh") {
          LOGD("Found tanh operation\n");
          NodeConf->pushNewTensorOperation(
              GPUNodeConfiguration::TENSOR_OP::TANH);
          idx++;
        } else if (tokens[idx] == "pool_max") {
          LOGD("Found pool_max operation\n");
          NodeConf->pushNewTensorOperation(
              GPUNodeConfiguration::TENSOR_OP::POOL_MAX);
          idx++;
        } else if (tokens[idx] == "pool_mean") {
          LOGD("Found pool_mean operation\n");
          NodeConf->pushNewTensorOperation(
              GPUNodeConfiguration::TENSOR_OP::POOL_MEAN);
          idx++;
        } else if (tokens[idx] == "pool_min") {
          LOGD("Found pool_min operation\n");
          NodeConf->pushNewTensorOperation(
              GPUNodeConfiguration::TENSOR_OP::POOL_MIN);
          idx++;
        } else if (tokens[idx] == "softmax") {
          LOGD("Found softmax operation\n");
          NodeConf->pushNewTensorOperation(
              GPUNodeConfiguration::TENSOR_OP::SOFTMAX);
          idx++;
        }
        /*Not a new operation. This means an approximation option*/
        else if (tokens[idx] == "fp32") {
          LOGD("Found fp32 option\n");
          int fp32 = std::stoi(tokens[idx + 1]);
          LOGD("fp32 parameter: %d, ignoring\n", fp32);
          NodeConf->pushNewApproximationChoiceForOperation(
              GPUNodeConfiguration::APPROX::FP32, fp32);
          idx += 2;
        } else if (tokens[idx] == "fp16") {
          LOGD("Found fp16 option\n");
          int fp16 = std::stoi(tokens[idx + 1]);
          LOGD("fp16 parameter: %d, ignoring\n", fp16);
          NodeConf->pushNewApproximationChoiceForOperation(
              GPUNodeConfiguration::APPROX::FP16, fp16);
          idx += 2;
        } else if (tokens[idx] == "perf") {
          LOGD("Found perf option\n");
          int perf = std::stoi(tokens[idx + 1]);
          LOGD("perf parameter: %d\n", perf);
          NodeConf->pushNewApproximationChoiceForOperation(
              GPUNodeConfiguration::APPROX::PERFORATION, perf);
          idx += 2;
        } else if (tokens[idx] == "perf_fp16") {
          LOGD("Found perf_fp16 option\n");
          int perf_fp16 = std::stoi(tokens[idx + 1]);
          LOGD("perf_fp16 parameter: %d\n", perf_fp16);
          NodeConf->pushNewApproximationChoiceForOperation(
              GPUNodeConfiguration::APPROX::PERFORATION_HP, perf_fp16);
          idx += 2;
        } else if (tokens[idx] == "samp") {
          LOGD("Found samp option\n");
          int samp = std::stoi(tokens[idx + 1]);
          LOGD("samp parameter: %d\n", samp);
          NodeConf->pushNewApproximationChoiceForOperation(
              GPUNodeConfiguration::APPROX::INPUT_SAMPLING, samp);
          idx += 2;
        } else if (tokens[idx] == "samp_fp16") {
          LOGD("Found samp_fp16 option\n");
          int samp_fp16 = std::stoi(tokens[idx + 1]);
          LOGD("samp_fp16 parameter: %d\n", samp_fp16);
          NodeConf->pushNewApproximationChoiceForOperation(
              GPUNodeConfiguration::APPROX::INPUT_SAMPLING_HP, samp_fp16);
          idx += 2;
        } else if (tokens[idx] == "red_samp") {
          LOGD("Found red_samp option\n");
          int red_samp = std::stoi(tokens[idx + 1]);
          LOGD("red_samp parameter: %d\n", red_samp);
          NodeConf->pushNewApproximationChoiceForOperation(
              GPUNodeConfiguration::APPROX::REDUCTION_SAMPLING, red_samp);
          idx += 2;
        }
        // TODO: other approximation options handled here
      }

      // Update first TensorID using number of tensor ops in current node
      firstTensorID += NodeConf->getApproxChoices().size();
    } else if (tokens[1] == "cpu") {
      INFO("---------Found cpu configuration\n");

      // There must be at least one operation, with an approximation option
      CUSTOM_ASSERT((tokens.size() >= 5) &&
                    "Not enough operations - approximation options.");

      CPUNodeConfiguration *NodeConf = new CPUNodeConfiguration();
      _InitialConfigurations.back().setup.insert(
          std::make_pair(tokens[0], NodeConf));

      _InitialConfigurations.back().idConfigMap.insert(
          std::make_pair(firstTensorID, NodeConf));
      INFO("*** firstTensorID = %d \n\n", firstTensorID);
      INFO("FIXED CPU ID ISSUE\n");
      unsigned idx = 2;
      while (idx < tokens.size()) {
        if (tokens[idx] == "add") {
          DEBUG("Found add operation\n");
          NodeConf->pushNewTensorOperation(
              CPUNodeConfiguration::TENSOR_OP::ADD);
          idx++;
        } else if (tokens[idx] == "batchnorm") {
          DEBUG("Found batchnorm operation\n");
          NodeConf->pushNewTensorOperation(
              CPUNodeConfiguration::TENSOR_OP::BATCHNORM);
          idx++;
        } else if (tokens[idx] == "conv") {
          DEBUG("Found conv operation\n");
          NodeConf->pushNewTensorOperation(
              CPUNodeConfiguration::TENSOR_OP::CONV);
          idx++;
        } else if (tokens[idx] == "group_conv") {
          DEBUG("Found group_conv operation\n");
          NodeConf->pushNewTensorOperation(
              CPUNodeConfiguration::TENSOR_OP::GROUP_CONV);
          idx++;
        } else if (tokens[idx] == "mul") {
          DEBUG("Found mul operation\n");
          NodeConf->pushNewTensorOperation(
              CPUNodeConfiguration::TENSOR_OP::MUL);
          idx++;
        } else if (tokens[idx] == "relu") {
          DEBUG("Found relu operation\n");
          NodeConf->pushNewTensorOperation(
              CPUNodeConfiguration::TENSOR_OP::RELU);
          idx++;
        } else if (tokens[idx] == "clipped_relu") {
          DEBUG("Found clipped_relu operation\n");
          NodeConf->pushNewTensorOperation(
              CPUNodeConfiguration::TENSOR_OP::CLIPPED_RELU);
          idx++;
        } else if (tokens[idx] == "tanh") {
          DEBUG("Found tanh operation\n");
          NodeConf->pushNewTensorOperation(
              CPUNodeConfiguration::TENSOR_OP::TANH);
          idx++;
        } else if (tokens[idx] == "pool_max") {
          DEBUG("Found pool_max operation\n");
          NodeConf->pushNewTensorOperation(
              CPUNodeConfiguration::TENSOR_OP::POOL_MAX);
          idx++;
        } else if (tokens[idx] == "pool_mean") {
          DEBUG("Found pool_mean operation\n");
          NodeConf->pushNewTensorOperation(
              CPUNodeConfiguration::TENSOR_OP::POOL_MEAN);
          idx++;
        } else if (tokens[idx] == "pool_min") {
          DEBUG("Found pool_min operation\n");
          NodeConf->pushNewTensorOperation(
              CPUNodeConfiguration::TENSOR_OP::POOL_MIN);
          idx++;
        } else if (tokens[idx] == "softmax") {
          DEBUG("Found softmax operation\n");
          NodeConf->pushNewTensorOperation(
              CPUNodeConfiguration::TENSOR_OP::SOFTMAX);
          idx++;
        } else /*Not a new operation. This means an approximation option*/
            if (tokens[idx] == "fp32") {
          DEBUG("Found fp32 option\n");
          int fp32 = std::stoi(tokens[idx + 1]);
          DEBUG("fp32 parameter: %d, ignoring\n", fp32);
          NodeConf->pushNewApproximationChoiceForOperation(
              CPUNodeConfiguration::APPROX::FP32, fp32);
          idx += 2;
        } else if (tokens[idx] == "perf") {
          DEBUG("Found perf option\n");
          int perf = std::stoi(tokens[idx + 1]);
          DEBUG("perf parameter: %d\n", perf);
          NodeConf->pushNewApproximationChoiceForOperation(
              CPUNodeConfiguration::APPROX::PERFORATION, perf);
          idx += 2;
        } else if (tokens[idx] == "samp") {
          DEBUG("Found samp option\n");
          int samp = std::stoi(tokens[idx + 1]);
          DEBUG("samp parameter: %d\n", samp);
          NodeConf->pushNewApproximationChoiceForOperation(
              CPUNodeConfiguration::APPROX::INPUT_SAMPLING, samp);
          idx += 2;
        }
        // TODO: other approximation options handled here
      }
      firstTensorID += NodeConf->getApproxChoices().size();
    } else {
      LOGW("Invalid Configuration File\n");
      _InitialConfigurations.clear();
      return;
    }
  }

  LOGD("DONE.\n");
}

void RtController::setConfigIndex(size_t idx) {
  _configuration_idx = std::max(
      static_cast<size_t>(0), std::min(idx, _InitialConfigurations.size() - 1));
}

size_t RtController::getConfigIndex() { return _configuration_idx; }

const Configuration &RtController::getConfig(size_t idx) {
  return _InitialConfigurations[idx];
}
const Configuration &RtController::getCurrentConfig() {
  return getConfig(getConfigIndex());
}

void RtController::reset() {
  _accuracy.reset();
  _perclass_accuracy.clear();

  _correctness_predictor_accuracy.reset();
  _correctness_predictor_perclass_accuracy.clear();

  _confidence_tracker.reset();
  _perclass_confidence_trackers.clear();
}

NodeConfiguration *RtController::getNodeConf(const char *node_id) {
  // if visc.node.id Not specified for this HPVM Node
  LOGD("*** using configuration %s ***",
       _InitialConfigurations[_configuration_idx].name.c_str());
  if (rtCtx->get_currentTensorID() == -1) {
    std::string s(node_id);
    // All nodes are expected to have a configuration
    return _InitialConfigurations[_configuration_idx].setup.at(s);
  } else {
    DEBUG("-- currentTensorID = \%u \n", rtCtx->get_currentTensorID());
    return _InitialConfigurations[_configuration_idx].idConfigMap.at(
        rtCtx->get_currentTensorID());
  }
}

void RtController::generate_report_accuracy() {
  try {
    std::ofstream out("final_accuracy");
    out << _accuracy.accuracy() * 100 << std::endl;

  } catch (...) {
    LOGD("Error while trying to generate final_accuracy");
  }
}

float safe_div(float a, int b) {
  if (b == 0) {
    return 0;
  } else {
    return a / b;
  }
}

void RtController::generate_report_confidence() {
  using std::endl;
  try {
    std::ofstream out("final_confidence");
    out << "confidenceTotalAvg " << _confidence_tracker.avg_all() << endl;
    out << "confidenceCorrectAvg " << _confidence_tracker.avg_correct() << endl;
    out << "confidenceWrongAvg " << _confidence_tracker.avg_wrong() << endl;

    for (int i = 0; i < _perclass_confidence_trackers.size(); i++) {
      auto &tracker = _perclass_confidence_trackers[i];

      out << i << ".confidenceTotalAvg " << tracker.avg_all() << endl;
      out << i << ".confidenceCorrectAvg " << tracker.avg_correct() << endl;
      out << i << ".confidenceWrongAvg " << tracker.avg_wrong() << endl;

      out << i << ".allCount " << tracker.n_all() << endl;
      out << i << ".correctCount " << tracker.n_correct() << endl;
      out << i << ".wrongCount " << tracker.n_wrong() << endl;
    }

  } catch (...) {
    LOGD("Error while trying to generate final_confidence");
  }

  try {
    std::ofstream out("final_correctness_prediction");
    out << "predictorTPTN " << _correctness_predictor_accuracy.accuracy()
        << endl;
    out << "predictorTP " << _correctness_predictor_accuracy_true.accuracy()
        << endl;
    out << "predictorTN " << _correctness_predictor_accuracy_false.accuracy()
        << endl;

    for (int i = 0; i < _correctness_predictor_perclass_accuracy.size(); i++) {
      auto &tracker = _correctness_predictor_perclass_accuracy[i];
      auto &tracker_true = _correctness_predictor_perclass_accuracy_true[i];
      auto &tracker_false = _correctness_predictor_perclass_accuracy_false[i];

      out << i << ".predictorTPTN " << tracker.accuracy() << endl;
      out << i << ".predictorTP " << tracker_true.accuracy() << endl;
      out << i << ".predictorTN " << tracker_false.accuracy() << endl;
    }

  } catch (...) {
    LOGD("Error while trying to generate final_confidence");
  }
}

//
//
// ConfidenceTracker
//

ConfidenceTracker::ConfidenceTracker() { reset(); }

void ConfidenceTracker::reset() {
  _sum_correct = 0;
  _count_correct = 0;
  _sum_all = 0;
  _count_all = 0;
}

void ConfidenceTracker::push_correct(float confidence) {
  _sum_correct += confidence;
  _count_correct++;
}

void ConfidenceTracker::push_total(float confidence) {
  _sum_all += confidence;
  _count_all++;
}

size_t ConfidenceTracker::n_correct() { return _count_correct; }

size_t ConfidenceTracker::n_wrong() { return _count_all - _count_correct; }

size_t ConfidenceTracker::n_all() { return _count_all; }

float ConfidenceTracker::avg_correct() {
  if (n_correct() == 0) {
    return 0;
  }

  return _sum_correct / (float)n_correct();
}

float ConfidenceTracker::avg_wrong() {
  float sum_wrong = _sum_all - _sum_correct;

  if (n_wrong() == 0) {
    return 0;
  }

  return sum_wrong / (float)n_wrong();
}

float ConfidenceTracker::avg_all() {
  if (n_all() == 0) {
    return 0;
  }

  return _sum_all / (float)n_all();
}

//
//
// AccuracyTracker
//

AccuracyTracker::AccuracyTracker() { reset(); }

void AccuracyTracker::reset() {
  _count_correct = 0;
  _count_all = 0;
}

void AccuracyTracker::add_prediction(bool correct) {
  if (correct) {
    _count_correct++;
  }
  _count_all++;
}

float AccuracyTracker::accuracy() {
  if (_count_all == 0) {
    return -1;
  }

  return (float)_count_correct / (float)_count_all;
}

//
//
// Model logging
//

void RtController::model_logger_init() {
  _model_log.open("model_log", std::ios::out | std::ios::app);
  _model_log << "begin" << std::endl;
}

void RtController::model_logger_cleanup() {
  _model_log << "end" << std::endl;
  _model_log.close();
}

void RtController::model_log(RtController::LabelType true_label,
                             RtController::LabelType predicted_label,
                             const std::vector<float> &confidence) {
  _model_log << getCurrentConfig().name << " ";
  _model_log << true_label << " ";
  _model_log << predicted_label;
  for (const auto &c : confidence) {
    _model_log << " " << c;
  }
  _model_log << std::endl;
}

void RtController::approximateMore() {
  if (getConfigIndex() < _InitialConfigurations.size() - 1) {
    setConfigIndex(getConfigIndex() + 1);
  } else {
    LOGW("Already at lowest accuracy");
  }
}

void RtController::approximateLess() {
  if (getConfigIndex() > 0) {
    setConfigIndex(getConfigIndex() - 1);
  } else {
    LOGW("Already at highest accuracy");
  }
}
