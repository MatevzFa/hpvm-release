#include "tensor_runtime.h"

#include <android/asset_manager.h>
#include <array>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <stdexcept>
#include <vector>

#include "RtContext.h"
#include "android_util.h"
#include "approx_api.h"
#include "clblast.h"
#include "clblast_rt/op_add_bias.h"
#include "clblast_rt/op_convolution.h"
#include "clblast_rt/op_convolution_approx.h"
#include "clblast_rt/op_gemm.h"
#include "clblast_rt/op_pooling.h"
#include "clblast_rt/tensor.h"
#include "clblast_rt/utility.h"
#include "constants.h"
#include "fmt/ostream.h"
#include "global_data.h"

#include "CL/cl.h"

#include "clblast.h"
#include "clblast_half.h"

#include "helpers.h"
#include "tensor_cpu_runtime.h"

#define PROFILE_EVENT_GPU_COMPUTATION "gpu_computation"

using namespace clblast;
using namespace clblast_rt;

//
// Initialisation, Cleanup
//

void llvm_hpvm_initTensorRt(int gpuid) {
  LOGD(__func__);

  THROW_ERROR_ON_MSG(rtCtx != nullptr, "Failed to initialize tensor rt");

  rtCtx = new RtContext();
}

void llvm_hpvm_cleanupTensorRt() {
  LOGD(__func__);

  THROW_ERROR_ON_MSG(rtCtx == nullptr, "Failed to cleanup tensor rt");
  delete rtCtx;
  rtCtx = nullptr;

  LOGD("end %s", __func__);
}

void llvm_hpvm_initApproxhpvmRt(int gpuid) { llvm_hpvm_initTensorRt(gpuid); }

void llvm_hpvm_cleanupApproxhpvmRt() { llvm_hpvm_cleanupTensorRt(); }

void llvm_hpvm_initializeRuntimeController(const char *config_file) {
  rtCtx->rtController().loadTunerConfs(config_file);
}

void llvm_hpvm_clearRuntimeController() {}

void llvm_hpvm_invokeRtControl(void *result, const char *_labels_path,
                               long start, long end) {
  rtCtx->rtController().invokeRtControl(result, start, end);
}

//
//
//

void *create4DTensor(int data_type, int data_format, size_t dim1_size,
                     size_t dim2_size, size_t dim3_size, size_t dim4_size) {
  auto profile_scope = rtCtx->profile(__func__);

  DataType arm_data_type = data_type_from_internal(data_type);
  Shape shape(dim1_size, dim2_size, dim3_size, dim4_size);

  if (data_format != TENSOR_LAYOUT_NCHW) {
    throw std::runtime_error("unsupported layout");
  }

  auto *tensor = create_tensor(arm_data_type, shape, DataPlacement::DEV);

  return tensor;
}

void initTensorData(void *tensor_ptr, void *data_ptr, size_t size_in_bytes) {
  auto *tensor = reinterpret_cast<Tensor *>(tensor_ptr);

  auto profile_scope = rtCtx->profile("initTensorData");

  host2dev(tensor_ptr);
  half2full(tensor_ptr);

  clChk(clEnqueueWriteBuffer(rtCtx->queue(), tensor->buf_dev().raw(),
                             CL_NON_BLOCKING, 0, size_in_bytes, data_ptr, 0,
                             nullptr, nullptr));
  sync();
}

void tensorCopy(void *src_ptr, void *dst_ptr) {
  auto *src = reinterpret_cast<Tensor *>(src_ptr);
  auto *dst = reinterpret_cast<Tensor *>(dst_ptr);

  auto profile_scope = rtCtx->profile("tensorCopy");

  if (src->is_full_used() && dst->is_full_used()) {
    clChk(clEnqueueCopyBuffer(rtCtx->queue(), src->buf_dev().raw(),
                              dst->buf_dev().raw(), 0, 0, src->buf_dev().size(),
                              0, nullptr, nullptr));
    sync();
  } else if (src->is_half_used() && dst->is_half_used()) {
    clChk(clEnqueueCopyBuffer(rtCtx->queue(), src->buf_dev().raw(),
                              dst->buf_dev().raw(), 0, 0, src->buf_dev().size(),
                              0, nullptr, nullptr));
    sync();
  } else if (src->is_host_used() && dst->is_host_used()) {
    std::memcpy(dst->buf_host().raw(), src->buf_host().raw(),
                src->buf_host().size());
  } else {
    THROW_ERROR_ON_MSG(
        true, "tensorCopy: half/full data mismatch or data not on device");
  }
}

void freeTensor(void *tensor_ptr) {
  auto *tensor = reinterpret_cast<Tensor *>(tensor_ptr);

  auto profile_scope = rtCtx->profile("freeTensor");

  if (tensor->buf_dev().is_allocated()) {
    tensor->buf_dev().release();
  }

  if (tensor->buf_dev_half().is_allocated()) {
    tensor->buf_dev_half().release();
  }

  if (tensor->buf_host().is_allocated()) {
    tensor->buf_host().release();
  }
}

//
//
// ReLU
//

void *tensorRelu(void *input) {
  auto *tensor = reinterpret_cast<Tensor *>(input);

  auto profile_scope = rtCtx->profile(__func__);

  host2dev(tensor);
  half2full(tensor);

  size_t channels = tensor->shape().n() * tensor->shape().c();
  size_t w = tensor->shape().w();
  size_t h = tensor->shape().h();

  // Not needed in ReLU
  float a = 0;
  float b = 0;

  cl_event e;

  clblast::Activation<cl_float>(
      ActivationFunc::RELU, channels, w, h, a, b, tensor->buf_dev().raw(), 0,
      tensor->buf_dev().raw(), 0, &rtCtx->queue(), &e);
  sync();

  rtCtx->profiler().push_cl_event("activation relu", e);
  clChk(clReleaseEvent(e));

  return tensor;
}

void *tensorHalfRelu(void *input) {
#ifdef ALWAYS_FP32
  return tensorRelu(input);
#endif

  auto *tensor = reinterpret_cast<Tensor *>(input);

  auto profile_scope = rtCtx->profile(__func__);

  host2dev(tensor);
  full2half(tensor);

  size_t channels = tensor->shape().n() * tensor->shape().c();
  size_t w = tensor->shape().w();
  size_t h = tensor->shape().h();

  // Not needed in ReLU
  half a = FloatToHalf(a);
  half b = FloatToHalf(b);

  cl_event e;

  clblast::Activation<cl_half>(
      ActivationFunc::RELU, channels, w, h, a, b, tensor->buf_dev_half().raw(),
      0, tensor->buf_dev_half().raw(), 0, &rtCtx->queue(), &e);
  sync();

  rtCtx->profiler().push_cl_event("activation half relu", e);
  clChk(clReleaseEvent(e));

  return tensor;
}

//
//
// Clipped ReLU
//

void *tensorRelu2(void *input, float min, float max) {
  auto *tensor = reinterpret_cast<Tensor *>(input);

  auto profile_scope = rtCtx->profile(__func__);

  host2dev(tensor);
  half2full(tensor);

  size_t channels = tensor->shape().n() * tensor->shape().c();
  size_t w = tensor->shape().w();
  size_t h = tensor->shape().h();

  float a = min;
  float b = max;

  cl_event e;

  clblast::Activation<cl_float>(ActivationFunc::CLIPPED_RELU, channels, w, h, a,
                                b, tensor->buf_dev().raw(), 0,
                                tensor->buf_dev().raw(), 0, &rtCtx->queue(),
                                &e);
  sync();

  rtCtx->profiler().push_cl_event("activation relu2", e);
  clChk(clReleaseEvent(e));

  return tensor;
}

void *tensorHalfRelu2(void *input, float min, float max) {
#ifdef ALWAYS_FP32
  return tensorRelu2(input, min, max);
#endif
  auto *tensor = reinterpret_cast<Tensor *>(input);

  auto profile_scope = rtCtx->profile(__func__);

  host2dev(tensor);
  full2half(tensor);

  size_t channels = tensor->shape().n() * tensor->shape().c();
  size_t w = tensor->shape().w();
  size_t h = tensor->shape().h();

  half a = FloatToHalf(min);
  half b = FloatToHalf(max);

  cl_event e;

  clblast::Activation<cl_half>(ActivationFunc::CLIPPED_RELU, channels, w, h, a,
                               b, tensor->buf_dev_half().raw(), 0,
                               tensor->buf_dev_half().raw(), 0, &rtCtx->queue(),
                               &e);
  sync();

  rtCtx->profiler().push_cl_event("activation half relu2", e);
  clChk(clReleaseEvent(e));

  return tensor;
}

//
//
// Tanh
//

void *tensorTanh(void *input) {
  auto *tensor = reinterpret_cast<Tensor *>(input);

  auto profile_scope = rtCtx->profile(__func__);

  host2dev(tensor);
  half2full(tensor);

  size_t channels = tensor->shape().n() * tensor->shape().c();
  size_t w = tensor->shape().w();
  size_t h = tensor->shape().h();

  // Not needed in TANH
  float a = 0;
  float b = 0;

  cl_event e;

  clblast::Activation<cl_float>(
      ActivationFunc::TANH, channels, w, h, a, b, tensor->buf_dev().raw(), 0,
      tensor->buf_dev().raw(), 0, &rtCtx->queue(), &e);
  sync();

  rtCtx->profiler().push_cl_event("activation tanh", e);
  clChk(clReleaseEvent(e));

  return tensor;
}

void *tensorHalfTanh(void *input) {
#ifdef ALWAYS_FP32
  return tensorTanh(input);
#endif

  auto *tensor = reinterpret_cast<Tensor *>(input);

  auto profile_scope = rtCtx->profile(__func__);

  host2dev(tensor);
  full2half(tensor);

  size_t channels = tensor->shape().n() * tensor->shape().c();
  size_t w = tensor->shape().w();
  size_t h = tensor->shape().h();

  // Not needed in TANH
  half a = FloatToHalf(0);
  half b = FloatToHalf(0);

  cl_event e;

  clblast::Activation<cl_half>(
      ActivationFunc::TANH, channels, w, h, a, b, tensor->buf_dev_half().raw(),
      0, tensor->buf_dev_half().raw(), 0, &rtCtx->queue(), &e);
  sync();

  rtCtx->profiler().push_cl_event("activation half tanh", e);
  clChk(clReleaseEvent(e));

  return tensor;
}

//
//
// Convolution
//

void *tensorConvolution(void *input_ptr, void *filter_ptr, int pad_h, int pad_w,
                        int stride_h, int stride_w, int conv_mode,
                        int conv_groups) {
  if (conv_groups > 1) {
    LOGW("%s: grouped conv: falling back to CPU runtime", __func__);
    return tensorConvCutlassCPU(input_ptr, filter_ptr, pad_h, pad_w, stride_h,
                                stride_w, conv_mode, conv_groups);
  }

  auto *input = reinterpret_cast<Tensor *>(input_ptr);
  auto *filter = reinterpret_cast<Tensor *>(filter_ptr);

  auto profile_scope = rtCtx->profile(__func__);

  host2dev(input);
  host2dev(filter);

  half2full(input);
  half2full(filter);

  LOGD("conv_mode=%d conv_groups=%d", conv_mode, conv_groups);

  DataType dtype = input->type();

  size_t w = input->shape().w();
  size_t h = input->shape().h();
  size_t c = input->shape().c();
  size_t n = input->shape().n();

  size_t kw = filter->shape().w();
  size_t kh = filter->shape().h();
  size_t kn = filter->shape().n();

  size_t conv_h = conv_size(h, kh, 2 * pad_h, stride_h);
  size_t conv_w = conv_size(w, kw, 2 * pad_w, stride_w);

  Shape output_shape(n, kn, conv_h, conv_w);

  Tensor *output = create_tensor(dtype, output_shape, DataPlacement::DEV);

  size_t workspace_size;
  clblast_rt::convolution(rtCtx->queue(), *input, *filter, *output, nullptr,
                          stride_w, stride_h, pad_w, pad_h, &workspace_size);
  BufferDev workspace(workspace_size);
  workspace.allocate(rtCtx->context(), CL_MEM_HOST_NO_ACCESS);

  cl_event e_im2col = nullptr;
  cl_event e_gemm = nullptr;

  clblast_rt::convolution(rtCtx->queue(), *input, *filter, *output,
                          workspace.raw(), stride_w, stride_h, pad_w, pad_h,
                          nullptr, &e_im2col, &e_gemm);

  sync();

  rtCtx->profiler().push_cl_event("convolution im2col", e_im2col);
  rtCtx->profiler().push_cl_event("convolution gemm", e_gemm);

  workspace.release();

  if (e_im2col != nullptr)
    clChk(clReleaseEvent(e_im2col));
  if (e_gemm != nullptr)
    clChk(clReleaseEvent(e_gemm));

#ifdef COMPARE_WITH_CPU
  if (!compare_tensors(output,
                       tensorConvCutlassCPU(input_ptr, filter_ptr, pad_h, pad_w,
                                            stride_h, stride_w, conv_mode,
                                            conv_groups),
                       1e-4)) {
    LOGE("WRONG   %s pad_h=%d pad_w=%d stride_h=%d stride_w=%d", __func__,
         pad_h, pad_w, stride_h, stride_w);
  } else {
    LOGE("CORRECT %s pad_h=%d pad_w=%d stride_h=%d stride_w=%d", __func__,
         pad_h, pad_w, stride_h, stride_w);
  }
#endif

  return output;
}

void *tensorHalfConvolution(void *input_ptr, void *filter_ptr, int pad_h,
                            int pad_w, int stride_h, int stride_w,
                            int conv_mode, int conv_groups) {
#ifdef ALWAYS_FP32
  return tensorConvolution(input_ptr, filter_ptr, pad_h, pad_w, stride_h,
                           stride_w, conv_mode, conv_groups);
#endif

#ifdef COMPARE_WITH_CPU
  return tensorConvolution(input_ptr, filter_ptr, pad_h, pad_w, stride_h,
                           stride_w, conv_mode, conv_groups);
#endif

  if (conv_groups > 1) {
    LOGW("%s: grouped conv: falling back to CPU runtime", __func__);
    return tensorConvCutlassCPU(input_ptr, filter_ptr, pad_h, pad_w, stride_h,
                                stride_w, conv_mode, conv_groups);
  }

  auto *input = reinterpret_cast<Tensor *>(input_ptr);
  auto *filter = reinterpret_cast<Tensor *>(filter_ptr);

  auto profile_scope = rtCtx->profile(__func__);

  host2dev(input);
  host2dev(filter);

  full2half(input);
  full2half(filter);

  LOGD("conv_mode=%d conv_groups=%d", conv_mode, conv_groups);

  DataType dtype = input->type_half();

  size_t w = input->shape().w();
  size_t h = input->shape().h();
  size_t c = input->shape().c();
  size_t n = input->shape().n();

  size_t kw = filter->shape().w();
  size_t kh = filter->shape().h();
  size_t kn = filter->shape().n();

  size_t conv_h = conv_size(h, kh, 2 * pad_h, stride_h);
  size_t conv_w = conv_size(w, kw, 2 * pad_w, stride_w);

  Shape output_shape(n, kn, conv_h, conv_w);

  Tensor *output = create_tensor(dtype, output_shape, DataPlacement::DEV_HALF);

  size_t workspace_size;
  clblast_rt::convolution_half(rtCtx->queue(), *input, *filter, *output,
                               nullptr, stride_w, stride_h, pad_w, pad_h,
                               &workspace_size);
  BufferDev workspace(workspace_size);
  workspace.allocate(rtCtx->context(), CL_MEM_HOST_NO_ACCESS);

  cl_event e_im2col = nullptr;
  cl_event e_gemm = nullptr;

  clblast_rt::convolution_half(rtCtx->queue(), *input, *filter, *output,
                               workspace.raw(), stride_w, stride_h, pad_w,
                               pad_h, nullptr, &e_im2col, &e_gemm);

  sync();

  rtCtx->profiler().push_cl_event("convolution half im2col", e_im2col);
  rtCtx->profiler().push_cl_event("convolution half gemm", e_gemm);

  if (e_im2col != nullptr)
    clChk(clReleaseEvent(e_im2col));
  if (e_gemm != nullptr)
    clChk(clReleaseEvent(e_gemm));

  workspace.release();

  return output;
}

//
//
// GEMM
//

void *tensorGemmGPU(void *lhs_ptr, void *rhs_ptr) {

  auto *lhs = reinterpret_cast<Tensor *>(lhs_ptr);
  auto *rhs = reinterpret_cast<Tensor *>(rhs_ptr);

  auto profile_scope = rtCtx->profile(__func__);

  host2dev(lhs);
  host2dev(rhs);

  half2full(lhs);
  half2full(rhs);

  // Determine K of matrix multiplication

  size_t n = lhs->shape().n();

  size_t mm_k = rhs->shape().h();
  size_t mm_n = rhs->shape().w();
  size_t mm_m = lhs->shape().area() / mm_k;

  assert(n == mm_m);

  Shape out_shape(mm_m, mm_n, 1, 1);
  Tensor *output = create_tensor(lhs->type(), out_shape, DataPlacement::DEV);

  cl_event e;

  Gemm<cl_float>(Layout::kRowMajor, Transpose::kNo, Transpose::kNo, mm_m, mm_n,
                 mm_k, 1.0, lhs->buf_dev().raw(), 0, mm_k, rhs->buf_dev().raw(),
                 0, mm_n, 0.0, output->buf_dev().raw(), 0, mm_n,
                 &rtCtx->queue(), &e);
  sync();

  rtCtx->profiler().push_cl_event("gemm", e);
  clChk(clReleaseEvent(e));

  return output;
}

void *tensorHalfGemmGPU(void *lhs_ptr, void *rhs_ptr) {
#ifdef ALWAYS_FP32
  return tensorGemmGPU(lhs_ptr, rhs_ptr);
#endif

  LOGW("%s: falling back to FP32 variant because of potential CLBlast bugs",
       __func__);
  return tensorGemmGPU(lhs_ptr, rhs_ptr);

  // return tensorGemmGPU(lhs_ptr, rhs_ptr);

  auto *lhs = reinterpret_cast<Tensor *>(lhs_ptr);
  auto *rhs = reinterpret_cast<Tensor *>(rhs_ptr);

  auto profile_scope = rtCtx->profile(__func__);

  host2dev(lhs);
  host2dev(rhs);

  full2half(lhs);
  full2half(rhs);

  // Determine K of matrix multiplication

  size_t n = lhs->shape().n();

  size_t mm_k = rhs->shape().h();
  size_t mm_n = rhs->shape().w();
  size_t mm_m = lhs->shape().area() / mm_k;

  assert(n == mm_m);

  Shape out_shape(mm_m, mm_n, 1, 1);
  Tensor *output =
      create_tensor(lhs->type_half(), out_shape, DataPlacement::DEV_HALF);

  half alpha = FloatToHalf(1.0);
  half beta = FloatToHalf(0.0);

  cl_event e;

  Gemm<half>(Layout::kRowMajor, Transpose::kNo, Transpose::kNo, mm_m, mm_n,
             mm_k, alpha, lhs->buf_dev_half().raw(), 0, mm_k,
             rhs->buf_dev_half().raw(), 0, mm_n, beta,
             output->buf_dev_half().raw(), 0, mm_n, &rtCtx->queue(), &e);
  sync();

  rtCtx->profiler().push_cl_event("gemm half", e);
  clChk(clReleaseEvent(e));

  return output;
}

//
//
// Regular Addition
//

void *tensorAddRegular(void *a_ptr, void *b_ptr) {
  auto *a = reinterpret_cast<Tensor *>(a_ptr);
  auto *b = reinterpret_cast<Tensor *>(b_ptr);

  auto profile_scope = rtCtx->profile(__func__);

  host2dev(a);
  host2dev(b);

  half2full(a);
  half2full(b);

  assert(a->shape().area() == b->shape().area() && "Shape mismatch");
  assert(a->shape().n() == b->shape().n() && "Shape mismatch");
  assert(a->shape().c() == b->shape().c() && "Shape mismatch");
  assert(a->shape().h() == b->shape().h() && "Shape mismatch");
  assert(a->shape().w() == b->shape().w() && "Shape mismatch");

  size_t n = a->shape().n();
  size_t c = a->shape().c();
  size_t h = a->shape().h();
  size_t w = a->shape().w();

  cl_event e;

  blastChk(Add<float>(n, c, h, w, a->buf_dev().raw(), 0, b->buf_dev().raw(), 0,
                      a->buf_dev().raw(), 0, &rtCtx->queue(), &e));
  sync();

  rtCtx->profiler().push_cl_event("add", e);

  clReleaseEvent(e);

  return a;
}

void *tensorHalfAddRegular(void *a_ptr, void *b_ptr) {
  auto *a = reinterpret_cast<Tensor *>(a_ptr);
  auto *b = reinterpret_cast<Tensor *>(b_ptr);

  auto profile_scope = rtCtx->profile(__func__);

  host2dev(a);
  host2dev(b);

  full2half(a);
  full2half(b);

  assert(a->shape().area() == b->shape().area() && "Shape mismatch");
  assert(a->shape().n() == b->shape().n() && "Shape mismatch");
  assert(a->shape().c() == b->shape().c() && "Shape mismatch");
  assert(a->shape().h() == b->shape().h() && "Shape mismatch");
  assert(a->shape().w() == b->shape().w() && "Shape mismatch");

  size_t n = a->shape().n();
  size_t c = a->shape().c();
  size_t h = a->shape().h();
  size_t w = a->shape().w();

  cl_event e;

  blastChk(Add<half>(n, c, h, w, a->buf_dev_half().raw(), 0,
                     b->buf_dev_half().raw(), 0, a->buf_dev_half().raw(), 0,
                     &rtCtx->queue(), &e));
  sync();

  rtCtx->profiler().push_cl_event("add half", e);

  clReleaseEvent(e);

  return a;
}

//
//
// Bias
//

void *tensorAdd(void *x_ptr, void *bias_ptr) {
  auto *x = reinterpret_cast<Tensor *>(x_ptr);
  auto *bias = reinterpret_cast<Tensor *>(bias_ptr);

  if (x->shape().n() == bias->shape().n() &&
      x->shape().c() == bias->shape().c() &&
      x->shape().h() == bias->shape().h() &&
      x->shape().w() == bias->shape().w()) {
    return tensorAddRegular(x, bias);
  }

  auto profile_scope = rtCtx->profile(__func__);

  host2dev(x);
  host2dev(bias);

  half2full(x);
  half2full(bias);

  size_t n_total = x->shape().area();
  size_t n_batches = x->shape().n();
  size_t channels = bias->shape().area();
  size_t per_channel = n_total / n_batches / channels;

  LOGD("per_channel=%zd channels=%zd n_batches=%zd\n", per_channel, channels,
       n_batches);

  cl_event e;

  blastChk(AddBias<float>(n_total, channels, per_channel, x->buf_dev().raw(), 0,
                          x->buf_dev().raw(), 0, bias->buf_dev().raw(), 0,
                          &rtCtx->queue(), &e));
  sync();

  rtCtx->profiler().push_cl_event("bias", e);

  clReleaseEvent(e);

  return x;
}

void *tensorHalfAdd(void *x_ptr, void *bias_ptr) {
#ifdef ALWAYS_FP32
  return tensorAdd(x_ptr, bias_ptr);
#endif

  auto *x = reinterpret_cast<Tensor *>(x_ptr);
  auto *bias = reinterpret_cast<Tensor *>(bias_ptr);

  if (x->shape().n() == bias->shape().n() &&
      x->shape().c() == bias->shape().c() &&
      x->shape().h() == bias->shape().h() &&
      x->shape().w() == bias->shape().w()) {
    return tensorHalfAddRegular(x, bias);
  }

  auto profile_scope = rtCtx->profile(__func__);

  host2dev(x);
  host2dev(bias);

  full2half(x);
  full2half(bias);

  size_t n_total = x->shape().area();
  size_t n_batches = x->shape().n();
  size_t channels = bias->shape().area();
  size_t per_channel = n_total / n_batches / channels;

  LOGD("per_channel=%zd channels=%zd n_batches=%zd\n", per_channel, channels,
       n_batches);

  cl_event e;

  blastChk(AddBias<half>(n_total, channels, per_channel,
                         x->buf_dev_half().raw(), 0, x->buf_dev_half().raw(), 0,
                         bias->buf_dev_half().raw(), 0, &rtCtx->queue(), &e));
  sync();

  rtCtx->profiler().push_cl_event("bias half", e);

  clReleaseEvent(e);

  return x;
}

/**
 * @param[in] input           Input matrix
 * @param[in] poolFunction    0 = max pooling, 1 = avg pooling

 * @returns Pooled output matrix
 */
void *tensorPooling(void *input_ptr, int poolFunction, int pool_h, int pool_w,
                    int pad_h, int pad_w, int stride_h, int stride_w) {

  auto *input = reinterpret_cast<Tensor *>(input_ptr);

  host2dev(input);

  half2full(input);

  auto profile_scope = rtCtx->profile(__func__);

  DataType dtype = input->type();

  size_t n = input->shape().n();
  size_t c = input->shape().c();

  size_t conv_h = conv_size(input->shape().h(), pool_h, 2 * pad_h, stride_h);
  size_t conv_w = conv_size(input->shape().w(), pool_w, 2 * pad_w, stride_w);

  Shape output_shape(n, c, conv_h, conv_w);

  Tensor *output = create_tensor(dtype, output_shape, DataPlacement::DEV);

  cl_event e;

  clblast_rt::pooling(rtCtx->queue(), pool_mode_from_internal(poolFunction),
                      *input, *output, pool_h, pool_w, pad_h, pad_w, stride_h,
                      stride_w, &e);
  sync();

  rtCtx->profiler().push_cl_event("pooling", e);
  clChk(clReleaseEvent(e));

#ifdef COMPARE_WITH_CPU
  if (!compare_tensors(output,
                       tensorPoolingCPU(input_ptr, poolFunction, pool_h, pool_w,
                                        pad_h, pad_w, stride_h, stride_w),
                       1e-4)) {
    LOGE("WRONG   %s pool_h=%d pool_w=%d", __func__, pool_h, pool_w);
  } else {
    LOGE("CORRECT %s pool_h=%d pool_w=%d", __func__, pool_h, pool_w);
  }
#endif

  return output;
}

void *tensorHalfPooling(void *input_ptr, int poolFunction, int pool_h,
                        int pool_w, int pad_h, int pad_w, int stride_h,
                        int stride_w) {
#ifdef ALWAYS_FP32
  return tensorPooling(input_ptr, poolFunction, pool_h, pool_w, pad_h, pad_w,
                       stride_h, stride_w);
#endif

#ifdef COMPARE_WITH_CPU
  return tensorPooling(input_ptr, poolFunction, pool_h, pool_w, pad_h, pad_w,
                       stride_h, stride_w);
#endif

  auto *input = reinterpret_cast<Tensor *>(input_ptr);

  host2dev(input);

  full2half(input);

  auto profile_scope = rtCtx->profile(__func__);

  DataType dtype = input->type_half();

  size_t n = input->shape().n();
  size_t c = input->shape().c();

  size_t conv_h = conv_size(input->shape().h(), pool_h, 2 * pad_h, stride_h);
  size_t conv_w = conv_size(input->shape().w(), pool_w, 2 * pad_w, stride_w);

  Shape output_shape(n, c, conv_h, conv_w);

  Tensor *output = create_tensor(dtype, output_shape, DataPlacement::DEV_HALF);

  cl_event e;

  clblast_rt::pooling_half(
      rtCtx->queue(), pool_mode_from_internal(poolFunction), *input, *output,
      pool_h, pool_w, pad_h, pad_w, stride_h, stride_w, &e);
  sync();

  rtCtx->profiler().push_cl_event("pooling half", e);
  clChk(clReleaseEvent(e));

  return output;
}

//
//
// SoftMax
//

void *tensorSoftmax(void *input_ptr) {
  auto profile_scope = rtCtx->profile(__func__);

  auto *input = reinterpret_cast<Tensor *>(input_ptr);

  half2full(input);
  dev2host(input);

  size_t nbatches = input->shape().n();
  size_t nclasses = input->shape().area() / nbatches;

  float *buf = reinterpret_cast<float *>(input->buf_host().raw());

  for (int b = 0; b < nbatches; b++) {

    float max_val = buf[b * nclasses];
    for (int c = 0; c < nclasses; c++) {
      if (buf[b * nclasses + c] > max_val) {
        max_val = buf[b * nclasses + c];
      }
    }

    float sum = 0;
    for (int c = 0; c < nclasses; c++) {
      buf[b * nclasses + c] -= max_val;
      buf[b * nclasses + c] = exp(buf[b * nclasses + c]);
      sum += buf[b * nclasses + c];
    }
    for (int c = 0; c < nclasses; c++) {
      buf[b * nclasses + c] = buf[b * nclasses + c] / sum;
    }
  }

  return input;
}

//
//
// ArgMax
//
void *tensorArgMax(void *input_ptr) {

  Tensor *input = (Tensor *)input_ptr;

  half2full(input);
  dev2host(input);

  size_t nbatches = input->shape().n();
  size_t nclasses = input->shape().area() / nbatches;

  Shape output_shape(nbatches, 1, 1, 1);
  Tensor *output =
      create_tensor(DataType::I32, output_shape, DataPlacement::HOST);

  float *host_ptr = (float *)input->buf_host().raw();
  int *out_ptr = (int *)output->buf_host().raw();

  for (int i = 0; i < nbatches; i++) {

    int start = i * nclasses;
    int max_index = 0;
    float max_val = host_ptr[start];
    for (int j = 0; j < nclasses; j++) {

      int index = start + j;
      // printf ("index = %d \n", index);
      float val = host_ptr[index];
      if (val > max_val) {
        max_val = val;
        max_index = j;
      }
    }

    out_ptr[i] = max_index;
  }

  return output;
}

//
//
// Batch Normalization
//

void *tensorBatchNorm(void *input_ptr, void *gamma_ptr, void *beta_ptr,
                      void *mean_ptr, void *variance_ptr, double epsilon) {
  auto *input = reinterpret_cast<Tensor *>(input_ptr);
  auto *gamma = reinterpret_cast<Tensor *>(gamma_ptr);
  auto *beta = reinterpret_cast<Tensor *>(beta_ptr);
  auto *mean = reinterpret_cast<Tensor *>(mean_ptr);
  auto *variance = reinterpret_cast<Tensor *>(variance_ptr);

  auto profile_scope = rtCtx->profile(__func__);

  host2dev(input);
  host2dev(gamma);
  host2dev(beta);
  host2dev(mean);
  host2dev(variance);

  half2full(input);
  half2full(gamma);
  half2full(beta);
  half2full(mean);
  half2full(variance);

  float eps_cast = (float)epsilon;

  size_t n_total = input->shape().area();
  size_t n_batches = input->shape().n();
  size_t channels = mean->shape().area();
  size_t per_channel = n_total / n_batches / channels;

  LOGD("per_channel=%zd channels=%zd n_batches=%zd\n", per_channel, channels,
       n_batches);

  cl_event e;

  blastChk(BatchNormalization<float>(n_total, channels, per_channel, //
                                     input->buf_dev().raw(), 0,      //
                                     input->buf_dev().raw(), 0,      //
                                     mean->buf_dev().raw(), 0,       //
                                     variance->buf_dev().raw(), 0,   //
                                     beta->buf_dev().raw(), 0,       //
                                     gamma->buf_dev().raw(), 0,      //
                                     eps_cast, &rtCtx->queue(), &e));
  sync();

  rtCtx->profiler().push_cl_event("batchnorm", e);

  clReleaseEvent(e);

  return input;
}

void *tensorHalfBatchNorm(void *input_ptr, void *gamma_ptr, void *beta_ptr,
                          void *mean_ptr, void *variance_ptr, double epsilon) {
#ifdef ALWAYS_FP32
  return tensorBatchNorm(input_ptr, gamma_ptr, beta_ptr, mean_ptr, variance_ptr,
                         epsilon);
#endif

  auto *input = reinterpret_cast<Tensor *>(input_ptr);
  auto *gamma = reinterpret_cast<Tensor *>(gamma_ptr);
  auto *beta = reinterpret_cast<Tensor *>(beta_ptr);
  auto *mean = reinterpret_cast<Tensor *>(mean_ptr);
  auto *variance = reinterpret_cast<Tensor *>(variance_ptr);

  auto profile_scope = rtCtx->profile(__func__);

  host2dev(input);
  host2dev(gamma);
  host2dev(beta);
  host2dev(mean);
  host2dev(variance);

  full2half(input);
  full2half(gamma);
  full2half(beta);
  full2half(mean);
  full2half(variance);

  half eps_cast = FloatToHalf((float)epsilon);

  size_t n_total = input->shape().area();
  size_t n_batches = input->shape().n();
  size_t channels = mean->shape().area();
  size_t per_channel = n_total / n_batches / channels;

  LOGD("per_channel=%zd channels=%zd n_batches=%zd\n", per_channel, channels,
       n_batches);

  cl_event e;

  blastChk(BatchNormalization<half>(n_total, channels, per_channel,    //
                                    input->buf_dev_half().raw(), 0,    //
                                    input->buf_dev_half().raw(), 0,    //
                                    mean->buf_dev_half().raw(), 0,     //
                                    variance->buf_dev_half().raw(), 0, //
                                    beta->buf_dev_half().raw(), 0,     //
                                    gamma->buf_dev_half().raw(), 0,    //
                                    eps_cast, &rtCtx->queue(), &e));
  sync();

  rtCtx->profiler().push_cl_event("batchnorm half", e);

  clReleaseEvent(e);

  return input;
}

//
//
// Approximated Convolution
//
//

void *tensorConvApprox(void *input_ptr, void *filter_ptr, int pad_h, int pad_w,
                       int stride_h, int stride_w, int conv_mode,
                       int conv_groups, int row, int col, int skip_every,
                       int offset) {

  if (row < 2 && col < 2 && skip_every < 2) {
    return tensorConvolution(input_ptr, filter_ptr, pad_h, pad_w, stride_h,
                             stride_w, conv_mode, conv_groups);
  }

  if (conv_groups > 1) {
    LOGW("%s: grouped conv: falling back to CPU runtime", __func__);
    return tensorConvApproxCPU(input_ptr, filter_ptr, pad_h, pad_w, stride_h,
                               stride_w, conv_mode, conv_groups, row, col,
                               skip_every, offset);
  }

  auto *input = reinterpret_cast<Tensor *>(input_ptr);
  auto *filter = reinterpret_cast<Tensor *>(filter_ptr);

  auto profile_scope = rtCtx->profile(__func__);

  host2dev(input);
  host2dev(filter);

  half2full(input);
  half2full(filter);

  LOGD("conv_mode=%d conv_groups=%d", conv_mode, conv_groups);

  DataType dtype = input->type();

  size_t w = input->shape().w();
  size_t h = input->shape().h();
  size_t c = input->shape().c();
  size_t n = input->shape().n();

  size_t kw = filter->shape().w();
  size_t kh = filter->shape().h();
  size_t kn = filter->shape().n();

  size_t conv_h = conv_size(h, kh, 2 * pad_h, stride_h);
  size_t conv_w = conv_size(w, kw, 2 * pad_w, stride_w);

  Shape output_shape(n, kn, conv_h, conv_w);

  Tensor *output = create_tensor(dtype, output_shape, DataPlacement::DEV);

  clblast_rt::ApproxInfo approx_info =
      approx_mode_from_internal(row, col, skip_every, offset);

  size_t workspace_size;
  clblast_rt::convolution_approx(rtCtx->queue(), *input, *filter, *output,
                                 nullptr, stride_w, stride_h, pad_w, pad_h,
                                 approx_info, &workspace_size);
  BufferDev workspace(workspace_size);
  workspace.allocate(rtCtx->context(), CL_MEM_HOST_NO_ACCESS);

  cl_event e_kn2row = nullptr;
  cl_event e_im2col = nullptr;
  cl_event e_gemm = nullptr;
  cl_event e_interpolate = nullptr;

  clblast_rt::convolution_approx(rtCtx->queue(), *input, *filter, *output,
                                 workspace.raw(), stride_w, stride_h, pad_w,
                                 pad_h, approx_info, nullptr, &e_kn2row,
                                 &e_im2col, &e_gemm, &e_interpolate);
  sync();

  rtCtx->profiler().push_cl_event("convolution approx kn2row", e_kn2row);
  rtCtx->profiler().push_cl_event("convolution approx im2col", e_im2col);
  rtCtx->profiler().push_cl_event("convolution approx gemm", e_gemm);
  rtCtx->profiler().push_cl_event("convolution approx interpolate",
                                  e_interpolate);

  if (e_kn2row != nullptr)
    clChk(clReleaseEvent(e_kn2row));
  if (e_im2col != nullptr)
    clChk(clReleaseEvent(e_im2col));
  if (e_gemm != nullptr)
    clChk(clReleaseEvent(e_gemm));
  if (e_interpolate != nullptr)
    clChk(clReleaseEvent(e_interpolate));

  workspace.release();

#ifdef COMPARE_WITH_CPU
  if (!compare_tensors(output,
                       tensorConvApproxCPU(input_ptr, filter_ptr, pad_h, pad_w,
                                           stride_h, stride_w, conv_mode,
                                           conv_groups, row, col, skip_every,
                                           offset),
                       1e-4)) {
    LOGE("WRONG   %s row=%d col=%d skip_every=%d offset=%d", __func__, row, col,
         skip_every, offset);
  } else {
    LOGE("CORRECT %s row=%d col=%d skip_every=%d offset=%d", __func__, row, col,
         skip_every, offset);
  }
#endif

  return output;
}

void *tensorConvApproxHalf(void *input_ptr, void *filter_ptr, int pad_h,
                           int pad_w, int stride_h, int stride_w, int conv_mode,
                           int conv_groups, int row, int col, int skip_every,
                           int offset) {
  return tensorConvApproxHalf2(input_ptr, filter_ptr, pad_h, pad_w, stride_h,
                               stride_w, conv_mode, conv_groups, row, col,
                               skip_every, offset);
}

void *tensorConvApproxHalf2(void *input_ptr, void *filter_ptr, int pad_h,
                            int pad_w, int stride_h, int stride_w,
                            int conv_mode, int conv_groups, int row, int col,
                            int skip_every, int offset) {
#ifdef ALWAYS_FP32
  return tensorConvApprox(input_ptr, filter_ptr, pad_h, pad_w, stride_h,
                          stride_w, conv_mode, conv_groups, row, col,
                          skip_every, offset);
#endif
#ifdef COMPARE_WITH_CPU
  return tensorConvApprox(input_ptr, filter_ptr, pad_h, pad_w, stride_h,
                          stride_w, conv_mode, conv_groups, row, col,
                          skip_every, offset);
#endif

  if (row < 2 && col < 2 && skip_every < 2) {
    return tensorHalfConvolution(input_ptr, filter_ptr, pad_h, pad_w, stride_h,
                                 stride_w, conv_mode, conv_groups);
  }

  if (conv_groups > 1) {
    LOGW("%s: grouped conv: falling back to CPU runtime", __func__);
    return tensorConvApproxCPU(input_ptr, filter_ptr, pad_h, pad_w, stride_h,
                               stride_w, conv_mode, conv_groups, row, col,
                               skip_every, offset);
  }

  auto *input = reinterpret_cast<Tensor *>(input_ptr);
  auto *filter = reinterpret_cast<Tensor *>(filter_ptr);

  auto profile_scope = rtCtx->profile(__func__);

  host2dev(input);
  host2dev(filter);

  full2half(input);
  full2half(filter);

  LOGD("conv_mode=%d conv_groups=%d", conv_mode, conv_groups);

  DataType dtype = input->type_half();

  size_t w = input->shape().w();
  size_t h = input->shape().h();
  size_t c = input->shape().c();
  size_t n = input->shape().n();

  size_t kw = filter->shape().w();
  size_t kh = filter->shape().h();
  size_t kn = filter->shape().n();

  size_t conv_h = conv_size(h, kh, 2 * pad_h, stride_h);
  size_t conv_w = conv_size(w, kw, 2 * pad_w, stride_w);

  Shape output_shape(n, kn, conv_h, conv_w);

  Tensor *output = create_tensor(dtype, output_shape, DataPlacement::DEV_HALF);

  clblast_rt::ApproxInfo approx_info =
      approx_mode_from_internal(row, col, skip_every, offset);

  size_t workspace_size;
  clblast_rt::convolution_approx_half(rtCtx->queue(), *input, *filter, *output,
                                      nullptr, stride_w, stride_h, pad_w, pad_h,
                                      approx_info, &workspace_size);
  BufferDev workspace(workspace_size);
  workspace.allocate(rtCtx->context(), CL_MEM_HOST_NO_ACCESS);

  cl_event e_kn2row = nullptr;
  cl_event e_im2col = nullptr;
  cl_event e_gemm = nullptr;
  cl_event e_interpolate = nullptr;

  clblast_rt::convolution_approx_half(
      rtCtx->queue(), *input, *filter, *output, workspace.raw(), stride_w,
      stride_h, pad_w, pad_h, approx_info, nullptr, &e_kn2row, &e_im2col,
      &e_gemm, &e_interpolate);

  sync();

  rtCtx->profiler().push_cl_event("convolution approx half kn2row", e_kn2row);
  rtCtx->profiler().push_cl_event("convolution approx half im2col", e_im2col);
  rtCtx->profiler().push_cl_event("convolution approx half gemm", e_gemm);
  rtCtx->profiler().push_cl_event("convolution approx half interpolate",
                                  e_interpolate);

  if (e_kn2row != nullptr)
    clChk(clReleaseEvent(e_kn2row));
  if (e_im2col != nullptr)
    clChk(clReleaseEvent(e_im2col));
  if (e_gemm != nullptr)
    clChk(clReleaseEvent(e_gemm));
  if (e_interpolate != nullptr)
    clChk(clReleaseEvent(e_interpolate));

  workspace.release();

#ifdef COMPARE_WITH_CPU
  if (!compare_tensors(output,
                       tensorConvApproxCPU(input_ptr, filter_ptr, pad_h, pad_w,
                                           stride_h, stride_w, conv_mode,
                                           conv_groups, row, col, skip_every,
                                           offset),
                       1e-4)) {
    LOGE("WRONG   %s row=%d col=%d skip_every=%d offset=%d", __func__, row, col,
         skip_every, offset);
  } else {
    LOGE("CORRECT %s row=%d col=%d skip_every=%d offset=%d", __func__, row, col,
         skip_every, offset);
  }
#endif

  return output;
}

void *tensorConvSampSim2(void *input_ptr, void *filter_ptr, int vertical_pad,
                         int horizontal_pad, int vertical_stride,
                         int horizontal_stride, int conv_mode, int conv_groups,
                         int skip_rate, int skip_offset,
                         float interpolation_rate) {
  return tensorConvApproxHalf2(
      input_ptr, filter_ptr, vertical_pad, horizontal_pad, vertical_stride,
      horizontal_stride, conv_mode, conv_groups, 1, 1, skip_rate, skip_offset);
}

void startMemTracking() { rtCtx->tracking_scope_in(); }

void freeBatchMemory() { rtCtx->tracking_scope_out(); }

void hpvm_request_tensor(void *tensor, int destination) {
  if (destination == 0) {
    dev2host(tensor);
  } else {
    LOGW("hpvm_request_tensor did nothing");
  }
}

void initAndroidRtController(AAssetManager *mgr, const char *confs_path,
                             const char *labels_path, size_t num_inputs) {
  assert(mgr);
  assert(confs_path);
  rtCtx->flag_as_android_app();
  if (labels_path != nullptr) {
    rtCtx->rtController().loadLabels(mgr, labels_path, num_inputs);
  }
  rtCtx->rtController().loadTunerConfs(mgr, confs_path);
}

void initRtController(const char *confs_path, const char *labels_path,
                      size_t num_inputs) {
  assert(confs_path);
  assert(labels_path);
  rtCtx->rtController().loadLabels(labels_path, num_inputs);
  rtCtx->rtController().loadTunerConfs(confs_path);
}

void convertToFP32(Tensor *tensor) { half2full(tensor); }

void hpvmAdaptiveApproximateMore() {
  if (rtCtx->is_android_app()) {
    rtCtx->rtController().approximateMore();
  } else {
    LOGE("Called %s in non-app mode", __func__);
  }
}

void hpvmAdaptiveApproximateLess() {
  if (rtCtx->is_android_app()) {
    rtCtx->rtController().approximateLess();
  } else {
    LOGE("Called %s in non-app mode", __func__);
  }
}

uint32_t hpvmAdaptiveGetConfigIndex() {
  return static_cast<uint32_t>(rtCtx->rtController().getConfigIndex());
}

uint32_t hpvmAdaptiveSetConfigIndex(uint32_t index) {
  if (rtCtx->is_android_app()) {
    uint32_t prev_index =
        static_cast<uint32_t>(rtCtx->rtController().getConfigIndex());
    rtCtx->rtController().setConfigIndex((size_t)index);
    return prev_index;
  } else {
    LOGE("Called %s in non-app mode", __func__);
    return 0;
  }
}
