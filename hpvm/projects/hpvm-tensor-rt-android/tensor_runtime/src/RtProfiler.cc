#include "RtProfiler.h"

#include "CL/cl.h"
#include "CL/cl_platform.h"
#include "android_util.h"
#include "clblast_rt/utility.h"
#include "fmt/format.h"

#include <chrono>
#include <cstddef>
#include <fstream>
#include <stdexcept>

//
//
// ==== RtProfiler ====
//

void RtProfiler::start(RtProfiler::EventID &event) {
  LOGD("RtProfiler::start %s", event.c_str());
  _events[event].start(now());
}

void RtProfiler::end(RtProfiler::EventID &event) {
  auto at = now();
  LOGD("RtProfiler::end %s", event.c_str());
  auto it = _events.find(event);
  if (it == _events.end()) {
    throw std::runtime_error("event not started");
  }
  it->second.end(at);
}

void RtProfiler::push(RtProfiler::EventID &event, long start_ms, long end_ms) {
  Event &e = _events[event];
  e.start(std::chrono::milliseconds(start_ms));
  e.end(std::chrono::milliseconds(end_ms));
}

RtProfiler::Time RtProfiler::now() {
  return std::chrono::duration_cast<Time>(
      std::chrono::high_resolution_clock::now().time_since_epoch());
}

void RtProfiler::generate_report(std::ostream &out) {
  auto fmt_string = "{:<60} | {:10} | {:10} | {:10} | {:10} | {:10}";
  out << fmt::format(
             "{:-<60} | {:-<10} | {:-<10} | {:-<10} | {:-<10} | {:-<10}", "",
             "", "", "", "", "")
      << std::endl;
  out << fmt::format(fmt_string, "Event", "Count", "Total", "Min", "Max", "Avg")
      << std::endl;
  out << fmt::format(
             "{:-<60} | {:-<10} | {:-<10} | {:-<10} | {:-<10} | {:-<10}", "",
             "", "", "", "", "")
      << std::endl;
  for (auto &event_tuple : _events) {
    auto name = event_tuple.first;
    auto event = event_tuple.second;
    out << fmt::format(fmt_string, name, event.count, event.total.count(),
                       event.min.count(), event.max.count(),
                       event.total.count() / event.count)
        << std::endl;
  }
}

void RtProfiler::generate_report_csv(std::ostream &out) {
  out << fmt::format("\"{}\";\"{}\";\"{}\";\"{}\";\"{}\";\"{}\"", "Event",
                     "Count", "Total", "Min", "Max", "Avg")
      << std::endl;
  for (auto &event_tuple : _events) {
    auto name = event_tuple.first;
    auto event = event_tuple.second;
    out << fmt::format("{};{};{};{};{};{}", name, event.count,
                       event.total.count(), event.min.count(),
                       event.max.count(),
                       (double)event.total.count() / event.count)
        << std::endl;
  }
}

//
//
// ==== RtProfiler::Event ====
//

RtProfiler::Event::Event()
    : count(0), total(0), min(Time::max()), max(Time::min()) {}

void RtProfiler::Event::start(Time at_time) { last_start = at_time; }

void RtProfiler::Event::end(Time at_time) { update(at_time - last_start); }

void RtProfiler::Event::update(Time duration) {
  count += 1;
  total += duration;
  min = std::min(min, duration);
  max = std::max(max, duration);
}

void RtProfiler::push_cl_event(const std::string &name, cl_event event) {
#ifdef RT_TIME_KERNELS
  if (event != nullptr) {
    cl_ulong start, end;
    clblast_rt::clChk(clGetEventProfilingInfo(
        event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &start, nullptr));
    clblast_rt::clChk(clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END,
                                              sizeof(cl_ulong), &end, nullptr));

    push("cl " + name + " [ns]", 0, (end - start));
    push("cl total [ns]", 0, (end - start));
  }
#endif
}

void RtProfiler::generate_report_profiling() {
  try {
    std::ofstream out("profile_info.txt");

#ifdef RT_TIME_KERNELS
    auto time = event_total("cl total [ns]");
#else
    auto time = event_total("batch time");
#endif

    out << "Total Time : " << time.count() << "\n";

  } catch (...) {
    LOGD("Error while trying to generate profile_info.txt");
  }
}

RtProfiler::Time RtProfiler::event_total(const EventID &event_id) {
  auto it = _events.find(event_id);

  if (it == _events.end()) {
    return RtProfiler::Time::zero();
  }

  auto &event = it->second;

  LOGI("event_total for %s: min=%lld max=%lld total=%lld", event_id.c_str(),
       event.min.count(), event.max.count(), event.total.count());

  return event.total;
}
