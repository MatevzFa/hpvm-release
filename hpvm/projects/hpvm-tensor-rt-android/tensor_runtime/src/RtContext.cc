#include "RtContext.h"

#include "CL/cl.h"
#include "RtProfiler.h"
#include "android_util.h"
#include "clblast.h"
#include "global_data.h"
#include "tensor_runtime.h"

#include "fmt/format.h"
#include "fmt/ostream.h"

#include <android/log.h>

#include <fstream>
#include <ios>
#include <memory>
#include <sstream>
#include <string>

#include "clblast_rt/utility.h"

using namespace clblast_rt;

//
//
// Util
//

std::string package_name() {
  std::ifstream in("/proc/self/cmdline");

  std::string package;
  std::getline(in, package);

  package.erase(std::find(package.begin(), package.end(), '\0'), package.end());

  LOGD("package_name=%s", package.c_str());

  return package;
}

std::string internal_storage_path() { return "/data/data/" + package_name(); }

std::string timestamp() {
  auto now = std::chrono::system_clock::now();
  return std::to_string(now.time_since_epoch().count());
}

//
//
//
//

RtContext::RtContext()
    : runtime_initialized(false), approxhpvm_runtime_mode(false), op_counter(0),
      total_ops(0), profile_data(""), currentTensorID(-1) {

  LOGI("Initializing OpenCL");

  cl_int status;

  //
  // Get platforms

  cl_uint num_platforms;
  status = clGetPlatformIDs(0, nullptr, &num_platforms);
  clChk(status);

  std::vector<cl_platform_id> platforms(num_platforms);
  status = clGetPlatformIDs(num_platforms, platforms.data(), nullptr);
  clChk(status);

  //
  // Get device

  cl_uint num_devices;
  status = clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_GPU, 0, nullptr,
                          &num_devices);
  clChk(status);

  std::vector<cl_device_id> devices(num_devices);
  status = clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_GPU, num_devices,
                          devices.data(), nullptr);
  clChk(status);

  size_t maxwgs;
  clGetDeviceInfo(devices[0], CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t),
                  &maxwgs, nullptr);

  LOGI("CL_DEVICE_MAX_WORK_GROUP_SIZE %8zd", maxwgs);

  //
  // Create context

  _context =
      clCreateContext(nullptr, 1, &devices[0], nullptr, nullptr, &status);
  clChk(status);

  //
  // Create queue
  _queue = clCreateCommandQueue(_context, devices[0], CL_QUEUE_PROFILING_ENABLE,
                                &status);
  clChk(status);

  // #ifndef RT_TIME_KERNELS
  //   LOGI("Filing CLBlast cache...");
  //   clblast::FillCache(devices[0]);
  //   LOGI("Done!");
  // #endif

  LOGI("*** RtContext Initialized ***");
}

RtContext::~RtContext() {
  LOGD("*** Destroying RtContext ***");

  //
  // Generate profile_info.txt & final_accuracy, required by hvpm_profiler
  if (!is_android_app()) {
    profiler().generate_report_profiling();
    rtController().generate_report_accuracy();
    rtController().generate_report_confidence();
  }

  if (_armtensors.total_size() > 0) {
    LOGE("Some of the tracked tensors have not been freed.");
  }

  // This block of code is specific for Android applications and results in a
  // segfault when running as a standalone binary (e.g. in tests and benches).
  if (is_android_app()) {

    std::ostringstream out;
    out << "*** Profiler Report ***" << std::endl;
    _profiler.generate_report(out);
    __android_log_write(ANDROID_LOG_INFO, "RtContext", out.str().c_str());

    std::ofstream fout(internal_storage_path() + "/profiler_results-" +
                       timestamp() + "-" +
                       std::to_string(_controller.getConfigIndex()) + ".csv");
    _profiler.generate_report_csv(fout);
    fout.close();
  }

  clblast::ClearCache();

  clChk(clReleaseCommandQueue(_queue));
  clChk(clReleaseContext(_context));
}

void RtContext::flag_as_android_app() { _is_android_app = true; }

void RtContext::track_tensor(clblast_rt::Tensor *arm_tensor) {
  _armtensors.push(arm_tensor);
}

void RtContext::tracking_scope_in() { _armtensors.scope_in(); }

void RtContext::tracking_scope_out() {
  for (auto &arm_tensor : _armtensors.top_iter()) {
    freeTensor(arm_tensor);
    delete arm_tensor;
  }
  _armtensors.scope_out();
}

ProfilerScope RtContext::profile(RtProfiler::EventID &event) {
  return ProfilerScope(_profiler, event);
}

void RtContext::profile_completed(RtProfiler::EventID &event,
                                  unsigned long start_ms,
                                  unsigned long end_ms) {
  _profiler.push(event, start_ms, end_ms);
}

RtController &RtContext::rtController() { return _controller; }

RtProfiler &RtContext::profiler() { return _profiler; }

bool RtContext::is_android_app() { return _is_android_app; }
