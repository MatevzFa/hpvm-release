

#include <android/asset_manager.h>
#include <fstream>
#include <map>
#include <sstream>
#include <string.h>
#include <string>
#include <vector>

#include "approx_knob_utils.h"
#include "debug.h"

//
//
// PerfParams
//

PerfParams::PerfParams() {
  row = 1;
  col = 1;
  skip_offset = 0;
}

PerfParams::PerfParams(int row1, int col1, int skip_offset1) {
  row = row1;
  col = col1;
  skip_offset = skip_offset1;
}

PerfParamSet::PerfParamSet() {}

void PerfParamSet::parse_params(std::istream &istream) {
  std::string line;
  std::string partial;
  std::vector<std::string> tokens;

  while (std::getline(istream, line)) { // Read each line

    // printf ("***** line === %s ", line);
    std::istringstream iss(line);
    std::string token;
    while (std::getline(iss, token, '\t')) { // Read each token in the line
      tokens.push_back(token);

      int index = token.find("perf");
      if (index != std::string::npos) {

        int index2 = token.find(",");
        std::string knob_str = token.substr(index2 + 1);
        int knob = atoi(knob_str.c_str());

        std::getline(iss, token, '\t');
        std::istringstream token_stream(token);

        std::string tok;

        std::getline(token_stream, tok, ',');
        int row = atoi(tok.c_str());

        std::getline(token_stream, tok, ',');
        int col = atoi(tok.c_str());

        std::getline(token_stream, tok, ',');
        int offset = atoi(tok.c_str());

        LOGD("**** knob = %d, row = %d, col = %d, offset = %d \n\n", knob, row,
             col, offset);
        PerfParams params(row, col, offset);
        perf_knob_map[knob] = params;
      }
    }
  }
}

void PerfParamSet::load_from(AAssetManager *mgr, const std::string &path) {
  AAsset *asset = AAssetManager_open(mgr, path.c_str(), AASSET_MODE_BUFFER);
  if (!asset) {
    LOGE("Could not open global knobs %s", path.c_str());
    return;
  }

  LOGI("PerfParamSet loading knobs");

  auto len = AAsset_getLength(asset);
  std::vector<char> buf(len);
  AAsset_read(asset, buf.data(), len);

  std::istringstream istream(std::string(buf.data(), len));
  parse_params(istream);
}

void PerfParamSet::load_from(const std::string &path) {
  std::ifstream in(path);
  parse_params(in);
}

PerfParams PerfParamSet::getPerfParams(int swing) {

  if (swing >= 150) {
    swing = swing - 30;
  }

  return perf_knob_map[swing];
}

//
//
// SampParams
//

SampParams::SampParams() {
  skip_rate = 1;
  skip_offset = 0;
}

SampParams::SampParams(int skip_rate1, int skip_offset1,
                       float interpolation_id1) {
  skip_rate = skip_rate1;
  skip_offset = skip_offset1;
  interpolation_id = interpolation_id1;
}

SampParamSet::SampParamSet() {}

void SampParamSet::parse_params(std::istream &istream) {
  std::string line;
  std::string partial;
  std::vector<std::string> tokens;

  while (std::getline(istream, line)) { // Read each line

    std::istringstream iss(line);
    std::string token;
    while (std::getline(iss, token, '\t')) { // Read each token in the line
      tokens.push_back(token);

      int index = token.find("samp");
      int test_index = token.find("reduction");

      if (index != std::string::npos && test_index == std::string::npos) {

        int index2 = token.find(",");
        std::string knob_str = token.substr(index2 + 1);
        int knob = atoi(knob_str.c_str());
        printf("knob = %d \n", knob);

        std::getline(iss, token, '\t');
        std::istringstream token_stream(token);

        std::string tok;

        std::getline(token_stream, tok, ',');
        int skip_every = atoi(tok.c_str());

        std::getline(token_stream, tok, ',');
        int offset = atoi(tok.c_str());

        std::getline(token_stream, tok, ',');
        float interpolation_id = atof(tok.c_str());

        LOGD("skip_every = %d, offset = %d \n", skip_every, offset);
        SampParams params(skip_every, offset, interpolation_id);
        samp_knob_map[knob] = params;
      }
    }
  }
}

void SampParamSet::load_from(AAssetManager *mgr, const std::string &path) {
  AAsset *asset = AAssetManager_open(mgr, path.c_str(), AASSET_MODE_BUFFER);
  if (!asset) {
    LOGE("Could not open global knobs %s", path.c_str());
    return;
  }

  LOGI("PerfParamSet loading knobs");

  auto len = AAsset_getLength(asset);
  std::vector<char> buf(len);
  AAsset_read(asset, buf.data(), len);

  std::istringstream istream(std::string(buf.data(), len));
  parse_params(istream);
}

void SampParamSet::load_from(const std::string &path) {
  std::ifstream in(path);
  parse_params(in);
}

SampParams SampParamSet::getSampParams(int swing) {

  if (swing >= 260) {
    swing = swing - 30;
  }

  return samp_knob_map[swing];
}

RedSampParams::RedSampParams() {
  skip_ratio = 0.0f;
  is_half = false;
}

RedSampParams::RedSampParams(float skip_ratio1, bool is_half1) {
  skip_ratio = skip_ratio1;
  is_half = is_half1;
}

RedSampParams getRedSampParams(int swing) {

  std::map<int, RedSampParams> red_samp_knob_map;

  RedSampParams params41(0.5, false);
  red_samp_knob_map[41] = params41;

  RedSampParams params42(0.5, true);
  red_samp_knob_map[42] = params42;

  RedSampParams params43(0.4, false);
  red_samp_knob_map[43] = params43;

  RedSampParams params44(0.4, true);
  red_samp_knob_map[44] = params44;

  RedSampParams params45(0.25, false);
  red_samp_knob_map[45] = params45;

  RedSampParams params46(0.25, true);
  red_samp_knob_map[46] = params46;

  return red_samp_knob_map[swing];
}
