# HPVM Tensor Runtime for Android

## Building

Before building, set the environment variables as described in [build instructions for Android](../../../building-android.md).

To build and install the HPVM Tensor Runtime into an Android app, run the following script:
```
export ANDROID_APP_ROOT="path/to/android/app/root
mkdir build
cd build
../scripts/configure.sh ..
ninja install
```
