#include "utilities/approxhpvm_util.hpp"

#include <cstddef>

namespace clblast {

size_t approxhpvm_perf_index_restore(size_t idx, size_t perf_offset,
                                     size_t perf_stride) {
  return (((idx - perf_offset + 1) * perf_stride) / (perf_stride - 1) +
          (((idx - perf_offset + 1) * perf_stride) % (perf_stride - 1) > 0) +
          perf_offset - 1);
}

size_t approxhpvm_perf_out_size(size_t dim_size, size_t perf_offset,
                                size_t perf_stride) {
  if (dim_size < perf_offset) {
    return dim_size;
  }
  size_t perf_area = (dim_size - perf_offset);
  if (perf_area == 0) {
    return dim_size;
  } else {
    size_t n_removed = (perf_area - 1) / perf_stride + 1;
    return perf_offset + perf_area - n_removed;
  }
}

size_t conv_size(size_t size, size_t kernel_size, size_t pad_both,
                 size_t stride) {
  return (size - kernel_size + pad_both) / stride + 1;
}

} // namespace clblast