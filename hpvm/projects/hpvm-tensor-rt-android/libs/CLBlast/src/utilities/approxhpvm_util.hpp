#ifndef __APPROXHPVM_UTIL_HPP__
#define __APPROXHPVM_UTIL_HPP__

#include <cstddef>

namespace clblast {

size_t approxhpvm_perf_out_size(size_t dim_size, size_t perf_offset,
                                size_t perf_stride);

size_t approxhpvm_perf_index_restore(size_t idx, size_t perf_offset,
                                     size_t perf_stride);

size_t conv_size(size_t size, size_t kernel_size, size_t pad_both,
                 size_t stride);

} // namespace clblast

#endif // __APPROXHPVM_UTIL_HPP__
