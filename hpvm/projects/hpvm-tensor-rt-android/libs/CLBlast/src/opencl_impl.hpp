#ifndef __OPENCL_IMPL_HPP__
#define __OPENCL_IMPL_HPP__

#define CL_TARGET_OPENCL_VERSION 120
#define CL_USE_DEPRECATED_OPENCL_1_1_APIS // to disable deprecation warnings
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS // to disable deprecation warnings
#define CL_USE_DEPRECATED_OPENCL_2_0_APIS // to disable deprecation warnings
#if defined(__APPLE__) || defined(__MACOSX)
#include <OpenCL/opencl.h>
#else
#include <CL/opencl.h>
#endif

#include <functional>

#include <dlfcn.h>

namespace clblast {

class CLApi {
public:
  int field;
  CLApi();

  ~CLApi();

public:
  static const CLApi &get();

#define DECLARE_FUNCTION(func_name)                                            \
  std::function<decltype(func_name)> func_name##_ptr = nullptr

  DECLARE_FUNCTION(clGetPlatformIDs);
  DECLARE_FUNCTION(clGetPlatformInfo);
  DECLARE_FUNCTION(clGetDeviceIDs);
  DECLARE_FUNCTION(clGetDeviceInfo);
  // DECLARE_FUNCTION(clReleaseDevice);
  DECLARE_FUNCTION(clReleaseContext);
  DECLARE_FUNCTION(clReleaseCommandQueue);
  // DECLARE_FUNCTION(clRetainDevice);
  DECLARE_FUNCTION(clRetainContext);
  DECLARE_FUNCTION(clReleaseProgram);
  DECLARE_FUNCTION(clBuildProgram);
  DECLARE_FUNCTION(clCreateContext);
  DECLARE_FUNCTION(clCreateContextFromType);
  DECLARE_FUNCTION(clGetContextInfo);
  DECLARE_FUNCTION(clGetProgramInfo);
  DECLARE_FUNCTION(clCreateProgramWithSource);
  DECLARE_FUNCTION(clGetProgramBuildInfo);
  DECLARE_FUNCTION(clCreateCommandQueue);
  DECLARE_FUNCTION(clCreateKernelsInProgram);
  DECLARE_FUNCTION(clRetainKernel);
  DECLARE_FUNCTION(clReleaseKernel);
  DECLARE_FUNCTION(clGetKernelInfo);
  DECLARE_FUNCTION(clReleaseMemObject);
  DECLARE_FUNCTION(clReleaseEvent);
  DECLARE_FUNCTION(clCreateBuffer);
  DECLARE_FUNCTION(clSetKernelArg);
  DECLARE_FUNCTION(clEnqueueReadBuffer);
  DECLARE_FUNCTION(clEnqueueNDRangeKernel);
  DECLARE_FUNCTION(clFlush);
  DECLARE_FUNCTION(clFinish);
  DECLARE_FUNCTION(clRetainMemObject);
  DECLARE_FUNCTION(clGetMemObjectInfo);

  DECLARE_FUNCTION(clCreateKernel);
  DECLARE_FUNCTION(clEnqueueCopyBuffer);
  DECLARE_FUNCTION(clEnqueueWriteBuffer);
  DECLARE_FUNCTION(clEnqueueFillBuffer);
  DECLARE_FUNCTION(clCreateProgramWithBinary);

  DECLARE_FUNCTION(clWaitForEvents);
  DECLARE_FUNCTION(clGetEventProfilingInfo);
  DECLARE_FUNCTION(clGetKernelWorkGroupInfo);
  DECLARE_FUNCTION(clGetCommandQueueInfo);

  DECLARE_FUNCTION(clGetEventInfo);
  DECLARE_FUNCTION(clEnqueueMarker);

#undef DECLARE_FUNCTION

private:
  void *_handle;

  void load();
};

} // namespace clblast

#endif // __OPENCL_IMPL_HPP__
