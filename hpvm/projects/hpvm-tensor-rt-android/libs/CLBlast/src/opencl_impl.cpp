#include "opencl_impl.hpp"
#include <array>
#include <sstream>
#include <string>

#define CL_TARGET_OPENCL_VERSION 120
#define CL_USE_DEPRECATED_OPENCL_1_1_APIS // to disable deprecation warnings
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS // to disable deprecation warnings
#define CL_USE_DEPRECATED_OPENCL_2_0_APIS // to disable deprecation warnings
#include "CL/cl.h"

#include <dlfcn.h>

#include <array>
#include <climits>

namespace clblast {

namespace {

static const std::array<std::string, 3> LIBS = {
    "libOpenCL.so",
    "/vendor/lib/egl/libGLES_mali.so",
};

void *try_load_opencl() {
  std::ostringstream out;
  out << "Could not load OpenCL. Tried ";

  for (const auto &lib : LIBS) {
    out << lib << " ";
    void *hnd = dlopen(lib.c_str(), RTLD_NOW | RTLD_LOCAL);

    if (hnd != nullptr) {
      return hnd;
    }
  }

  out << ".";

  throw std::runtime_error(out.str());
}
} // namespace

CLApi::CLApi() {
  _handle = try_load_opencl();

  load();
}

CLApi::~CLApi() { dlclose(_handle); }

const CLApi &CLApi::get() {
  static CLApi api;
  return api;
}

#define LOAD_FUNCTION(handle, func_name)                                       \
  func_name##_ptr =                                                            \
      reinterpret_cast<decltype(func_name) *>(dlsym(handle, #func_name));

void CLApi::load() {
  LOAD_FUNCTION(_handle, clGetPlatformIDs);
  LOAD_FUNCTION(_handle, clGetPlatformInfo);
  LOAD_FUNCTION(_handle, clGetDeviceIDs);
  LOAD_FUNCTION(_handle, clGetDeviceInfo);
  // LOAD_FUNCTION(_handle, clReleaseDevice);
  LOAD_FUNCTION(_handle, clReleaseContext);
  LOAD_FUNCTION(_handle, clReleaseCommandQueue);
  // LOAD_FUNCTION(_handle, clRetainDevice);
  LOAD_FUNCTION(_handle, clRetainContext);
  LOAD_FUNCTION(_handle, clReleaseProgram);
  LOAD_FUNCTION(_handle, clBuildProgram);
  LOAD_FUNCTION(_handle, clCreateContext);
  LOAD_FUNCTION(_handle, clCreateContextFromType);
  LOAD_FUNCTION(_handle, clGetContextInfo);
  LOAD_FUNCTION(_handle, clGetProgramInfo);
  LOAD_FUNCTION(_handle, clCreateProgramWithSource);
  LOAD_FUNCTION(_handle, clGetProgramBuildInfo);
  LOAD_FUNCTION(_handle, clCreateCommandQueue);
  LOAD_FUNCTION(_handle, clCreateKernelsInProgram);
  LOAD_FUNCTION(_handle, clRetainKernel);
  LOAD_FUNCTION(_handle, clReleaseKernel);
  LOAD_FUNCTION(_handle, clGetKernelInfo);
  LOAD_FUNCTION(_handle, clReleaseMemObject);
  LOAD_FUNCTION(_handle, clReleaseEvent);
  LOAD_FUNCTION(_handle, clCreateBuffer);
  LOAD_FUNCTION(_handle, clSetKernelArg);
  LOAD_FUNCTION(_handle, clEnqueueReadBuffer);
  LOAD_FUNCTION(_handle, clEnqueueNDRangeKernel);
  LOAD_FUNCTION(_handle, clFlush);
  LOAD_FUNCTION(_handle, clFinish);
  LOAD_FUNCTION(_handle, clRetainMemObject);
  LOAD_FUNCTION(_handle, clGetMemObjectInfo);

  LOAD_FUNCTION(_handle, clCreateKernel);
  LOAD_FUNCTION(_handle, clEnqueueCopyBuffer);
  LOAD_FUNCTION(_handle, clEnqueueWriteBuffer);
  LOAD_FUNCTION(_handle, clEnqueueFillBuffer);
  LOAD_FUNCTION(_handle, clCreateProgramWithBinary);

  LOAD_FUNCTION(_handle, clWaitForEvents);
  LOAD_FUNCTION(_handle, clGetEventProfilingInfo);
  LOAD_FUNCTION(_handle, clGetKernelWorkGroupInfo);
  LOAD_FUNCTION(_handle, clGetCommandQueueInfo);

  LOAD_FUNCTION(_handle, clGetEventInfo);
  LOAD_FUNCTION(_handle, clEnqueueMarker);
}

#undef LOAD_FUNCTION

} // namespace clblast

using namespace clblast;

cl_int clGetPlatformIDs(cl_uint num_entries, cl_platform_id *platforms,
                        cl_uint *num_platforms) {
  auto func = CLApi::get().clGetPlatformIDs_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clGetPlatformIDs_ptr not loaded");
  }
  return func(num_entries, platforms, num_platforms);
}

cl_int clGetPlatformInfo(cl_platform_id platform, cl_platform_info param_name,
                         size_t param_value_size, void *param_value,
                         size_t *param_value_size_ret) {
  auto func = CLApi::get().clGetPlatformInfo_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clGetPlatformInfo_ptr not loaded");
  }
  return func(platform, param_name, param_value_size, param_value,
              param_value_size_ret);
}

cl_int clGetDeviceIDs(cl_platform_id platform, cl_device_type device_type,
                      cl_uint num_entries, cl_device_id *devices,
                      cl_uint *num_devices) {
  auto func = CLApi::get().clGetDeviceIDs_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clGetDeviceIDs_ptr not loaded");
  }
  return func(platform, device_type, num_entries, devices, num_devices);
}

cl_int clGetDeviceInfo(cl_device_id device, cl_device_info param_name,
                       size_t param_value_size, void *param_value,
                       size_t *param_value_size_ret) {
  auto func = CLApi::get().clGetDeviceInfo_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clGetDeviceInfo_ptr not loaded");
  }
  return func(device, param_name, param_value_size, param_value,
              param_value_size_ret);
}

// cl_int clReleaseDevice(cl_device_id device) {
//   auto func = CLApi::get().clReleaseDevice_ptr;
//   if (func == nullptr) {
//     throw std::runtime_error("clReleaseDevice_ptr not loaded");
//   }
//   return func(device);
// }

cl_int clReleaseContext(cl_context context) {
  auto func = CLApi::get().clReleaseContext_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clReleaseContext_ptr not loaded");
  }
  return func(context);
}

cl_int clReleaseCommandQueue(cl_command_queue command_queue) {
  auto func = CLApi::get().clReleaseCommandQueue_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clReleaseCommandQueue_ptr not loaded");
  }
  return func(command_queue);
}

// cl_int clRetainDevice(cl_device_id device) {
//   auto func = CLApi::get().clRetainDevice_ptr;
//   if (func == nullptr) {
//     throw std::runtime_error("clRetainDevice_ptr not loaded");
//   }
//   return func(device);
// }

cl_int clRetainContext(cl_context context) {
  auto func = CLApi::get().clRetainContext_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clRetainContext_ptr not loaded");
  }
  return func(context);
}

cl_int clReleaseProgram(cl_program program) {
  auto func = CLApi::get().clReleaseProgram_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clReleaseProgram_ptr not loaded");
  }
  return func(program);
}

cl_int clBuildProgram(cl_program program, cl_uint num_devices,
                      const cl_device_id *device_list, const char *options,
                      void (*pfn_notify)(cl_program, void *), void *user_data) {
  auto func = CLApi::get().clBuildProgram_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clBuildProgram_ptr not loaded");
  }
  return func(program, num_devices, device_list, options, pfn_notify,
              user_data);
}

cl_context clCreateContext(const cl_context_properties *properties,
                           cl_uint num_devices, const cl_device_id *devices,
                           void (*pfn_notify)(const char *, const void *,
                                              size_t, void *),
                           void *user_data, cl_int *errcode_ret) {
  auto func = CLApi::get().clCreateContext_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clCreateContext_ptr not loaded");
  }
  return func(properties, num_devices, devices, pfn_notify, user_data,
              errcode_ret);
}

cl_context clCreateContextFromType(
    const cl_context_properties *properties, cl_device_type device_type,
    void (*pfn_notify)(const char *, const void *, size_t, void *),
    void *user_data, cl_int *errcode_ret) {
  auto func = CLApi::get().clCreateContextFromType_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clCreateContextFromType_ptr not loaded");
  }
  return func(properties, device_type, pfn_notify, user_data, errcode_ret);
}

cl_int clGetContextInfo(cl_context context, cl_context_info param_name,
                        size_t param_value_size, void *param_value,
                        size_t *param_value_size_ret) {
  auto func = CLApi::get().clGetContextInfo_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clGetContextInfo_ptr not loaded");
  }
  return func(context, param_name, param_value_size, param_value,
              param_value_size_ret);
}

cl_int clGetProgramInfo(cl_program program, cl_program_info param_name,
                        size_t param_value_size, void *param_value,
                        size_t *param_value_size_ret) {
  auto func = CLApi::get().clGetProgramInfo_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clGetProgramInfo_ptr not loaded");
  }
  return func(program, param_name, param_value_size, param_value,
              param_value_size_ret);
}

cl_program clCreateProgramWithSource(cl_context context, cl_uint count,
                                     const char **strings,
                                     const size_t *lengths,
                                     cl_int *errcode_ret) {
  auto func = CLApi::get().clCreateProgramWithSource_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clCreateProgramWithSource_ptr not loaded");
  }
  return func(context, count, strings, lengths, errcode_ret);
}

cl_int clGetProgramBuildInfo(cl_program program, cl_device_id device,
                             cl_program_build_info param_name,
                             size_t param_value_size, void *param_value,
                             size_t *param_value_size_ret) {
  auto func = CLApi::get().clGetProgramBuildInfo_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clGetProgramBuildInfo_ptr not loaded");
  }
  return func(program, device, param_name, param_value_size, param_value,
              param_value_size_ret);
}

cl_command_queue clCreateCommandQueue(cl_context context, cl_device_id device,
                                      cl_command_queue_properties properties,
                                      cl_int *errcode_ret) {
  auto func = CLApi::get().clCreateCommandQueue_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clCreateCommandQueue_ptr not loaded");
  }
  return func(context, device, properties, errcode_ret);
}

cl_int clCreateKernelsInProgram(cl_program program, cl_uint num_kernels,
                                cl_kernel *kernels, cl_uint *num_kernels_ret) {
  auto func = CLApi::get().clCreateKernelsInProgram_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clCreateKernelsInProgram_ptr not loaded");
  }
  return func(program, num_kernels, kernels, num_kernels_ret);
}

cl_int clRetainKernel(cl_kernel kernel) {
  auto func = CLApi::get().clRetainKernel_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clRetainKernel_ptr not loaded");
  }
  return func(kernel);
}

cl_int clReleaseKernel(cl_kernel kernel) {
  auto func = CLApi::get().clReleaseKernel_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clReleaseKernel_ptr not loaded");
  }
  return func(kernel);
}

cl_int clGetKernelInfo(cl_kernel kernel, cl_kernel_info param_name,
                       size_t param_value_size, void *param_value,
                       size_t *param_value_size_ret) {
  auto func = CLApi::get().clGetKernelInfo_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clGetKernelInfo_ptr not loaded");
  }
  return func(kernel, param_name, param_value_size, param_value,
              param_value_size_ret);
}

///////////////

cl_int clReleaseMemObject(cl_mem memobj) {
  auto func = CLApi::get().clReleaseMemObject_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clReleaseMemObject_ptr not loaded");
  }
  return func(memobj);
}

cl_int clReleaseEvent(cl_event event) {
  auto func = CLApi::get().clReleaseEvent_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clReleaseEvent_ptr not loaded");
  }
  return func(event);
}

cl_mem clCreateBuffer(cl_context context, cl_mem_flags flags, size_t size,
                      void *host_ptr, cl_int *errcode_ret) {
  auto func = CLApi::get().clCreateBuffer_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clCreateBuffer_ptr not loaded");
  }
  return func(context, flags, size, host_ptr, errcode_ret);
}

cl_int clSetKernelArg(cl_kernel kernel, cl_uint arg_index, size_t arg_size,
                      const void *arg_value) {
  auto func = CLApi::get().clSetKernelArg_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clSetKernelArg_ptr not loaded");
  }
  return func(kernel, arg_index, arg_size, arg_value);
}

cl_int clEnqueueReadBuffer(cl_command_queue command_queue, cl_mem buffer,
                           cl_bool blocking_read, size_t offset, size_t size,
                           void *ptr, cl_uint num_events_in_wait_list,
                           const cl_event *event_wait_list, cl_event *event) {
  auto func = CLApi::get().clEnqueueReadBuffer_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clEnqueueReadBuffer_ptr not loaded");
  }
  return func(command_queue, buffer, blocking_read, offset, size, ptr,
              num_events_in_wait_list, event_wait_list, event);
}

cl_int clEnqueueNDRangeKernel(
    cl_command_queue command_queue, cl_kernel kernel, cl_uint work_dim,
    const size_t *global_work_offset, const size_t *global_work_size,
    const size_t *local_work_size, cl_uint num_events_in_wait_list,
    const cl_event *event_wait_list, cl_event *event) {
  auto func = CLApi::get().clEnqueueNDRangeKernel_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clEnqueueNDRangeKernel_ptr not loaded");
  }
  return func(command_queue, kernel, work_dim, global_work_offset,
              global_work_size, local_work_size, num_events_in_wait_list,
              event_wait_list, event);
}

cl_int clFlush(cl_command_queue command_queue) {
  auto func = CLApi::get().clFinish_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clFinish_ptr not loaded");
  }
  return func(command_queue);
}

cl_int clFinish(cl_command_queue command_queue) {
  auto func = CLApi::get().clFinish_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clFinish_ptr not loaded");
  }
  return func(command_queue);
}

cl_int clRetainMemObject(cl_mem memobj) {
  auto func = CLApi::get().clRetainMemObject_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clRetainMemObject_ptr not loaded");
  }
  return func(memobj);
}

cl_int clGetMemObjectInfo(cl_mem memobj, cl_mem_info param_name,
                          size_t param_value_size, void *param_value,
                          size_t *param_value_size_ret) {
  auto func = CLApi::get().clGetMemObjectInfo_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clGetMemObjectInfo_ptr not loaded");
  }
  return func(memobj, param_name, param_value_size, param_value,
              param_value_size_ret);
}

cl_kernel clCreateKernel(cl_program program, const char *kernel_name,
                         cl_int *errcode_ret) {
  auto func = CLApi::get().clCreateKernel_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clCreateKernel_ptr not loaded");
  }
  return func(program, kernel_name, errcode_ret);
}

cl_int clEnqueueCopyBuffer(cl_command_queue command_queue, cl_mem src_buffer,
                           cl_mem dst_buffer, size_t src_offset,
                           size_t dst_offset, size_t size,
                           cl_uint num_events_in_wait_list,
                           const cl_event *event_wait_list, cl_event *event) {
  auto func = CLApi::get().clEnqueueCopyBuffer_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clEnqueueCopyBuffer_ptr not loaded");
  }
  return func(command_queue, src_buffer, dst_buffer, src_offset, dst_offset,
              size, num_events_in_wait_list, event_wait_list, event);
}

cl_int clEnqueueWriteBuffer(cl_command_queue command_queue, cl_mem buffer,
                            cl_bool blocking_write, size_t offset, size_t size,
                            const void *ptr, cl_uint num_events_in_wait_list,
                            const cl_event *event_wait_list, cl_event *event) {
  auto func = CLApi::get().clEnqueueWriteBuffer_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clEnqueueWriteBuffer_ptr not loaded");
  }
  return func(command_queue, buffer, blocking_write, offset, size, ptr,
              num_events_in_wait_list, event_wait_list, event);
}

cl_int clEnqueueFillBuffer(cl_command_queue command_queue, cl_mem buffer,
                           const void *pattern, size_t pattern_size,
                           size_t offset, size_t size,
                           cl_uint num_events_in_wait_list,
                           const cl_event *event_wait_list, cl_event *event) {

  auto func = CLApi::get().clEnqueueFillBuffer_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clEnqueueFillBuffer_ptr not loaded");
  }
  return func(command_queue, buffer, pattern, pattern_size, offset, size,
              num_events_in_wait_list, event_wait_list, event);
}

cl_program clCreateProgramWithBinary(cl_context context, cl_uint num_devices,
                                     const cl_device_id *device_list,
                                     const size_t *lengths,
                                     const unsigned char **binaries,
                                     cl_int *binary_status,
                                     cl_int *errcode_ret) {
  auto func = CLApi::get().clCreateProgramWithBinary_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clCreateProgramWithBinary_ptr not loaded");
  }
  return func(context, num_devices, device_list, lengths, binaries,
              binary_status, errcode_ret);
}

cl_int clWaitForEvents(cl_uint num_events, const cl_event *event_list) {
  auto func = CLApi::get().clWaitForEvents_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clWaitForEvents_ptr not loaded");
  }
  return func(num_events, event_list);
}

cl_int clGetEventProfilingInfo(cl_event event, cl_profiling_info param_name,
                               size_t param_value_size, void *param_value,
                               size_t *param_value_size_ret) {
  auto func = CLApi::get().clGetEventProfilingInfo_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clGetEventProfilingInfo_ptr not loaded");
  }
  return func(event, param_name, param_value_size, param_value,
              param_value_size_ret);
}

cl_int clGetKernelWorkGroupInfo(cl_kernel kernel, cl_device_id device,
                                cl_kernel_work_group_info param_name,
                                size_t param_value_size, void *param_value,
                                size_t *param_value_size_ret) {
  auto func = CLApi::get().clGetKernelWorkGroupInfo_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clGetKernelWorkGroupInfo_ptr not loaded");
  }
  return func(kernel, device, param_name, param_value_size, param_value,
              param_value_size_ret);
}

cl_int clGetCommandQueueInfo(cl_command_queue command_queue,
                             cl_command_queue_info param_name,
                             size_t param_value_size, void *param_value,
                             size_t *param_value_size_ret) {
  auto func = CLApi::get().clGetCommandQueueInfo_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clGetCommandQueueInfo_ptr not loaded");
  }
  return func(command_queue, param_name, param_value_size, param_value,
              param_value_size_ret);
}

cl_int clGetCommandQueueInfo(cl_kernel kernel, cl_device_id device,
                             cl_kernel_work_group_info param_name,
                             size_t param_value_size, void *param_value,
                             size_t *param_value_size_ret) {
  auto func = CLApi::get().clGetKernelWorkGroupInfo_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clGetKernelWorkGroupInfo_ptr not loaded");
  }
  return func(kernel, device, param_name, param_value_size, param_value,
              param_value_size_ret);
}

cl_int clGetEventInfo(cl_event event, cl_event_info param_name,
                      size_t param_value_size, void *param_value,
                      size_t *param_value_size_ret) {
  auto func = CLApi::get().clGetEventInfo_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clGetEventInfo_ptr not loaded");
  }
  return func(event, param_name, param_value_size, param_value,
              param_value_size_ret);
}

cl_int clEnqueueMarker(cl_command_queue command_queue, cl_event *event) {
  auto func = CLApi::get().clEnqueueMarker_ptr;
  if (func == nullptr) {
    throw std::runtime_error("clEnqueueMarker_ptr not loaded");
  }
  return func(command_queue, event);
}
