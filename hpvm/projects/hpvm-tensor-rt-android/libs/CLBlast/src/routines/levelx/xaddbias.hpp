#ifndef CLBLAST_ROUTINES_ADDBIAS_H
#define CLBLAST_ROUTINES_ADDBIAS_H

#include "clblast.h"
#include "routine.hpp"

namespace clblast {

template <typename T> class XaddBias : public Routine {
public:
  // Constructor
  XaddBias(Queue &queue, EventPointer event,
           const std::string &name = "ADDBIAS");

  // Templated-precision implementation of the routine
  void DoAddBias(const size_t n_total, const size_t channels, //
                 const size_t per_channel,                    //
                 const Buffer<T> &in_buffer, const size_t in_offset,
                 const Buffer<T> &out_buffer, const size_t out_offset,
                 const Buffer<T> &bias_buffer, const size_t bias_offset);
};

} // namespace clblast

#endif // CLBLAST_ROUTINES_ADDBIAS_H
