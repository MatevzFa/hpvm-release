
// =================================================================================================
// This file is part of the CLBlast project. The project is licensed under
// Apache Version 2.0. This project loosely follows the Google C++ styleguide
// and uses a tab-size of two spaces and a max- width of 100 characters per
// line.
//
// Author(s):
//   Cedric Nugteren <www.cedricnugteren.nl>
//
// This file implements the Xim2colApprox class (see the header for information
// about the class).
//
// =================================================================================================

#include "routines/levelx/xim2col_approx.hpp"
#include "clblast.h"
#include "utilities/approxhpvm_util.hpp"

#include <string>
#include <vector>

namespace clblast {
// =================================================================================================

// Constructor: forwards to base class constructor
template <typename T>
Xim2colApprox<T>::Xim2colApprox(Queue &queue, EventPointer event, const std::string &name)
    : Routine(queue, event, name, {"Copy"}, PrecisionValue<T>(), {},
              {
#include "../../kernels/levelx/im2col_approx_col.opencl"
#include "../../kernels/levelx/im2col_approx_kernel.opencl"
#include "../../kernels/levelx/im2col_approx_row.opencl"
              }) {
}

// =================================================================================================

// The main routine
template <typename T>
void Xim2colApprox<T>::DoIm2colApprox(const KernelMode kernel_mode, const size_t batch_size,
                                      const size_t channels, const size_t height,
                                      const size_t width, const size_t kernel_h,
                                      const size_t kernel_w, const size_t pad_h, const size_t pad_w,
                                      const size_t stride_h, const size_t stride_w,
                                      const size_t dilation_h, const size_t dilation_w,
                                      const Buffer<T> &im_buffer, const size_t im_offset,
                                      const Buffer<T> &col_buffer, const size_t col_offset,
                                      const ApproxInfo &approx_info) {

  // Flip the output along kernel_h and kernel_w, or not.
  std::string kernel_name = (kernel_mode == KernelMode::kConvolution) ? "Xim2colApproxKernelFlip"
                                                                      : "Xim2colApproxKernelNormal";

  switch (approx_info.mode) {
  case ApproxMode::PERF_ROW:
    kernel_name += "_ROW";
    break;
  case ApproxMode::PERF_COL:
    kernel_name += "_COL";
    break;
  case ApproxMode::SAMP_KERNEL:
    kernel_name += "_KERNEL";
    break;
  }

  // Makes sure all dimensions are larger than zero
  if ((channels == 0) || (height == 0) || (width == 0)) {
    throw BLASError(StatusCode::kInvalidDimension);
  }

  // Sets the height and width of the 'col' result
  const auto size_h = height + 2 * pad_h;
  const auto padding_h = dilation_h * (kernel_h - 1) + 1;
  const auto col_h = (size_h >= padding_h) ? (size_h - padding_h) / stride_h + 1 : 1;
  const auto size_w = width + 2 * pad_w;
  const auto padding_w = dilation_w * (kernel_w - 1) + 1;
  const auto col_w = (size_w >= padding_w) ? (size_w - padding_w) / stride_w + 1 : 1;

  // Retrieves the kernel from the compiled binary
  auto kernel = Kernel(program_, kernel_name);

  size_t output_dim_perf = 0;
  if (approx_info.mode == ApproxMode::PERF_ROW) {
    output_dim_perf =
        approxhpvm_perf_out_size(col_h, approx_info.perf_offset, approx_info.perf_stride);
  } else if (approx_info.mode == ApproxMode::PERF_COL) {
    output_dim_perf =
        approxhpvm_perf_out_size(col_w, approx_info.perf_offset, approx_info.perf_stride);
  }

  size_t idx = 0;
  // Sets the kernel arguments
  kernel.SetArgument(idx++, static_cast<int>(height));
  kernel.SetArgument(idx++, static_cast<int>(width));
  if (approx_info.mode == ApproxMode::SAMP_KERNEL) {
    kernel.SetArgument(idx++, static_cast<int>(batch_size));
    kernel.SetArgument(idx++, static_cast<int>(channels));
  } else {
    kernel.SetArgument(idx++, static_cast<int>(batch_size * channels));
  }
  kernel.SetArgument(idx++, static_cast<int>(col_h));
  kernel.SetArgument(idx++, static_cast<int>(col_w));
  kernel.SetArgument(idx++, static_cast<int>(output_dim_perf));
  kernel.SetArgument(idx++, static_cast<int>(kernel_h));
  kernel.SetArgument(idx++, static_cast<int>(kernel_w));
  kernel.SetArgument(idx++, static_cast<int>(pad_h));
  kernel.SetArgument(idx++, static_cast<int>(pad_w));
  kernel.SetArgument(idx++, static_cast<int>(stride_h));
  kernel.SetArgument(idx++, static_cast<int>(stride_w));
  kernel.SetArgument(idx++, static_cast<int>(dilation_h));
  kernel.SetArgument(idx++, static_cast<int>(dilation_w));
  kernel.SetArgument(idx++, im_buffer());
  kernel.SetArgument(idx++, static_cast<int>(im_offset));
  kernel.SetArgument(idx++, col_buffer());
  kernel.SetArgument(idx++, static_cast<int>(col_offset));
  kernel.SetArgument(idx++, static_cast<int>(approx_info.perf_offset));
  kernel.SetArgument(idx++, static_cast<int>(approx_info.perf_stride));
  if (approx_info.mode == ApproxMode::SAMP_KERNEL) {
    kernel.SetArgument(idx++, static_cast<int>(approxhpvm_perf_out_size(
                                  kernel_h * kernel_w * channels, approx_info.perf_offset,
                                  approx_info.perf_stride)));
  }

  size_t real_col_w, real_col_h;
  if (approx_info.mode == ApproxMode::PERF_ROW) {
    real_col_w = col_w;
    real_col_h = approxhpvm_perf_out_size(col_h, approx_info.perf_offset, approx_info.perf_stride);
  } else if (approx_info.mode == ApproxMode::PERF_COL) {
    real_col_w = approxhpvm_perf_out_size(col_w, approx_info.perf_offset, approx_info.perf_stride);
    real_col_h = col_h;
  } else {
    real_col_w = col_w;
    real_col_h = col_h;
  }

  const auto dimy_mul =
      approx_info.mode == ApproxMode::SAMP_KERNEL ? batch_size : batch_size * channels;
  // Launches the kernel
  const auto w_ceiled = Ceil(real_col_w, db_["COPY_DIMX"]);
  const auto h_ceiled = Ceil(real_col_h, db_["COPY_DIMY"]);
  const auto global = std::vector<size_t>{w_ceiled, h_ceiled * dimy_mul};
  const auto local = std::vector<size_t>{db_["COPY_DIMX"], db_["COPY_DIMY"]};
  RunKernel(kernel, queue_, device_, global, local, event_);
}

// =================================================================================================

// Compiles the templated class
template class Xim2colApprox<half>;
template class Xim2colApprox<float>;
template class Xim2colApprox<double>;
template class Xim2colApprox<float2>;
template class Xim2colApprox<double2>;

// =================================================================================================
} // namespace clblast
