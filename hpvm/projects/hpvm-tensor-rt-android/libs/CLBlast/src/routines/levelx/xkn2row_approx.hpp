
#ifndef CLBLAST_ROUTINES_XKN2ROW_APPROX_H_
#define CLBLAST_ROUTINES_XKN2ROW_APPROX_H_

#include "routine.hpp"

#include "utilities/approxhpvm_util.hpp"

namespace clblast {

// =================================================================================================

// See comment at top of file for a description of the class
template <typename T> class Xkn2rowApprox : public Routine {
public:
  // Constructor
  Xkn2rowApprox(Queue &queue, EventPointer event, const std::string &name = "KN2ROWAPPROX");

  // Templated-precision implementation of the routine
  void DoKn2rowApprox(const KernelMode kernel_mode, const size_t nfilters, const size_t channels,
                      const size_t kernel_h, const size_t kernel_w, const Buffer<T> &kn_buffer,
                      const size_t kn_offset, const Buffer<T> &row_buffer, const size_t row_offset,
                      const ApproxInfo &approx_info);
};

// =================================================================================================
} // namespace clblast

// CLBLAST_ROUTINES_XKN2ROW_APPROX_H_
#endif
