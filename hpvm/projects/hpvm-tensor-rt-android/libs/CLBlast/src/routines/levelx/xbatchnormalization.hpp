#ifndef CLBLAST_ROUTINES_BATCHNORMALIZATION_H
#define CLBLAST_ROUTINES_BATCHNORMALIZATION_H

#include "clblast.h"
#include "routine.hpp"

namespace clblast {

template <typename T> class XbatchNormalization : public Routine {
public:
  // Constructor
  XbatchNormalization(Queue &queue, EventPointer event,
                      const std::string &name = "BATCHNORMALIZATION");

  // Templated-precision implementation of the routine
  void
  DoBatchNormalization(const size_t n_total, const size_t channels, //
                       const size_t per_channel,                    //
                       const Buffer<T> &in_buffer, const size_t in_offset,
                       const Buffer<T> &out_buffer, const size_t out_offset,
                       const Buffer<T> &var_buffer, const size_t var_offset,
                       const Buffer<T> &mean_buffer, const size_t mean_offset,
                       const Buffer<T> &beta_buffer, const size_t beta_offset,
                       const Buffer<T> &gamma_buffer, const size_t gamma_offset,
                       const T epsilon);
};

} // namespace clblast

#endif // CLBLAST_ROUTINES_BATCHNORMALIZATION_H
