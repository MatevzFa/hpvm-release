
// =================================================================================================
// This file is part of the CLBlast project. The project is licensed under
// Apache Version 2.0. This project loosely follows the Google C++ styleguide
// and uses a tab-size of two spaces and a max- width of 100 characters per
// line.
//
// Author(s):
//   Cedric Nugteren <www.cedricnugteren.nl>
//
// This file implements the Xomatcopy routine. The precision is implemented
// using a template argument.
//
// =================================================================================================

#ifndef CLBLAST_ROUTINES_XCAST_H
#define CLBLAST_ROUTINES_XCAST_H

#include "clblast.h"
#include "routine.hpp"

namespace clblast {
// =================================================================================================

// See comment at top of file for a description of the class
template <typename From, typename To> class Xcast : public Routine {
public:
  // Constructor
  Xcast(Queue &queue, EventPointer event,
        const std::string &name = "ACTIVATION");

  // Templated-precision implementation of the routine
  void DoCast(const size_t n, const Buffer<From> &a_buffer,
              const size_t a_offset, const Buffer<To> &b_buffer,
              const size_t b_offset);
};

// =================================================================================================
} // namespace clblast

// CLBLAST_ROUTINES_XCAST_H
#endif
