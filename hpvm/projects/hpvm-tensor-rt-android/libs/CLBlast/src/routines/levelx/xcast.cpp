
// =================================================================================================
// This file is part of the CLBlast project. The project is licensed under
// Apache Version 2.0. This project loosely follows the Google C++ styleguide
// and uses a tab-size of two spaces and a max- width of 100 characters per
// line.
//
// Author(s):
//   Cedric Nugteren <www.cedricnugteren.nl>
//
// This file implements the Xomatcopy class (see the header for information
// about the class).
//
// =================================================================================================

#include "routines/levelx/xcast.hpp"

#include <string>
#include <vector>

#include "clblast.h"
#include "utilities/clblast_exceptions.hpp"
#include "utilities/utilities.hpp"

namespace clblast {
// =================================================================================================

// Constructor: forwards to base class constructor
template <typename From, typename To>
Xcast<From, To>::Xcast(Queue &queue, EventPointer event,
                       const std::string &name)
    : Routine(queue, event, name, {}, PrecisionValue<half>(), {},
              {
#include "../../kernels/levelx/cast.opencl"
              }) {
}

// =================================================================================================

template <typename From, typename To>
inline const std::string get_kernel_name();

template <> inline const std::string get_kernel_name<half, float>() {
  return "XcastHalf2Full";
}

template <> inline const std::string get_kernel_name<float, half>() {
  return "XcastFull2Half";
}

// =================================================================================================

// The main routine
template <typename From, typename To>
void Xcast<From, To>::DoCast(const size_t n, const Buffer<From> &a_buffer,
                             const size_t a_offset, const Buffer<To> &b_buffer,
                             const size_t b_offset) {
  // Flip the output along kernel_h and kernel_w, or not.
  std::string kernel_name = get_kernel_name<From, To>();

  // Makes sure all dimensions are larger than zero
  if ((n == 0)) {
    throw BLASError(StatusCode::kInvalidDimension);
  }

  // Retrieves the kernel from the compiled binary
  auto kernel = Kernel(program_, kernel_name);

  size_t idx = 0;
  // Sets the kernel arguments
  kernel.SetArgument(idx++, static_cast<int>(n));
  kernel.SetArgument(idx++, a_buffer());
  kernel.SetArgument(idx++, static_cast<int>(a_offset));
  kernel.SetArgument(idx++, b_buffer());
  kernel.SetArgument(idx++, static_cast<int>(b_offset));

  const size_t lws = 16;
  // Launches the kernel
  const auto w_ceiled = Ceil(n, lws);
  const auto global = std::vector<size_t>{w_ceiled};
  const auto local = std::vector<size_t>{lws};
  RunKernel(kernel, queue_, device_, global, local, event_);
}

// =================================================================================================

// Compiles the templated class
template class Xcast<half, float>;
template class Xcast<float, half>;

// =================================================================================================
} // namespace clblast
