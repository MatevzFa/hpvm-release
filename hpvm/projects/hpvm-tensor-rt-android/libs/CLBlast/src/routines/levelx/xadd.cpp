#include "xadd.hpp"

namespace clblast {

template <typename T>
Xadd<T>::Xadd(Queue &queue, EventPointer event, const std::string &name)
    : Routine(queue, event, name, {}, PrecisionValue<T>(), {},
              {
#include "../../kernels/levelx/add.opencl"
              }) {
}

template <typename T>
void Xadd<T>::DoAdd(const size_t n, const size_t c, const size_t h,
                    const size_t w, const Buffer<T> &a_buffer,
                    const size_t a_offset, const Buffer<T> &b_buffer,
                    const size_t b_offset, const Buffer<T> &out_buffer,
                    const size_t out_offset) {

  std::string kernel_name = "Xadd";

  // Makes sure all dimensions are larger than zero
  if ((n == 0) || (c == 0) || (h == 0) || (w == 0)) {
    throw BLASError(StatusCode::kInvalidDimension);
  }

  // Retrieves the kernel from the compiled binary
  auto kernel = Kernel(program_, kernel_name);

  size_t idx = 0;
  // Sets the kernel arguments
  kernel.SetArgument(idx++, static_cast<int>(n));
  kernel.SetArgument(idx++, static_cast<int>(c));
  kernel.SetArgument(idx++, static_cast<int>(h));
  kernel.SetArgument(idx++, static_cast<int>(w));
  kernel.SetArgument(idx++, a_buffer());
  kernel.SetArgument(idx++, static_cast<int>(a_offset));
  kernel.SetArgument(idx++, b_buffer());
  kernel.SetArgument(idx++, static_cast<int>(b_offset));
  kernel.SetArgument(idx++, out_buffer());
  kernel.SetArgument(idx++, static_cast<int>(out_offset));

  const size_t lws_x = 16;
  const size_t lws_y = 16;
  // Launches the kernel
  const auto w_ceiled = Ceil(w, lws_x);
  const auto h_ceiled = Ceil(n * c, lws_y);
  const auto global = std::vector<size_t>{w_ceiled, h_ceiled};
  const auto local = std::vector<size_t>{lws_x, lws_y};
  RunKernel(kernel, queue_, device_, global, local, event_);
}

template class Xadd<float>;
template class Xadd<half>;

} // namespace clblast