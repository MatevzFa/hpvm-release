
// =================================================================================================
// This file is part of the CLBlast project. The project is licensed under Apache Version 2.0. This
// project loosely follows the Google C++ styleguide and uses a tab-size of two spaces and a max-
// width of 100 characters per line.
//
// Author(s):
//   Cedric Nugteren <www.cedricnugteren.nl>
//
// This file implements the Xomatcopy routine. The precision is implemented using a template
// argument.
//
// =================================================================================================

#ifndef CLBLAST_ROUTINES_XAPPROXINTERPOLATE_H_
#define CLBLAST_ROUTINES_XAPPROXINTERPOLATE_H_

#include "clblast.h"
#include "routine.hpp"

namespace clblast {
// =================================================================================================

// See comment at top of file for a description of the class
template <typename T>
class XapproxInterpolate : public Routine {
 public:
  // Constructor
  XapproxInterpolate(Queue &queue, EventPointer event, const std::string &name = "OMATCOPY");

  // Templated-precision implementation of the routine
  void DoApproxInterpolate(const size_t channels, const size_t orig_height, const size_t orig_width,
                           const ApproxInfo &approx_info, const Buffer<T> &a_buffer,
                           const size_t a_offset, const Buffer<T> &b_buffer, const size_t b_offset);
};

// =================================================================================================
}  // namespace clblast

// CLBLAST_ROUTINES_XAPPROXINTERPOLATE_H_
#endif
