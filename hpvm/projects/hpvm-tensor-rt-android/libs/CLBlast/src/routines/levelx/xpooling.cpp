#include "routines/levelx/xpooling.hpp"

#include <string>
#include <vector>

#include "clblast.h"
#include "utilities/approxhpvm_util.hpp"

namespace clblast {
// =================================================================================================

// Constructor: forwards to base class constructor
template <typename T>
Xpooling<T>::Xpooling(Queue &queue, EventPointer event, const std::string &name)
    : Routine(queue, event, name, {"Copy"}, PrecisionValue<T>(), {},
              {
#include "../../kernels/levelx/pooling.opencl"
              }) {
}

// =================================================================================================

int poolmode(PoolMode mode) {
  switch (mode) {
    case PoolMode::AVG:
      return 0;
    case PoolMode::MIN:
      return 1;
    case PoolMode::MAX:
      return 2;
  }
}

// The main routine
template <typename T>
void Xpooling<T>::DoPooling(PoolMode mode, const size_t channels,          //
                            const size_t height, const size_t width,       //
                            const size_t pool_h, const size_t pool_w,      //
                            const size_t pad_h, const size_t pad_w,        //
                            const size_t stride_h, const size_t stride_w,  //
                            const Buffer<T> &in_buffer, const size_t in_offset,
                            const Buffer<T> &out_buffer, const size_t out_offset) {
  // Flip the output along kernel_h and kernel_w, or not.
  const auto kernel_name = "Xpooling";

  // Makes sure all dimensions are larger than zero
  if ((channels == 0) || (height == 0) || (width == 0)) {
    throw BLASError(StatusCode::kInvalidDimension);
  }

  auto out_w = conv_size(width, pool_w, 2 * pad_w, stride_w);
  auto out_h = conv_size(height, pool_h, 2 * pad_h, stride_h);

  // Retrieves the kernel from the compiled binary
  auto kernel = Kernel(program_, kernel_name);

  size_t idx = 0;
  // Sets the kernel arguments
  kernel.SetArgument(idx++, static_cast<int>(channels));
  kernel.SetArgument(idx++, static_cast<int>(poolmode(mode)));
  kernel.SetArgument(idx++, static_cast<int>(height));
  kernel.SetArgument(idx++, static_cast<int>(width));
  kernel.SetArgument(idx++, static_cast<int>(out_h));
  kernel.SetArgument(idx++, static_cast<int>(out_w));
  kernel.SetArgument(idx++, static_cast<int>(pool_h));
  kernel.SetArgument(idx++, static_cast<int>(pool_w));
  kernel.SetArgument(idx++, static_cast<int>(pad_h));
  kernel.SetArgument(idx++, static_cast<int>(pad_w));
  kernel.SetArgument(idx++, static_cast<int>(stride_h));
  kernel.SetArgument(idx++, static_cast<int>(stride_w));
  kernel.SetArgument(idx++, in_buffer());
  kernel.SetArgument(idx++, static_cast<int>(in_offset));
  kernel.SetArgument(idx++, out_buffer());
  kernel.SetArgument(idx++, static_cast<int>(out_offset));

  // Launches the kernel
  const size_t lws = 16;
  const auto s_ceiled = Ceil(out_w * out_h * channels, lws);
  const auto global = std::vector<size_t>{s_ceiled};
  const auto local = std::vector<size_t>{lws};
  RunKernel(kernel, queue_, device_, global, local, event_);
}

// =================================================================================================

// Compiles the templated class
template class Xpooling<half>;
template class Xpooling<float>;
template class Xpooling<double>;
template class Xpooling<float2>;
template class Xpooling<double2>;

// =================================================================================================
}  // namespace clblast
