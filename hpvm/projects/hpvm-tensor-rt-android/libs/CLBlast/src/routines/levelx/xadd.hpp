#ifndef CLBLAST_ROUTINES_ADD_H
#define CLBLAST_ROUTINES_ADD_H

#include "clblast.h"
#include "routine.hpp"

namespace clblast {

template <typename T> class Xadd : public Routine {
public:
  // Constructor
  Xadd(Queue &queue, EventPointer event, const std::string &name = "ADD");

  // Templated-precision implementation of the routine
  void DoAdd(const size_t n, const size_t c, const size_t h, const size_t w,
             const Buffer<T> &a_buffer, const size_t a_offset,
             const Buffer<T> &b_buffer, const size_t b_offset,
             const Buffer<T> &out_buffer, const size_t out_offset);
};

} // namespace clblast

#endif // CLBLAST_ROUTINES_ADD_H
