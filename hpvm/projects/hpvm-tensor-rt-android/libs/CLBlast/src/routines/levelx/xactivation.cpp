
// =================================================================================================
// This file is part of the CLBlast project. The project is licensed under Apache Version 2.0. This
// project loosely follows the Google C++ styleguide and uses a tab-size of two spaces and a max-
// width of 100 characters per line.
//
// Author(s):
//   Cedric Nugteren <www.cedricnugteren.nl>
//
// This file implements the Xomatcopy class (see the header for information about the class).
//
// =================================================================================================

#include "routines/levelx/xactivation.hpp"

#include <string>
#include <vector>

#include "clblast.h"
#include "utilities/approxhpvm_util.hpp"
#include "utilities/clblast_exceptions.hpp"
#include "utilities/utilities.hpp"

namespace clblast {
// =================================================================================================

// Constructor: forwards to base class constructor
template <typename T>
Xactivation<T>::Xactivation(Queue &queue, EventPointer event, const std::string &name)
    : Routine(queue, event, name, {"Copy"}, PrecisionValue<T>(), {},
              {
#include "../../kernels/levelx/activation.opencl"
              }) {
}

// =================================================================================================

// The main routine
template <typename T>
void Xactivation<T>::DoActivation(const ActivationFunc &act, const size_t channels, const size_t w,
                                  const size_t h, const T &a, const T &b,
                                  const Buffer<T> &in_buffer, const size_t in_offset,
                                  const Buffer<T> &out_buffer, const size_t out_offset) {
  // Flip the output along kernel_h and kernel_w, or not.
  std::string kernel_name = "X";
  switch (act) {
    case ActivationFunc::TANH:
      kernel_name += "tanh";
      break;
    case ActivationFunc::RELU:
      kernel_name += "relu";
      break;
    case ActivationFunc::CLIPPED_RELU:
      kernel_name += "clippedRelu";
      break;
    default:
      throw BLASError(StatusCode::kUnexpectedError);
  }

  // Makes sure all dimensions are larger than zero
  if ((channels == 0) || (w == 0) || (h == 0)) {
    throw BLASError(StatusCode::kInvalidDimension);
  }

  // Retrieves the kernel from the compiled binary
  auto kernel = Kernel(program_, kernel_name);

  size_t idx = 0;
  // Sets the kernel arguments
  kernel.SetArgument(idx++, static_cast<int>(channels * w * h));
  kernel.SetArgument(idx++, GetRealArg(a));
  kernel.SetArgument(idx++, GetRealArg(b));
  kernel.SetArgument(idx++, in_buffer());
  kernel.SetArgument(idx++, static_cast<int>(in_offset));
  kernel.SetArgument(idx++, out_buffer());
  kernel.SetArgument(idx++, static_cast<int>(out_offset));

  const size_t lws = 16;
  // Launches the kernel
  const auto w_ceiled = Ceil(channels * w * h, lws);
  const auto global = std::vector<size_t>{w_ceiled};
  const auto local = std::vector<size_t>{lws};
  RunKernel(kernel, queue_, device_, global, local, event_);
}

// =================================================================================================

// Compiles the templated class
template class Xactivation<half>;
template class Xactivation<float>;
// template class Xactivation<double>;
// template class Xactivation<float2>;
// template class Xactivation<double2>;

// =================================================================================================
}  // namespace clblast
