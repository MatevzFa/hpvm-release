#include "xaddbias.hpp"

namespace clblast {

template <typename T>
XaddBias<T>::XaddBias(Queue &queue, EventPointer event, const std::string &name)
    : Routine(queue, event, name, {}, PrecisionValue<T>(), {},
              {
#include "../../kernels/levelx/addbias.opencl"
              }) {
}

template <typename T>
void XaddBias<T>::DoAddBias(const size_t n_total, const size_t channels, //
                            const size_t per_channel,                    //
                            const Buffer<T> &in_buffer, const size_t in_offset,
                            const Buffer<T> &out_buffer,
                            const size_t out_offset,
                            const Buffer<T> &bias_buffer,
                            const size_t bias_offset) {

  std::string kernel_name = "XaddBias";

  // Makes sure all dimensions are larger than zero
  if ((n_total == 0) || (channels == 0) || (per_channel == 0)) {
    throw BLASError(StatusCode::kInvalidDimension);
  }

  // Retrieves the kernel from the compiled binary
  auto kernel = Kernel(program_, kernel_name);

  size_t idx = 0;
  // Sets the kernel arguments
  kernel.SetArgument(idx++, static_cast<int>(n_total));
  kernel.SetArgument(idx++, static_cast<int>(channels));
  kernel.SetArgument(idx++, static_cast<int>(per_channel));
  kernel.SetArgument(idx++, in_buffer());
  kernel.SetArgument(idx++, static_cast<int>(in_offset));
  kernel.SetArgument(idx++, out_buffer());
  kernel.SetArgument(idx++, static_cast<int>(out_offset));
  kernel.SetArgument(idx++, bias_buffer());
  kernel.SetArgument(idx++, static_cast<int>(bias_offset));

  const size_t lws = 16;
  // Launches the kernel
  const auto w_ceiled = Ceil(n_total, lws);
  const auto global = std::vector<size_t>{w_ceiled};
  const auto local = std::vector<size_t>{lws};
  RunKernel(kernel, queue_, device_, global, local, event_);
}

template class XaddBias<float>;
template class XaddBias<half>;

} // namespace clblast