
#ifndef CLBLAST_ROUTINES_XIM2COL_APPROX_H_
#define CLBLAST_ROUTINES_XIM2COL_APPROX_H_

#include "routine.hpp"

#include "utilities/approxhpvm_util.hpp"

namespace clblast {

// =================================================================================================

// See comment at top of file for a description of the class
template <typename T> class Xim2colApprox : public Routine {
public:
  // Constructor
  Xim2colApprox(Queue &queue, EventPointer event,
                const std::string &name = "IM2COLAPPROX");

  // Templated-precision implementation of the routine
  void DoIm2colApprox(const KernelMode kernel_mode, const size_t batch_size,
                      const size_t channels, const size_t height,
                      const size_t width, const size_t kernel_h,
                      const size_t kernel_w, const size_t pad_h,
                      const size_t pad_w, const size_t stride_h,
                      const size_t stride_w, const size_t dilation_h,
                      const size_t dilation_w, const Buffer<T> &im_buffer,
                      const size_t im_offset, const Buffer<T> &col_buffer,
                      const size_t col_offset, const ApproxInfo &approx_info);
};

// =================================================================================================
} // namespace clblast

// CLBLAST_ROUTINES_XIM2COL_APPROX_H_
#endif
