
#ifndef CLBLAST_ROUTINES_XPOOLING_H_
#define CLBLAST_ROUTINES_XPOOLING_H_

#include "routine.hpp"

#include "utilities/approxhpvm_util.hpp"

namespace clblast {



// =================================================================================================

// See comment at top of file for a description of the class
template <typename T> class Xpooling : public Routine {
public:
  // Constructor
  Xpooling(Queue &queue, EventPointer event,
           const std::string &name = "POOLING");

  // Templated-precision implementation of the routine
  void DoPooling(PoolMode mode, const size_t channels,               //
                 const size_t height, const size_t width,            //
                 const size_t pool_h, const size_t pool_w,           //
                 const size_t pad_h, const size_t pad_w,             //
                 const size_t stride_h, const size_t stride_w,       //
                 const Buffer<T> &in_buffer, const size_t in_offset, //
                 const Buffer<T> &out_buffer, const size_t out_offset);
};

// =================================================================================================
} // namespace clblast

// CLBLAST_ROUTINES_XPOOLING_H_
#endif
