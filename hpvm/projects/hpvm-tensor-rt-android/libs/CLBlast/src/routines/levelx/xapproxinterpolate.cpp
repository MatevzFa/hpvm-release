
// =================================================================================================
// This file is part of the CLBlast project. The project is licensed under
// Apache Version 2.0. This project loosely follows the Google C++ styleguide
// and uses a tab-size of two spaces and a max- width of 100 characters per
// line.
//
// Author(s):
//   Cedric Nugteren <www.cedricnugteren.nl>
//
// This file implements the Xomatcopy class (see the header for information
// about the class).
//
// =================================================================================================

#include "routines/levelx/xapproxinterpolate.hpp"

#include <string>
#include <vector>

#include "clblast.h"
#include "utilities/approxhpvm_util.hpp"
#include "utilities/clblast_exceptions.hpp"

namespace clblast {
// =================================================================================================

// Constructor: forwards to base class constructor
template <typename T>
XapproxInterpolate<T>::XapproxInterpolate(Queue &queue, EventPointer event,
                                          const std::string &name)
    : Routine(queue, event, name, {"Copy"}, PrecisionValue<T>(), {},
              {
#include "../../kernels/levelx/approxinterpolate.opencl"
              }) {
}

// =================================================================================================

// The main routine
template <typename T>
void XapproxInterpolate<T>::DoApproxInterpolate(
    const size_t channels, const size_t orig_height, const size_t orig_width,
    const ApproxInfo &approx_info, const Buffer<T> &in_buffer,
    const size_t in_offset, const Buffer<T> &out_buffer,
    const size_t out_offset) {
  // Flip the output along kernel_h and kernel_w, or not.
  std::string kernel_name = "Xinterpolate";
  switch (approx_info.mode) {
  case ApproxMode::PERF_ROW:
    kernel_name += "Row";
    break;
  case ApproxMode::PERF_COL:
    kernel_name += "Col";
    break;
  default:
    throw BLASError(StatusCode::kUnexpectedError);
  }

  // Makes sure all dimensions are larger than zero
  if ((channels == 0) || (orig_height == 0) || (orig_width == 0)) {
    throw BLASError(StatusCode::kInvalidDimension);
  }

  auto in_w =
      approx_info.mode == ApproxMode::PERF_COL
          ? approxhpvm_perf_out_size(orig_width, approx_info.perf_offset,
                                     approx_info.perf_stride)
          : orig_width;
  auto in_h =
      approx_info.mode == ApproxMode::PERF_ROW
          ? approxhpvm_perf_out_size(orig_height, approx_info.perf_offset,
                                     approx_info.perf_stride)
          : orig_height;

  // Retrieves the kernel from the compiled binary
  auto kernel = Kernel(program_, kernel_name);

  size_t idx = 0;
  // Sets the kernel arguments
  kernel.SetArgument(idx++, static_cast<int>(channels));
  kernel.SetArgument(idx++, static_cast<int>(in_h));
  kernel.SetArgument(idx++, static_cast<int>(in_w));
  kernel.SetArgument(idx++, static_cast<int>(orig_height));
  kernel.SetArgument(idx++, static_cast<int>(orig_width));
  kernel.SetArgument(idx++, static_cast<int>(approx_info.perf_offset));
  kernel.SetArgument(idx++, static_cast<int>(approx_info.perf_stride));
  kernel.SetArgument(idx++, in_buffer());
  kernel.SetArgument(idx++, static_cast<int>(in_offset));
  kernel.SetArgument(idx++, out_buffer());
  kernel.SetArgument(idx++, static_cast<int>(out_offset));

  // Launches the kernel
  const auto lws = 16;
  const auto ceiled = Ceil(orig_width * channels, lws);
  const auto global = std::vector<size_t>{ceiled};
  const auto local = std::vector<size_t>{lws};
  RunKernel(kernel, queue_, device_, global, local, event_);
}

// =================================================================================================

// Compiles the templated class
template class XapproxInterpolate<half>;
template class XapproxInterpolate<float>;
template class XapproxInterpolate<double>;
template class XapproxInterpolate<float2>;
template class XapproxInterpolate<double2>;

// =================================================================================================
} // namespace clblast
