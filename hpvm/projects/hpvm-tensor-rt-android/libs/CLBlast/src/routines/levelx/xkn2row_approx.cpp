
// =================================================================================================
// This file is part of the CLBlast project. The project is licensed under
// Apache Version 2.0. This project loosely follows the Google C++ styleguide
// and uses a tab-size of two spaces and a max- width of 100 characters per
// line.
//
// Author(s):
//   Cedric Nugteren <www.cedricnugteren.nl>
//
// This file implements the Xkn2rowApprox class (see the header for information
// about the class).
//
// =================================================================================================

#include "routines/levelx/xkn2row_approx.hpp"
#include "utilities/approxhpvm_util.hpp"
#include "utilities/utilities.hpp"

#include <cstddef>
#include <string>
#include <vector>

namespace clblast {
// =================================================================================================

// Constructor: forwards to base class constructor
template <typename T>
Xkn2rowApprox<T>::Xkn2rowApprox(Queue &queue, EventPointer event, const std::string &name)
    : Routine(queue, event, name, {"Copy"}, PrecisionValue<T>(), {},
              {
#include "../../kernels/levelx/kn2row_approx.opencl"
              }) {
}

// =================================================================================================

template <typename From, typename To> To convert(From val);

template <> inline half convert<float, half>(float val) { return FloatToHalf(val); }
template <> inline float convert<float, float>(float val) { return val; }

// The main routine
template <typename T>
void Xkn2rowApprox<T>::DoKn2rowApprox(const KernelMode kernel_mode, const size_t nfilters,
                                      const size_t channels, const size_t kernel_h,
                                      const size_t kernel_w, const Buffer<T> &kn_buffer,
                                      const size_t kn_offset, const Buffer<T> &row_buffer,
                                      const size_t row_offset, const ApproxInfo &approx_info) {

  // Flip the output along kernel_h and kernel_w, or not.
  const auto kernel_name = (kernel_mode == KernelMode::kConvolution) ? "Xkn2rowApproxKernelFlip"
                                                                     : "Xkn2rowApproxKernelNormal";

  // Makes sure all dimensions are larger than zero
  if ((channels == 0) || (kernel_h == 0) || (kernel_w == 0)) {
    throw BLASError(StatusCode::kInvalidDimension);
  }

  size_t kernel_area = kernel_h * kernel_w * channels;
  size_t kernel_area_reduced =
      approx_info.mode == ApproxMode::SAMP_KERNEL
          ? approxhpvm_perf_out_size(kernel_area, approx_info.perf_offset, approx_info.perf_stride)
          : kernel_area;

  // Retrieves the kernel from the compiled binary
  auto kernel = Kernel(program_, kernel_name);
  size_t idx = 0;
  kernel.SetArgument(idx++, static_cast<int>(nfilters));
  kernel.SetArgument(idx++, static_cast<int>(kernel_h));
  kernel.SetArgument(idx++, static_cast<int>(kernel_w));
  kernel.SetArgument(idx++, static_cast<int>(kernel_area));
  kernel.SetArgument(idx++, static_cast<int>(kernel_area_reduced));
  kernel.SetArgument(idx++, kn_buffer());
  kernel.SetArgument(idx++, static_cast<int>(kn_offset));
  kernel.SetArgument(idx++, row_buffer());
  kernel.SetArgument(idx++, static_cast<int>(row_offset));
  kernel.SetArgument(idx++, static_cast<int>(approx_info.perf_offset));
  kernel.SetArgument(idx++, static_cast<int>(approx_info.perf_stride));

  // Sets the kernel arguments

  // Launches the kernel
  const auto w_ceiled = Ceil(kernel_area_reduced, db_["COPY_DIMX"]);
  const auto h_ceiled = Ceil(nfilters, db_["COPY_DIMY"]);
  const auto global = std::vector<size_t>{w_ceiled, h_ceiled};
  const auto local = std::vector<size_t>{db_["COPY_DIMX"], db_["COPY_DIMY"]};
  RunKernel(kernel, queue_, device_, global, local, event_);
}

// =================================================================================================

// Compiles the templated class
template class Xkn2rowApprox<half>;
template class Xkn2rowApprox<float>;
// template class Xkn2rowApprox<double>;
// template class Xkn2rowApprox<float2>;
// template class Xkn2rowApprox<double2>;

// =================================================================================================
} // namespace clblast
