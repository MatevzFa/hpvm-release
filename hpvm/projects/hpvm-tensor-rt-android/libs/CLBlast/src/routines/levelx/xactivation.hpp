
// =================================================================================================
// This file is part of the CLBlast project. The project is licensed under Apache Version 2.0. This
// project loosely follows the Google C++ styleguide and uses a tab-size of two spaces and a max-
// width of 100 characters per line.
//
// Author(s):
//   Cedric Nugteren <www.cedricnugteren.nl>
//
// This file implements the Xomatcopy routine. The precision is implemented using a template
// argument.
//
// =================================================================================================

#ifndef CLBLAST_ROUTINES_XACTIVATION_H
#define CLBLAST_ROUTINES_XACTIVATION_H

#include "clblast.h"
#include "routine.hpp"

namespace clblast {
// =================================================================================================

// See comment at top of file for a description of the class
template <typename T>
class Xactivation : public Routine {
 public:
  // Constructor
  Xactivation(Queue &queue, EventPointer event, const std::string &name = "ACTIVATION");

  // Templated-precision implementation of the routine
  void DoActivation(const ActivationFunc &act, const size_t channels, const size_t w, const size_t h,
                    const T &a, const T &b, const Buffer<T> &in_buffer, const size_t a_offset,
                    const Buffer<T> &out_buffer, const size_t b_offset);
};

// =================================================================================================
}  // namespace clblast

// CLBLAST_ROUTINES_XACTIVATION_H
#endif
