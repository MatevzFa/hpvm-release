R"(

// =================================================================================================

// Kernel flip version of the Xkn2rowApprox kernel (for convolution)
__kernel void XaddBias(const int n_total,
                       const int channels,    //
                       const int per_channel, //

                       const __global real *restrict in_buffer, //
                       const int in_offset,                     //

                       __global real *restrict out_buffer, //
                       const int out_offset,               //

                       const __global real *restrict bias, //
                       const int bias_offset               //
) {
  const int gid = get_global_id(0);

  if (gid < n_total) {
    const int gid_x = gid % per_channel;
    const int gid_c = (gid / per_channel) % channels;
    const int offset = (gid / per_channel) * per_channel;

    out_buffer[out_offset + offset + gid_x] =
        in_buffer[in_offset + offset + gid_x] + bias[bias_offset + gid_c];
  }
}

// =================================================================================================

// End of the C++11 raw string literal
)"
