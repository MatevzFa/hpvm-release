R"(

// Work-group size parameters re-used from the 'copy' kernel
#ifndef COPY_DIMX
#define COPY_DIMX 8  // Local workgroup size in the first dimension (w)
#endif
#ifndef COPY_DIMY
#define COPY_DIMY 8  // Local workgroup size in the second dimension (h)
#endif

// =================================================================================================

// =================================================================================================

#define POOL_AVG 0
#define POOL_MIN 1
#define POOL_MAX 2

// Kernel flip version of the Xkn2rowApprox kernel (for convolution)
__kernel void
Xpooling(const int channels,                     //
         const int mode,                         //
         const int in_h, const in_w,             //
         const int out_h, const int out_w,       //
         const int pool_h, const int pool_w,     //
         const int pad_h, const int pad_w,       //
         const int stride_h, const int stride_w, //

         const __global real *restrict in_buffer, const int in_offset, //
         __global real *out_buffer, const int out_offset               //
) {
    const int gid_x = get_global_id(0) % out_w;
    const int gid_y = (get_global_id(0) % (out_w*out_h)) / out_w;
    const int gid_z = (get_global_id(0) / (out_w*out_h));

    const int wg_x = gid_x / COPY_DIMX;
    const int wg_y = gid_y / COPY_DIMY;

    const int lid_x = get_local_id(0);
    const int lid_y = get_local_id(1);

    if (gid_x < out_w && gid_y < out_h && gid_z < channels) {

        const int in_x = gid_x * stride_w - pad_w;
        const int in_y = gid_y * stride_h - pad_h;

        const int in_ch_offset  = gid_z * in_h  * in_w;
        const int out_ch_offset = gid_z * out_h * out_w;

        real acc;
        if (mode == POOL_MAX) {
            acc = -INFINITY;
        } else if (mode == POOL_MIN) {
            acc = INFINITY;
        } else {
            acc = 0;
        }

        for (int y = 0; y < pool_h; y++) {
            for (int x = 0; x < pool_w; x++) {
                if ((in_x + x) < in_w && (in_y + y) < in_h) {
                    real val = in_buffer[in_offset + in_ch_offset + (in_y + y) * in_w + (in_x + x)];
                    if (mode == POOL_MAX) {
                        acc = max(acc, val);
                    } else if (mode == POOL_MIN) {
                        acc = min(acc, val);
                    }  else {
                        acc += val;
                    }
                }
            }
        }

        if (mode == POOL_AVG) {
            acc /= (pool_h * pool_w);
        }

        out_buffer[out_offset + out_ch_offset + gid_y * out_w + gid_x] = acc;
    }
}

// =================================================================================================

// End of the C++11 raw string literal
)"
