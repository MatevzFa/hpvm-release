
// =================================================================================================
// This file is part of the CLBlast project. The project is licensed under
// Apache Version 2.0. This project loosely follows the Google C++ styleguide
// and uses a tab-size of two spaces and a max- width of 100 characters per
// line.
//
// Author(s):
//   Cedric Nugteren <www.cedricnugteren.nl>
//
// This file contains the kn2row kernel.
//
// =================================================================================================

// Enables loading of this file using the C++ pre-processor's #include (C++11
// standard raw string literal). Comment-out this line for syntax-highlighting
// when developing.
R"(

// Work-group size parameters re-used from the 'copy' kernel
#ifndef COPY_DIMX
#define COPY_DIMX 8 // Local workgroup size in the first dimension (w)
#endif
#ifndef COPY_DIMY
#define COPY_DIMY 8 // Local workgroup size in the second dimension (h)
#endif

// =================================================================================================

// Main body of the kernel
INLINE_FUNC void Xkn2rowApprox(const int nfilters, const int kernel_h,
                               const int kernel_w, const int kernel_area,
                               const int kernel_area_reduced,
                               const bool kernel_flip,
                               const __global real *restrict kn_buffer,
                               const int kn_offset, __global real *row_buffer,
                               const int row_offset, const int perf_offset,
                               const int perf_stride) {

  const int kwh_id = get_global_id(0);
  const int flt_id = get_global_id(1);

  const float fac = (float)perf_stride / (float)(perf_stride - 1);

#if PRECISION == 16
  const real ffac = convert_half(fac);
#else
  const real ffac = fac;
#endif

  int kwh_id_in =
      approxhpvm_perf_index_restore(kwh_id, perf_offset, perf_stride);

  if (kernel_flip) {
    kwh_id_in = kernel_area - kwh_id_in - 1;
  }

  if (kwh_id_in < kernel_area && flt_id < nfilters) {
    const int in_idx = flt_id * kernel_area + kwh_id_in;       // sparse access
    const int out_idx = flt_id * kernel_area_reduced + kwh_id; // dense access
    row_buffer[row_offset + out_idx] = kn_buffer[kn_offset + in_idx] * ffac;
  }
}

// =================================================================================================

// Kernel flip version of the Xkn2rowApprox kernel (for convolution)
__kernel __attribute__((reqd_work_group_size(COPY_DIMX, COPY_DIMY, 1))) void
Xkn2rowApproxKernelFlip(const int nfilters, const int kernel_h,
                        const int kernel_w, const int kernel_area,
                        const int kernel_area_reduced,
                        const __global real *restrict kn_buffer,
                        const int kn_offset, __global real *row_buffer,
                        const int row_offset, const int perf_offset,
                        const int perf_stride) {
  const bool kernel_flip = true;
  Xkn2rowApprox(nfilters, kernel_h, kernel_w, kernel_area, kernel_area_reduced,
                kernel_flip, kn_buffer, kn_offset, row_buffer, row_offset,
                perf_offset, perf_stride);
}

// Normal version of the Xkn2rowApprox kernel (for cross-correlation)
__kernel __attribute__((reqd_work_group_size(COPY_DIMX, COPY_DIMY, 1))) void
Xkn2rowApproxKernelNormal(const int nfilters, const int kernel_h,
                          const int kernel_w, const int kernel_area,
                          const int kernel_area_reduced,
                          const __global real *restrict kn_buffer,
                          const int kn_offset, __global real *row_buffer,
                          const int row_offset, const int perf_offset,
                          const int perf_stride) {
  const bool kernel_flip = false;
  Xkn2rowApprox(nfilters, kernel_h, kernel_w, kernel_area, kernel_area_reduced,
                kernel_flip, kn_buffer, kn_offset, row_buffer, row_offset,
                perf_offset, perf_stride);
}

// =================================================================================================

// End of the C++11 raw string literal
)"
