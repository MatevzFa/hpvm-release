R"(

// =================================================================================================

__kernel void XcastHalf2Full(const int n,                             //
                             const __global half *restrict in_buffer, //
                             const int in_offset,                     //
                             __global float *out_buffer,              //
                             const int out_offset                     //
) {
  const int gid_x = get_global_id(0);

  if (gid_x < n) {
    out_buffer[out_offset + gid_x] =
        convert_float(in_buffer[in_offset + gid_x]);
  }
}

__kernel void XcastFull2Half(const int n,                              //
                             const __global float *restrict in_buffer, //
                             const int in_offset,                      //
                             __global half *out_buffer,                //
                             const int out_offset                      //
) {
  const int gid_x = get_global_id(0);

  if (gid_x < n) {
    out_buffer[out_offset + gid_x] = convert_half(in_buffer[in_offset + gid_x]);
  }
}

// =================================================================================================

// End of the C++11 raw string literal
)"
