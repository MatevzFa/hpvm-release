
// =================================================================================================
// This file is part of the CLBlast project. The project is licensed under
// Apache Version 2.0. This project loosely follows the Google C++ styleguide
// and uses a tab-size of two spaces and a max- width of 100 characters per
// line.
//
// Author(s):
//   Cedric Nugteren <www.cedricnugteren.nl>
//
// This file contains the im2col kernel.
//
// =================================================================================================

// Enables loading of this file using the C++ pre-processor's #include (C++11
// standard raw string literal). Comment-out this line for syntax-highlighting
// when developing.
R"(

// Work-group size parameters re-used from the 'copy' kernel
#ifndef COPY_DIMX
  #define COPY_DIMX 8      // Local workgroup size in the first dimension (w)
#endif
#ifndef COPY_DIMY
  #define COPY_DIMY 8      // Local workgroup size in the second dimension (h)
#endif

// =================================================================================================

// Main body of the kernel
INLINE_FUNC void Xim2colApprox_KERNEL(const int input_h, const int input_w, const int batch_size, const int channels,
                         const int output_h, const int output_w, const int output_dim_perf,
                         const int kernel_h, const int kernel_w,
                         const int pad_h, const int pad_w,
                         const int stride_h, const int stride_w,
                         const int dilation_h, const int dilation_w,
                         const bool kernel_flip,
                         const __global real* restrict im_buffer, const int im_offset,
                         __global real* col_buffer, const int col_offset,
                         const int perf_offset, const int perf_stride, const int kernel_area_reduced) {
  // Thread IDs
  const int w_id = get_global_id(0); 
  const int h_id = get_global_id(1) % output_h;
  const int n_id = get_global_id(1) / output_h;

  if (h_id < output_h && w_id < output_w && n_id < batch_size) {
    for (int khw_id = 0; khw_id < kernel_area_reduced; ++khw_id) {

      const int kchw_id_in = approxhpvm_perf_index_restore(khw_id, perf_offset, perf_stride);
      const int khw_id_in = kchw_id_in % (kernel_w * kernel_h);

      const int kh_id = khw_id_in / kernel_w;
      const int kw_id = khw_id_in % kernel_w;
      const int kc_id = kchw_id_in / (kernel_w * kernel_h);

      if (kc_id < channels) {
        // Retrieves the input value
        const int h_index = -pad_h + kh_id * dilation_h + stride_h * h_id;
        const int w_index = -pad_w + kw_id * dilation_w + stride_w * w_id;
        real val;
        if (h_index >= 0 && h_index < input_h && w_index >= 0 && w_index < input_w) {
          const int input_index = 
              n_id  * (channels * input_h * input_w) + 
              kc_id * (input_h * input_w)            + 
              h_index * input_w                      +
              w_index;
          val = im_buffer[input_index + im_offset];
        }
        else {
          SetToZero(val);
        }

        // Sets the output value
        const int kernel_index = (kernel_flip)
                                ? kernel_area_reduced - khw_id - 1
                                : khw_id;
        const int patch_index = w_id + output_w * h_id;
        const int output_index = patch_index + kernel_index * output_w * output_h +
                                 n_id * output_w * output_h * kernel_area_reduced;
        col_buffer[output_index + col_offset] = val;
      }
    }
  }
}

// =================================================================================================

// Kernel flip version of the Xim2colApprox kernel (for convolution)
__kernel __attribute__((reqd_work_group_size(COPY_DIMX, COPY_DIMY, 1)))
void Xim2colApproxKernelFlip_KERNEL(const int input_h, const int input_w, const int batch_size, const int channels,
                       const int output_h, const int output_w, const int output_dim_perf,
                       const int kernel_h, const int kernel_w,
                       const int pad_h, const int pad_w,
                       const int stride_h, const int stride_w,
                       const int dilation_h, const int dilation_w,
                       const __global real* restrict im_buffer, const int im_offset,
                       __global real* col_buffer, const int col_offset,
                       const int perf_offset, const int perf_stride, const int kernel_area_reduced) {
  const bool kernel_flip = true;
  Xim2colApprox_KERNEL(input_h, input_w, batch_size, channels, output_h, output_w, output_dim_perf, kernel_h, kernel_w,
          pad_h, pad_w, stride_h, stride_w, dilation_h, dilation_w,
          kernel_flip,
          im_buffer, im_offset, col_buffer, col_offset,
          perf_offset, perf_stride, kernel_area_reduced);
}

// Normal version of the Xim2colApprox kernel (for cross-correlation)
__kernel __attribute__((reqd_work_group_size(COPY_DIMX, COPY_DIMY, 1)))
void Xim2colApproxKernelNormal_KERNEL(const int input_h, const int input_w, const int batch_size, const int channels,
                         const int output_h, const int output_w, const int output_dim_perf,
                         const int kernel_h, const int kernel_w,
                         const int pad_h, const int pad_w,
                         const int stride_h, const int stride_w,
                         const int dilation_h, const int dilation_w,
                         const __global real* restrict im_buffer, const int im_offset,
                         __global real* col_buffer, const int col_offset,
                         const int perf_offset, const int perf_stride, const int kernel_area_reduced) {
  const bool kernel_flip = false;
  Xim2colApprox_KERNEL(input_h, input_w, batch_size, channels, output_h, output_w, output_dim_perf, kernel_h, kernel_w,
          pad_h, pad_w, stride_h, stride_w, dilation_h, dilation_w,
          kernel_flip,
          im_buffer, im_offset, col_buffer, col_offset,
          perf_offset, perf_stride, kernel_area_reduced);
}

// =================================================================================================

// End of the C++11 raw string literal
)"

    // =================================================================================================
