R"(

// Work-group size parameters re-used from the 'copy' kernel
#ifndef COPY_DIMX
#define COPY_DIMX 8  // Local workgroup size in the first dimension (w)
#endif
#ifndef COPY_DIMY
#define COPY_DIMY 8  // Local workgroup size in the second dimension (h)
#endif

struct interp_info {
  int needs_interp;
  int idx_prev;
  int idx_this;
};

INLINE_FUNC struct interp_info calc_interp_info(int out_idx, int perf_offset, int perf_stride, int max_idx) {
  struct interp_info info;

  int nskipped = (out_idx - perf_offset + perf_stride - 1) / perf_stride;

  // printf("out_idx=%d offset=%d stride=%d max_idx=%d nskipped=%d\n", out_idx, perf_offset, perf_stride, max_idx, nskipped);

  if (out_idx < perf_offset) {
    // This row exist in src and no preivous row were perforated out

    info.idx_this = out_idx;
    info.needs_interp = 0;

  } else if ((out_idx - perf_offset) % perf_stride == 0) {
    // This row was perforated out
    // Average row_prev and row_this

    info.idx_prev = out_idx - nskipped - 1;
    info.idx_this = out_idx - nskipped;
    info.needs_interp = (info.idx_prev > -1);

    if (info.idx_this >= max_idx) {
      info.idx_this = info.idx_prev;
      info.needs_interp = 0;
    }

  } else {
    // This row exist in src but has to be offset due to some previous rows being perforated out

    info.idx_this = out_idx - nskipped;
    info.needs_interp = 0;
  }

  return info;
}

__kernel void XinterpolateRow(
    const int channels, const int in_h, const int in_w, const int out_h, const int out_w,
    const int perf_offset, const int perf_stride, const __global real *restrict in_buffer,
    const int in_offset, __global real *out_buffer, const int out_offset) {

  const int wh_mul = out_w * out_h;
  const int gid_x = get_global_id(0) % out_w;
  const int gid_z = get_global_id(0) / out_w;

  const int in_ch_offset = gid_z * in_h * in_w;
  const int out_ch_offset = gid_z * wh_mul;

  if (gid_x < out_w && gid_z < channels) {
    for (int gid_y = 0; gid_y < out_h; gid_y++) {
      struct interp_info info = calc_interp_info(gid_y, perf_offset, perf_stride, in_h);

      real dst_val = in_buffer[in_offset + in_ch_offset + info.idx_this * in_w + gid_x];

      if (info.needs_interp) {
        dst_val += in_buffer[in_offset + in_ch_offset + info.idx_prev * in_w + gid_x];
        dst_val /= 2;
      }

      out_buffer[out_offset + out_ch_offset + gid_y * out_w + gid_x] = dst_val;
    }
  }
}

__kernel void XinterpolateCol(
    const int channels, const int in_h, const int in_w, const int out_h, const int out_w,
    const int perf_offset, const int perf_stride, const __global real *restrict in_buffer,
    const int in_offset, __global real *out_buffer, const int out_offset) {

  const int wh_mul = out_w * out_h;
  const int gid_x = get_global_id(0) % out_w;
  const int gid_z = get_global_id(0) / out_w;

  const int in_ch_offset = gid_z * in_h * in_w;
  const int out_ch_offset = gid_z * wh_mul;

  if (gid_x < out_w && gid_z < channels) {
    struct interp_info info = calc_interp_info(gid_x, perf_offset, perf_stride, in_w);

    for (int gid_y = 0; gid_y < out_h; gid_y++) {

      real dst_val = in_buffer[in_offset + in_ch_offset + gid_y * in_w + info.idx_this];

      if (info.needs_interp) {
        dst_val += in_buffer[in_offset + in_ch_offset + gid_y * in_w + info.idx_prev];
        dst_val /= 2;
      }

      out_buffer[out_offset + out_ch_offset + gid_y * out_w + gid_x] = dst_val;
    }
  }
}

// =================================================================================================

// End of the C++11 raw string literal
)"

    // =================================================================================================
