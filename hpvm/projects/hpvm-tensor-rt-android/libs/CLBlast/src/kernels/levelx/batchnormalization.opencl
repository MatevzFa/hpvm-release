R"(

// =================================================================================================

__kernel void XbatchNormalization(
    const int n_total,     //
    const int channels,    //
    const int per_channel, //

    const __global real *restrict in_buffer, //
    const int in_offset,                     //

    __global real *restrict out_buffer, //
    const int out_offset,               //

    const __global real *restrict mean, const int mean_offset,   //
    const __global real *restrict var, const int var_offset,     //
    const __global real *restrict beta, const int beta_offset,   //
    const __global real *restrict gamma, const int gamma_offset, //
    const real_arg epsilon                                       //

) {
  const int gid = get_global_id(0);

  const real eps = GetRealArg(epsilon);

  if (gid < n_total) {
    const int gid_x = gid % per_channel;
    const int gid_c = (gid / per_channel) % channels;
    const int offset = (gid / per_channel) * per_channel;

    const real denominator = sqrt(var[var_offset + gid_c] + epsilon);
    const real numerator =
        in_buffer[in_offset + offset + gid_x] - mean[mean_offset + gid_c];
    const real x_bar = numerator / denominator;

    out_buffer[out_offset + offset + gid_x] =
        beta[beta_offset + gid_c] + x_bar * gamma[gamma_offset + gid_c];
  }
}

// =================================================================================================

// End of the C++11 raw string literal
)"
