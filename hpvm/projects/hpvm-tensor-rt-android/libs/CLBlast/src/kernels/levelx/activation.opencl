R"(

// =================================================================================================

// Kernel flip version of the Xkn2rowApprox kernel (for convolution)
__kernel void Xrelu(const int n,                                                   //
                    const real_arg a, const real_arg b,                            //
                    const __global real *restrict in_buffer, const int in_offset,  //
                    __global real *out_buffer, const int out_offset                //
) {
  const int gid_x = get_global_id(0);

  if (gid_x < n) {
    out_buffer[out_offset + gid_x] = max((real)ZERO, in_buffer[in_offset + gid_x]);
  }
}

__kernel void XclippedRelu(const int n,                                                   //
                           const real_arg a, const real_arg b,                            //
                           const __global real *restrict in_buffer, const int in_offset,  //
                           __global real *out_buffer, const int out_offset                //
) {
  const int gid_x = get_global_id(0);

  const real aa = GetRealArg(a);
  const real bb = GetRealArg(b);

  if (gid_x < n) {
    out_buffer[out_offset + gid_x] = clamp(in_buffer[in_offset + gid_x], aa, bb);
  }
}

__kernel void Xtanh(const int n,                                                   //
                    const real_arg a, const real_arg b,                            //
                    const __global real *restrict in_buffer, const int in_offset,  //
                    __global real *out_buffer, const int out_offset                //
) {
  const int gid_x = get_global_id(0);

  if (gid_x < n) {
    out_buffer[out_offset + gid_x] = tanh(in_buffer[in_offset + gid_x]);
  }
}

// =================================================================================================

// End of the C++11 raw string literal
)"
