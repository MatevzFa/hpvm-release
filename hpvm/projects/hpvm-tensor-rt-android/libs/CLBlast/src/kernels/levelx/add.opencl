R"(

// =================================================================================================

// Kernel flip version of the Xkn2rowApprox kernel (for convolution)
__kernel void Xadd(const int n, const int c, const int h, const int w,
                   const __global real *restrict a_buffer, const int a_offset,
                   const __global real *restrict b_buffer, const int b_offset,
                         __global real *restrict c_buffer, const int c_offset) {

  const int gid_x = get_global_id(0);
  const int gid_y = get_global_id(1);

  if (gid_x < w && gid_y < n*c) {

    const base_offset = gid_y * h*w + gid_x;

    for (int xh = 0, i = base_offset; xh < h; xh++, i += w) {
        c_buffer[c_offset + i] =
            a_buffer[a_offset + i] + b_buffer[b_offset + i];
    }
  }
}

// =================================================================================================

// End of the C++11 raw string literal
)"
