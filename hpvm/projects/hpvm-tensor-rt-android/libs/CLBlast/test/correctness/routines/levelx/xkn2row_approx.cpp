
// =================================================================================================
// This file is part of the CLBlast project. The project is licensed under Apache Version 2.0. This
// project loosely follows the Google C++ styleguide and uses a tab-size of two spaces and a max-
// width of 100 characters per line.
//
// Author(s):
//   Cedric Nugteren <www.cedricnugteren.nl>
//
// =================================================================================================

#include "test/correctness/testblas.hpp"
#include "test/routines/levelx/xkn2row_approx.hpp"

// Main function (not within the clblast namespace)
int main(int argc, char *argv[]) {
  auto errors = size_t{0};
  errors += clblast::RunTests<clblast::TestXkn2rowApprox<float>, float, float>(argc, argv, false, "Skn2rowApprox");
  errors += clblast::RunTests<clblast::TestXkn2rowApprox<double>, double, double>(argc, argv, true, "Dkn2rowApprox");
  errors += clblast::RunTests<clblast::TestXkn2rowApprox<clblast::float2>, clblast::float2, clblast::float2>(argc, argv, true, "Ckn2rowApprox");
  errors += clblast::RunTests<clblast::TestXkn2rowApprox<clblast::double2>, clblast::double2, clblast::double2>(argc, argv, true, "Zkn2rowApprox");
  errors += clblast::RunTests<clblast::TestXkn2rowApprox<clblast::half>, clblast::half, clblast::half>(argc, argv, true, "Hkn2rowApprox");
  if (errors > 0) { return 1; } else { return 0; }
}

// =================================================================================================
