#ifndef __TESTS_COMMON_H__
#define __TESTS_COMMON_H__

#include <array>
#include <cstdint>
#include <vector>

#include <gtest/gtest.h>

#include "clblast_rt/tensor.h"

#include "helpers.h"
#include "tensor_runtime.h"

template <typename T> using TensorHW = std::vector<std::vector<T>>;
template <typename T> using TensorC = std::vector<TensorHW<T>>;
template <typename T> using TensorBatch = std::vector<TensorC<T>>;

Tensor make(const TensorBatch<float> &batch);

Tensor make_i32(const TensorBatch<std::int32_t> &batch);

void release(Tensor &tensor);

bool is_close_float(Tensor &a, Tensor &b, float eps = 1e-6);
bool is_close_i32(Tensor &a, Tensor &b);

testing::AssertionResult pred_tensor_eq(const Tensor &a, const Tensor &b,
                                        float eps);

#endif // __TESTS_COMMON_H__
