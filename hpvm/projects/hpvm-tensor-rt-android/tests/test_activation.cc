#include <gtest/gtest.h>

#include <iostream>
#include <ostream>

#include "android_util.h"
#include "approx_api.h"
#include "clblast_rt/tensor.h"
#include "clblast_rt/utility.h"
#include "common.h"
#include "global_data.h"
#include "helpers.h"
#include "RtContext.h"
#include "tensor_runtime.h"

using namespace clblast_rt;

// Demonstrate some basic assertions.
TEST(Tensor, Tanh) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  auto im = make({{{
      {.1, .3, .4},
      {-.4, -2, 0},
      {-.05, 1, .05},
  }}});

  auto res = make({{{{0.09966799, 0.29131261, 0.37994896},
                     {-0.37994896, -0.96402758, 0.},
                     {-0.04995837, 0.76159416, 0.04995837}}}});

  Tensor *out = reinterpret_cast<Tensor *>(tensorTanh(&im));

  is_close_float(*out, res);

  // !! freeTensor(out); stack allocated !!
  release(im);
  release(res);

  freeBatchMemory();
}

TEST(Tensor, ReLU) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  auto im = make({{{
      {.1, .3, .4},
      {-.4, -2, 0},
      {-.05, 1, .05},
  }}});

  auto res = make({{{
      {.1, .3, .4},
      {0, 0, 0},
      {0, 1, .05},
  }}});

  Tensor *out = reinterpret_cast<Tensor *>(tensorRelu(&im));

  is_close_float(*out, res);

  // !! freeTensor(out); stack allocated !!
  release(im);
  release(res);

  freeBatchMemory();
}

TEST(Tensor, ClippedReLU) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  auto im = make({{{
      {.1, .3, .4},
      {-.4, -2, 0},
      {-.05, 1, .05},
  }}});

  auto res = make({{{
      {.01, .01, .01},
      {-.01, -.01, 0},
      {-.01, .01, .01},
  }}});

  Tensor *out = reinterpret_cast<Tensor *>(tensorRelu2(&im, -.01, .01));

  is_close_float(*out, res);

  // !! freeTensor(out); stack allocated !!
  release(im);
  release(res);

  freeBatchMemory();
}
