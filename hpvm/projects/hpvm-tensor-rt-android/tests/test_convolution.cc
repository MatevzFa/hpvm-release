#include <gtest/gtest.h>

#include <iostream>
#include <ostream>

#include "android_util.h"
#include "approx_api.h"
#include "clblast_rt/utility.h"
#include "common.h"
#include "global_data.h"
#include "helpers.h"
#include "RtContext.h"
#include "tensor_runtime.h"

using namespace clblast_rt;

// Demonstrate some basic assertions.
TEST(Tensor, Convolution) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  auto im = make({{{
                       {1, 1, 1, 1},
                       {1, 1, 1, 1},
                       {1, 1, 1, 1},
                       {1, 1, 1, 1},
                   },
                   {
                       {1, 1, 1, 1},
                       {1, 1, 1, 1},
                       {1, 1, 1, 1},
                       {1, 1, 1, 1},
                   }}});

  auto kn = make({{{
                       {1, 1, 1},
                       {1, 1, 1},
                       {1, 1, 1},
                   },
                   {
                       {1, 1, 1},
                       {1, 1, 1},
                       {1, 1, 1},
                   }}});

  Tensor *out =
      reinterpret_cast<Tensor *>(tensorConvolution(&im, &kn, 1, 1, 1, 1, 0, 1));

  auto res = make({{{
      {8, 12, 12, 8},
      {12, 18, 18, 12},
      {12, 18, 18, 12},
      {8, 12, 12, 8},
  }}});

  is_close_float(*out, res);

  freeTensor(out);
  release(im);
  release(kn);
  release(res);

  freeBatchMemory();
}

TEST(Tensor, ConvolutionApproxPerfRow) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  auto im = make({{{
      {1, 1, 1, 1},
      {1, 1, 1, 1},
      {1, 1, 1, 1},
      {1, 1, 1, 1},
  }}});

  auto kn = make({{{
      {1, 1, 1},
      {1, 1, 1},
      {1, 1, 1},
  }}});

  Tensor *out = reinterpret_cast<Tensor *>(
      tensorConvApprox(&im, &kn, 1, 1, 1, 1, 0, 1, 2, 1, 1, 0));

  auto res = make({{{
      // X {4, 6, 6, 6},
      //   {6, 9, 9, 6},
      // X {6, 9, 9, 6},
      //   {4, 6, 6, 4},
      {6, 9, 9, 6},
      {6, 9, 9, 6},
      {(6 + 4) / 2., (9 + 6) / 2., (9 + 6) / 2., (6 + 4) / 2.},
      {4, 6, 6, 4},

  }}});

  is_close_float(*out, res);

  freeTensor(out);
  release(im);
  release(kn);
  release(res);

  freeBatchMemory();
}

TEST(Tensor, ConvolutionApproxSampKernel) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  auto im = make({{{
                       {1, 1, 1, 1},
                       {1, 1, 1, 1},
                       {1, 1, 1, 1},
                       {1, 1, 1, 1},
                   },
                   {
                       {1, 1, 1, 1},
                       {1, 1, 1, 1},
                       {1, 1, 1, 1},
                       {1, 1, 1, 1},
                   }}});

  auto kn = make({{{
                       {1, 1, 1},
                       {1, 1, 1},
                       {1, 1, 1},
                   },
                   {
                       {1, 1, 1},
                       {1, 1, 1},
                       {1, 1, 1},
                   }}});

  Tensor *out = reinterpret_cast<Tensor *>(
      tensorConvApprox(&im, &kn, 1, 1, 1, 1, 0, 1, 1, 1, 2, 0));

  auto res = make({{{
      {8, 12, 12, 8},
      {12, 16, 16, 12},
      {12, 16, 16, 12},
      {8, 12, 12, 8},
  }}});

  is_close_float(*out, res);

  freeTensor(out);
  release(im);
  release(kn);
  release(res);

  freeBatchMemory();
}

TEST(Tensor, ConvolutionBatchBug) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  auto im = make({
      // batch[0]
      {{
           {1, 1, 1, 1},
           {1, 1, 1, 1},
           {1, 1, 1, 1},
           {1, 1, 1, 1},
       },
       {
           {1, 1, 1, 1},
           {1, 1, 1, 1},
           {1, 1, 1, 1},
           {1, 1, 1, 1},
       }},
      // batch[1]
      {{
           {1, 1, 1, 1},
           {1, 1, 1, 1},
           {1, 1, 1, 1},
           {1, 1, 1, 1},
       },
       {
           {1, 1, 1, 1},
           {1, 1, 1, 1},
           {1, 1, 1, 1},
           {1, 1, 1, 1},
       }},
      // batch[2]
      {{
           {1, 1, 1, 1},
           {1, 1, 1, 1},
           {1, 1, 1, 1},
           {1, 1, 1, 1},
       },
       {
           {1, 1, 1, 1},
           {1, 1, 1, 1},
           {1, 1, 1, 1},
           {1, 1, 1, 1},
       }},
  });

  auto kn = make({{{
                       {1, 1, 1},
                       {1, 1, 1},
                       {1, 1, 1},
                   },
                   {
                       {1, 1, 1},
                       {1, 1, 1},
                       {1, 1, 1},
                   }}});

  Tensor *out = reinterpret_cast<Tensor *>(
      tensorConvolution(&im, &kn, 1, 1, 1, 1, 0, 1));

  auto res = make({
      // batch[0]
      {{
          {8, 12, 12, 8},
          {12, 18, 18, 12},
          {12, 18, 18, 12},
          {8, 12, 12, 8},
      }},
      // batch[1]
      {{
          {8, 12, 12, 8},
          {12, 18, 18, 12},
          {12, 18, 18, 12},
          {8, 12, 12, 8},
      }},
      // batch[2]
      {{
          {8, 12, 12, 8},
          {12, 18, 18, 12},
          {12, 18, 18, 12},
          {8, 12, 12, 8},
      }},
  });

  is_close_float(*out, res);

  freeTensor(out);
  release(im);
  release(kn);
  release(res);

  freeBatchMemory();
}