#include <gtest/gtest.h>

#include <iostream>
#include <ostream>

#include "android_util.h"
#include "approx_api.h"
#include "clblast_rt/tensor.h"
#include "clblast_rt/utility.h"
#include "common.h"
#include "global_data.h"
#include "helpers.h"
#include "RtContext.h"
#include "tensor_runtime.h"

using namespace clblast_rt;

// Demonstrate some basic assertions.
TEST(Tensor, Softmax) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  auto im = make({
      {{{1, 1, 2, 1, 1, 3, 1, 1}}},
      {{{1, 1, 1, 1, 2, 1, 1, 1}}},
  });

  Tensor *out = reinterpret_cast<Tensor *>(tensorSoftmax(&im));

  // Computed via numpy
  auto res = make({
      {{{0.06208351, 0.06208351, 0.16876046, 0.06208351, 0.06208351, 0.4587385,
         0.06208351, 0.06208351}}},
      {{{0.10289885, 0.10289885, 0.10289885, 0.10289885, 0.27970807, 0.10289885,
         0.10289885, 0.10289885}}},
  });

  is_close_float(*out, res);

  //   freeTensor(out);
  release(im);
  release(res);

  freeBatchMemory();
}

// Demonstrate some basic assertions.
TEST(Tensor, ArgMax) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  auto im = make({
      {{{0.06208351, 0.06208351, 0.16876046, 0.06208351, 0.06208351, 0.4587385,
         0.06208351, 0.06208351}}},
      {{{0.10289885, 0.10289885, 0.10289885, 0.10289885, 0.27970807, 0.10289885,
         0.10289885, 0.10289885}}},
  });

  Tensor *out = reinterpret_cast<Tensor *>(tensorArgMax(&im));

  host2dev(out);

  auto res = make_i32({{{
      {5},
      {4},
  }}});

  is_close_i32(*out, res);

  //   freeTensor(out);
  release(im);
  release(res);

  freeBatchMemory();
}
