#include <gtest/gtest.h>

#include <iostream>
#include <ostream>

#include "android_util.h"
#include "approx_api.h"
#include "clblast_rt/utility.h"
#include "common.h"
#include "global_data.h"
#include "helpers.h"
#include "RtContext.h"
#include "tensor_runtime.h"

using namespace clblast_rt;

// Demonstrate some basic assertions.
TEST(Tensor, GemmSimple) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  auto im = make({{{
      {1, 1, 1},
      {1, 1, 1},
      {1, 1, 1},
  }}});

  auto weights = make({{{
      {1, -1},
      {1, -1},
      {1, -1},
      {1, -1},
      {1, -1},
      {1, -1},
      {1, -1},
      {1, -1},
      {1, -1},
  }}});

  Tensor *out = reinterpret_cast<Tensor *>(tensorGemmGPU(&im, &weights));

  auto res = make({{{
      {9, -9},
  }}});

  is_close_float(*out, res);

  freeTensor(out);
  release(im);
  release(weights);
  release(res);

  freeBatchMemory();
}

TEST(Tensor, GemmBatches) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  auto im = make({
      // batch[0]
      {{
           // ch[0]
           {1, 1},
           {1, 1},
       },
       {
           // ch[1]
           {1, 1},
           {1, 1},
       }},
      // batch[1]
      {{
           // ch[0]
           {-1, -1},
           {-1, -1},
       },
       {
           // ch[1]
           {-1, -1},
           {-1, -1},
       }},
  });

  auto weights = make({{{
      {1, -1},
      {1, -1},
      {1, -1},
      {1, -1},
      {1, -1},
      {1, -1},
      {1, -1},
      {1, -1},
  }}});

  Tensor *out = reinterpret_cast<Tensor *>(tensorGemmGPU(&im, &weights));

  auto res = make({{{
      {8, -8},
      {-8, 8},
  }}});

  is_close_float(*out, res);

  freeTensor(out);
  release(im);
  release(weights);
  release(res);

  freeBatchMemory();
}
