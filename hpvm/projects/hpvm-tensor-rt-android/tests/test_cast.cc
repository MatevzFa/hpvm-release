#include <gtest/gtest.h>

#include <iostream>
#include <ostream>

#include "android_util.h"
#include "approx_api.h"
#include "clblast_rt/tensor.h"
#include "clblast_rt/utility.h"
#include "common.h"
#include "constants.h"
#include "global_data.h"
#include "helpers.h"
#include "RtContext.h"
#include "tensor_runtime.h"

using namespace clblast_rt;

// Demonstrate some basic assertions.
TEST(Tensor, Casts) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  auto ref = make({{{
      {1, 2, 3, 4},
      {2, 3, 4, 5},
      {-5, -4, -3, -2},
      {-4, -3, -2, -1},
  }}});

  auto *tensor = create_tensor(DataType::FP32, ref.shape(), DataPlacement::DEV);
  initTensorData(tensor, ref.buf_host().raw(), ref.buf_host().size());

  full2half(tensor);

  EXPECT_FALSE(tensor->is_full_used());
  EXPECT_FALSE(tensor->buf_dev().is_allocated());

  half2full(tensor);

  EXPECT_FALSE(tensor->is_half_used());
  EXPECT_FALSE(tensor->buf_dev_half().is_allocated());

  is_close_float(*tensor, ref);

  freeTensor(tensor);
  release(ref);

  freeBatchMemory();
}