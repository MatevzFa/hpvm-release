#include <gtest/gtest.h>

#include <iostream>
#include <ostream>

#include "android_util.h"
#include "approx_api.h"
#include "clblast_rt/utility.h"
#include "common.h"
#include "global_data.h"
#include "helpers.h"
#include "RtContext.h"
#include "tensor_runtime.h"

using namespace clblast_rt;

// Demonstrate some basic assertions.
TEST(Tensor, Bias) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  auto im = make({{{
                       {1, 1, 1, 1},
                       {1, 1, 1, 1},
                       {1, 1, 1, 1},
                       {1, 1, 1, 1},
                   },
                   {
                       {2, 2, 2, 2},
                       {2, 2, 2, 2},
                       {2, 2, 2, 2},
                       {2, 2, 2, 2},
                   }}});

  auto bias = make({{
      {
          {1},
      },
      {
          {1},
      },
  }});

  Tensor *out = reinterpret_cast<Tensor *>(tensorAdd(&im, &bias));

  auto res = make({{{
                        {2, 2, 2, 2},
                        {2, 2, 2, 2},
                        {2, 2, 2, 2},
                        {2, 2, 2, 2},
                    },
                    {
                        {3, 3, 3, 3},
                        {3, 3, 3, 3},
                        {3, 3, 3, 3},
                        {3, 3, 3, 3},
                    }}});

  is_close_float(*out, res);

  //   freeTensor(out);
  release(im);
  release(res);

  freeBatchMemory();
}

TEST(Tensor, BiasDense) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  auto im = make({
      {{{1, 1, 1, 1}}},
      {{{1, 1, 1, 1}}},
      {{{1, 1, 1, 1}}},
  });

  auto bias = make({{
      {
          {1},
      },
      {
          {2},
      },
      {
          {3},
      },
      {
          {4},
      },
  }});

  Tensor *out = reinterpret_cast<Tensor *>(tensorAdd(&im, &bias));

  print("add bias", std::cout, rtCtx->queue(), out->type(), out->shape(),
        out->buf_dev().raw(), 0);

  auto res = make({{{
      {2, 3, 4, 5},
      {2, 3, 4, 5},
      {2, 3, 4, 5},
  }}});

  is_close_float(*out, res);

  //   freeTensor(out);
  release(im);
  release(res);

  freeBatchMemory();
}
