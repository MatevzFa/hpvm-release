#include <gtest/gtest.h>

#include <iostream>
#include <ostream>

#include "RtContext.h"
#include "android_util.h"
#include "approx_api.h"
#include "clblast.h"
#include "clblast_rt/op_convolution_approx.h"
#include "clblast_rt/tensor.h"
#include "clblast_rt/utility.h"
#include "clpp11.hpp"
#include "common.h"
#include "constants.h"
#include "global_data.h"
#include "helpers.h"
#include "tensor_runtime.h"

using namespace clblast_rt;

// Demonstrate some basic assertions.
TEST(Tensor, Kn2Row) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  clblast::ApproxInfo info{};
  info.mode = clblast::ApproxMode::SAMP_KERNEL;
  info.perf_offset = 1;
  info.perf_stride = 2;

  auto kn = make({{{
                       {1, 2, 3},
                       {4, 5, 6},
                       {7, 8, 9},
                   },
                   {
                       {10, 11, 12},
                       {13, 14, 15},
                       {16, 17, 18},
                   }},
                  {{
                       {21, 22, 23},
                       {24, 25, 26},
                       {27, 28, 29},
                   },
                   {
                       {30, 31, 32},
                       {33, 34, 35},
                       {36, 37, 38},
                   }}});

  auto *row =
      reinterpret_cast<Tensor *>(create4DTensor(TENSOR_TYPE_FLOAT, TENSOR_LAYOUT_NCHW, 1, 1, 2, 9));

  host2dev(&kn);

  using namespace clblast;

  clblast::Kn2rowApprox<float>(KernelMode::kCrossCorrelation, 2, 2, 3, 3, kn.buf_dev().raw(), 0,
                               row->buf_dev().raw(), 0, info, &rtCtx->queue());
  sync();

  print("row", std::cout, rtCtx->queue(), row->type(), row->shape(), row->buf_dev().raw(), 0);

  auto res = make({{{
      {2, 6, 10, 14, 18, 22, 26, 30, 34},
      {42, 46, 50, 54, 58, 62, 66, 70, 74},
  }}});

  is_close_float(*row, res);

  freeTensor(row);
  release(kn);
  release(res);

  freeBatchMemory();
}

TEST(Tensor, Im2Col) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  clblast::ApproxInfo info{};
  info.mode = clblast::ApproxMode::SAMP_KERNEL;
  info.perf_offset = 1;
  info.perf_stride = 2;

  auto im = make({
      {{
           {1, 2},
           {3, 4},
       },
       {
           {11, 12},
           {13, 14},
       }},
      //   {{
      //        {21, 22},
      //        {23, 24},
      //    },
      //    {
      //        {31, 32},
      //        {33, 34},
      //    }},
  });

  auto *col = reinterpret_cast<Tensor *>(
      create4DTensor(TENSOR_TYPE_FLOAT, TENSOR_LAYOUT_NCHW, 1, 1, 9 * 1, 4));

  host2dev(&im);

  using namespace clblast;

  clblast::Im2colApprox<float>(KernelMode::kCrossCorrelation, 1, 2, 2, 2, 3, 3, 1, 1, 1, 1, 1, 1,
                               im.buf_dev().raw(), 0, col->buf_dev().raw(), 0, info,
                               &rtCtx->queue());
  sync();

  print("col", std::cout, rtCtx->queue(), col->type(), col->shape(), col->buf_dev().raw(), 0);

  auto res = make({{{
      {0, 0, 0, 1},
      {0, 0, 2, 0},
      {1, 2, 3, 4},
      {0, 3, 0, 0},
      {4, 0, 0, 0},
      {0, 0, 11, 12},
      {0, 11, 0, 13},
      {12, 0, 14, 0},
      {13, 14, 0, 0},
  }}});

  is_close_float(*col, res);

  freeTensor(col);
  release(im);
  release(res);

  freeBatchMemory();
}