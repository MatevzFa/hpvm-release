#include <gtest/gtest.h>

#include <iostream>
#include <ostream>

#include "android_util.h"
#include "approx_api.h"
#include "clblast_rt/tensor.h"
#include "clblast_rt/utility.h"
#include "common.h"
#include "constants.h"
#include "global_data.h"
#include "helpers.h"
#include "RtContext.h"
#include "tensor_runtime.h"

using namespace clblast_rt;

// Demonstrate some basic assertions.
TEST(Tensor, PoolingMax) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  auto im = make({{{
      {2, 1, 1, 1},
      {1, 1, 1, 2},
      {1, 1, 3, 1},
      {2, 1, 1, 1},
  }}});

  Tensor *out = reinterpret_cast<Tensor *>(
      tensorPooling(&im, POOLING_MAX, 2, 2, 0, 0, 2, 2));

  // Computed via numpy
  auto res = make({{{
      {2, 2},
      {2, 3},
  }}});

  is_close_float(*out, res);

  //   freeTensor(out);
  release(im);
  release(res);

  freeBatchMemory();
}

TEST(Tensor, PoolingMin) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  auto im = make({{{
      {2, 1, 1, 1},
      {1, 1, 1, 2},
      {1, 1, 3, 1},
      {2, 1, 1, 1},
  }}});

  Tensor *out = reinterpret_cast<Tensor *>(
      tensorPooling(&im, POOLING_MIN, 2, 2, 0, 0, 2, 2));

  // Computed via numpy
  auto res = make({{{
      {1, 1},
      {1, 1},
  }}});

  is_close_float(*out, res);

  //   freeTensor(out);
  release(im);
  release(res);

  freeBatchMemory();
}

TEST(Tensor, PoolingAvg) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  auto im = make({{{
      {2, 1, 1, 1},
      {1, 1, 1, 2},
      {1, 1, 3, 1},
      {2, 1, 1, 1},
  }}});

  Tensor *out = reinterpret_cast<Tensor *>(
      tensorPooling(&im, POOLING_AVG, 2, 2, 0, 0, 2, 2));

  // Computed via numpy
  auto res = make({{{
      {(2 + 1 + 1 + 1) / 4., (1 + 1 + 1 + 2) / 4.},
      {(1 + 1 + 1 + 2) / 4., (3 + 1 + 1 + 1) / 4.},
  }}});

  is_close_float(*out, res);

  //   freeTensor(out);
  release(im);
  release(res);

  freeBatchMemory();
}