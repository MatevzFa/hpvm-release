#include "common.h"

#include "clblast_rt/tensor.h"
#include "helpers.h"

#include <gtest/gtest.h>

#include <algorithm>
#include <cstdint>
#include <cstring>
#include <stdexcept>

using namespace clblast_rt;

Tensor make(const TensorBatch<float> &batch) {
  size_t n = -1, c = -1, h = -1, w = -1;

  std::vector<float> linearized;

  n = batch.size();

  for (auto &tensor : batch) {
    if (c == -1) {
      c = tensor.size();
    } else if (c != tensor.size()) {
      throw std::runtime_error("invalid");
    }

    for (auto &channel : tensor) {
      if (h == -1) {
        h = channel.size();
      } else if (h != channel.size()) {
        throw std::runtime_error("invalid");
      }

      for (auto &row : channel) {
        if (w == -1) {
          w = row.size();
        } else if (w != row.size()) {
          throw std::runtime_error("invalid");
        }

        for (auto &el : row) {
          linearized.push_back(el);
        }
      }
    }
  }

  Shape shape(n, c, h, w);

  Tensor tensor(DataType::FP32, shape);

  tensor.set_data_placement(DataPlacement::HOST);
  tensor.buf_host().allocate();

  std::memcpy(tensor.buf_host().raw(), linearized.data(),
              tensor.buf_host().size());

  return tensor;
}

Tensor make_i32(const TensorBatch<int32_t> &batch) {
  size_t n = -1, c = -1, h = -1, w = -1;

  std::vector<int32_t> linearized;

  n = batch.size();

  for (auto &tensor : batch) {
    if (c == -1) {
      c = tensor.size();
    } else if (c != tensor.size()) {
      throw std::runtime_error("invalid");
    }

    for (auto &channel : tensor) {
      if (h == -1) {
        h = channel.size();
      } else if (h != channel.size()) {
        throw std::runtime_error("invalid");
      }

      for (auto &row : channel) {
        if (w == -1) {
          w = row.size();
        } else if (w != row.size()) {
          throw std::runtime_error("invalid");
        }

        for (auto &el : row) {
          linearized.push_back(el);
        }
      }
    }
  }

  Shape shape(n, c, h, w);

  Tensor tensor(DataType::I32, shape);

  tensor.set_data_placement(DataPlacement::HOST);
  tensor.buf_host().allocate();

  std::memcpy(tensor.buf_host().raw(), linearized.data(),
              tensor.buf_host().size());

  return tensor;
}

void release(Tensor &tensor) {
  tensor.buf_dev().release();
  tensor.buf_dev_half().release();
  tensor.buf_host().release();
}

bool is_close_float(Tensor &a, Tensor &b, float eps) {
  half2full(&a);
  half2full(&b);

  dev2host(&a);
  dev2host(&b);

  EXPECT_EQ(a.buf_host().size(), b.buf_host().size());
  EXPECT_EQ(a.shape().area(), b.shape().area());

  float *a_data = (float *)a.buf_host().raw();
  float *b_data = (float *)b.buf_host().raw();

  size_t area = a.shape().area();
  for (int i = 0; i < area; i++) {
    EXPECT_NEAR(a_data[i], b_data[i], eps);
  }

  return true;
}

testing::AssertionResult pred_tensor_eq(const Tensor &a, const Tensor &b,
                                        float rel_err) {

  Tensor &a_mut = const_cast<Tensor &>(a);
  Tensor &b_mut = const_cast<Tensor &>(b);

  half2full(&a_mut);
  half2full(&b_mut);

  dev2host(&a_mut);
  dev2host(&b_mut);

  EXPECT_EQ(a_mut.buf_host().size(), b_mut.buf_host().size());
  EXPECT_EQ(a_mut.shape().area(), b_mut.shape().area());

  float *a_data = (float *)a_mut.buf_host().raw();
  float *b_data = (float *)b_mut.buf_host().raw();

  size_t area = a_mut.shape().area();

  for (int i = 0; i < area; i++) {

    if (abs(b_data[i]) < 1e-12) {
      if (abs(a_data[i]) > 1e-12) {
        return testing::AssertionFailure();
      }
    } else {
      float relative = abs((a_data[i] - b_data[i]) / b_data[i]);
      if (relative > rel_err) {
        return testing::AssertionFailure();
      }
    }
  }

  return testing::AssertionSuccess();
}

bool is_close_i32(Tensor &a, Tensor &b) {
  half2full(&a);
  half2full(&b);

  dev2host(&a);
  dev2host(&b);

  EXPECT_EQ(a.buf_host().size(), b.buf_host().size());
  EXPECT_EQ(a.shape().area(), b.shape().area());

  int32_t *a_data = (int32_t *)a.buf_host().raw();
  int32_t *b_data = (int32_t *)b.buf_host().raw();

  size_t area = a.shape().area();
  for (int i = 0; i < area; i++) {
    EXPECT_EQ(a_data[i], b_data[i]);
  }

  return true;
}