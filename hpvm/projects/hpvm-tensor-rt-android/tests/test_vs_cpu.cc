#include <gtest/gtest.h>

#include <iostream>
#include <ostream>
#include <random>
#include <tuple>

#include "RtContext.h"
#include "android_util.h"
#include "approx_api.h"
#include "clblast_rt/tensor.h"
#include "clblast_rt/utility.h"
#include "common.h"
#include "constants.h"
#include "global_data.h"
#include "helpers.h"
#include "tensor_cpu_runtime.h"
#include "tensor_runtime.h"

using namespace clblast_rt;

const float eps = 1e-5;
const float eps_half = 1e-2;

Tensor *random(int n, int c, int h, int w) {
  Tensor *tensor = reinterpret_cast<Tensor *>(
      create4DTensor(TENSOR_TYPE_FLOAT, TENSOR_LAYOUT_NCHW, n, c, h, w));

  std::vector<float> random_data(n * c * h * w);

  std::default_random_engine rng(123);
  std::uniform_real_distribution<float> distr;

  for (auto &value : random_data) {
    value = distr(rng);
  }

  initTensorData(tensor, random_data.data(),
                 random_data.size() * sizeof(float));

  return tensor;
}

Tensor *clone(Tensor *tensor) {
  auto shape = tensor->shape();

  Tensor *out = reinterpret_cast<Tensor *>(
      create4DTensor(TENSOR_TYPE_FLOAT, TENSOR_LAYOUT_NCHW, shape.n(),
                     shape.c(), shape.h(), shape.w()));

  host2dev(tensor);
  host2dev(out);

  half2full(tensor);
  half2full(out);

  tensorCopy(tensor, out);

  return out;
}

TEST(CompareCPU, Clone) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  int n = 50;
  int c = 64;
  int h = 32;
  int w = 32;

  Tensor *x = random(n, c, h, w);

  Tensor *out = clone(x);

  is_close_float(*out, *x, 1e-16);

  freeBatchMemory();
}

TEST(CompareCPU, Add) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  int n = 50;
  int c = 64;
  int h = 32;
  int w = 32;

  Tensor *x = random(n, c, h, w);
  Tensor *bias = random(1, c, 1, 1);

  Tensor *out = reinterpret_cast<Tensor *>(tensorAdd(clone(x), bias));
  Tensor *out_cpu = reinterpret_cast<Tensor *>(tensorAddCPU(clone(x), bias));

  is_close_float(*out, *out_cpu, eps);

  freeBatchMemory();
}

TEST(CompareCPU, AddHalf) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  int n = 50;
  int c = 64;
  int h = 32;
  int w = 32;

  Tensor *x = random(n, c, h, w);
  Tensor *bias = random(1, c, 1, 1);

  Tensor *out_half = reinterpret_cast<Tensor *>(tensorHalfAdd(clone(x), bias));
  Tensor *out_cpu = reinterpret_cast<Tensor *>(tensorAddCPU(clone(x), bias));

  is_close_float(*out_half, *out_cpu, eps_half);

  freeBatchMemory();
}

TEST(CompareCPU, Relu) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  int n = 50;
  int c = 64;
  int h = 32;
  int w = 32;

  Tensor *x = random(n, c, h, w);

  Tensor *out = reinterpret_cast<Tensor *>(tensorRelu(clone(x)));
  Tensor *out_half = reinterpret_cast<Tensor *>(tensorHalfRelu(clone(x)));
  Tensor *out_cpu = reinterpret_cast<Tensor *>(tensorReluCPU(clone(x)));

  is_close_float(*out, *out_cpu, eps);
  is_close_float(*out_half, *out_cpu, eps_half);

  freeBatchMemory();
}

TEST(CompareCPU, Tanh) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  int n = 50;
  int c = 64;
  int h = 32;
  int w = 32;

  Tensor *x = random(n, c, h, w);

  Tensor *out = reinterpret_cast<Tensor *>(tensorTanh(clone(x)));
  Tensor *out_half = reinterpret_cast<Tensor *>(tensorHalfTanh(clone(x)));
  Tensor *out_cpu = reinterpret_cast<Tensor *>(tensorTanhCPU(clone(x)));

  is_close_float(*out, *out_cpu, eps);
  is_close_float(*out_half, *out_cpu, eps_half);

  freeBatchMemory();
}

TEST(CompareCPU, ClippedRelu) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  int n = 50;
  int c = 64;
  int h = 32;
  int w = 32;

  Tensor *x = random(n, c, h, w);

  float cmin = -0.123;
  float cmax = 0.321;

  Tensor *out = reinterpret_cast<Tensor *>(tensorRelu2(clone(x), cmin, cmax));
  Tensor *out_half =
      reinterpret_cast<Tensor *>(tensorHalfRelu2(clone(x), cmin, cmax));
  Tensor *out_cpu =
      reinterpret_cast<Tensor *>(tensorRelu2CPU(clone(x), cmin, cmax));

  is_close_float(*out_half, *out_cpu, eps_half);
  is_close_float(*out, *out_cpu, eps);

  freeBatchMemory();
}

TEST(CompareCPU, GemmGPU) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  int n = 25;
  int c = 32;
  int h = 16;
  int w = 16;

  Tensor *x = random(n, c, h, w);
  Tensor *weights = random(1, 1, h * w * c, 10);

  Tensor *out = reinterpret_cast<Tensor *>(tensorGemmGPU(x, weights));
  Tensor *out_cpu = reinterpret_cast<Tensor *>(tensorGemmCPU(x, weights));

  is_close_float(*out, *out_cpu, 1e-7);

  freeBatchMemory();
}

TEST(CompareCPU, GemmHalfGPU) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  int n = 25;
  int c = 32;
  int h = 16;
  int w = 16;

  Tensor *x = random(n, c, h, w);
  Tensor *weights = random(1, 1, h * w * c, 10);

  Tensor *out_half = reinterpret_cast<Tensor *>(tensorHalfGemmGPU(x, weights));
  Tensor *out_cpu = reinterpret_cast<Tensor *>(tensorGemmCPU(x, weights));

  is_close_float(*out_half, *out_cpu, eps_half);

  freeBatchMemory();
}

TEST(CompareCPU, BatchNorm) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  int n = 25;
  int c = 32;
  int h = 16;
  int w = 16;

  Tensor *x = random(n, c, h, w);
  Tensor *gamma = random(1, 32, 1, 1);
  Tensor *beta = random(1, 32, 1, 1);
  Tensor *mean = random(1, 32, 1, 1);
  Tensor *var = random(1, 32, 1, 1);
  double bn_eps = 0.1;

  Tensor *out = reinterpret_cast<Tensor *>(
      tensorBatchNorm(clone(x), gamma, beta, mean, var, bn_eps));
  Tensor *out_cpu = reinterpret_cast<Tensor *>(
      tensorBatchNormCPU(clone(x), gamma, beta, mean, var, bn_eps));

  is_close_float(*out, *out_cpu, eps);

  freeBatchMemory();
}

TEST(CompareCPU, BatchNormHalf) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  int n = 25;
  int c = 32;
  int h = 16;
  int w = 16;

  Tensor *x = random(n, c, h, w);
  Tensor *gamma = random(1, 32, 1, 1);
  Tensor *beta = random(1, 32, 1, 1);
  Tensor *mean = random(1, 32, 1, 1);
  Tensor *var = random(1, 32, 1, 1);
  double bn_eps = 0.1;

  Tensor *out_half = reinterpret_cast<Tensor *>(
      tensorHalfBatchNorm(clone(x), gamma, beta, mean, var, bn_eps));
  Tensor *out_cpu = reinterpret_cast<Tensor *>(
      tensorBatchNormCPU(clone(x), gamma, beta, mean, var, bn_eps));

  is_close_float(*out_half, *out_cpu, 1e-2);

  freeBatchMemory();
}

struct ConvApproxParams {
  int row, col, skip_every, offset;

  ConvApproxParams(int row, int col, int skip_every, int offset)
      : row(row), col(col), skip_every(skip_every), offset(offset) {}

  ConvApproxParams() : ConvApproxParams(1, 1, 1, 0) {}
};

std::ostream &operator<<(std::ostream &os, const ConvApproxParams &params) {
  return os << "row=" << params.row << " col=" << params.col
            << " skip_every=" << params.skip_every
            << " offset=" << params.offset;
}

namespace clblast_rt {
std::ostream &operator<<(std::ostream &os, Tensor &tensor) {
  half2full(&tensor);
  host2dev(&tensor);
  print("tensor", os, rtCtx->queue(), tensor.type(), tensor.shape(),
        tensor.buf_dev().raw(), 0, 4);
  return os;
}
} // namespace clblast_rt

class ConvApproxTest : public ::testing::TestWithParam<ConvApproxParams> {
protected:
  ConvApproxParams params;
};

TEST_P(ConvApproxTest, ConvApprox) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  int n = 16;
  int c = 16;
  int h = 16;
  int w = 16;

  int kn = 16;
  int kh = 3;
  int kw = 3;

  Tensor *x = random(n, c, h, w);
  Tensor *filter = random(kn, c, kh, kw);

  int pad_h = 1;
  int pad_w = 1;

  int stride_h = 1;
  int stride_w = 1;

  int row = GetParam().row;
  int col = GetParam().col;
  int skip_every = GetParam().skip_every;
  int offset = GetParam().offset;

  Tensor *out = reinterpret_cast<Tensor *>(
      tensorConvApprox(x, filter, pad_h, pad_w, stride_h, stride_w, 1, 1, row,
                       col, skip_every, offset));
  Tensor *out_cpu = reinterpret_cast<Tensor *>(
      tensorConvApproxCPU(x, filter, pad_h, pad_w, stride_h, stride_w, 1, 1,
                          row, col, skip_every, offset));

  EXPECT_PRED3(pred_tensor_eq, *out, *out_cpu, 1e-5);

  freeBatchMemory();
}

INSTANTIATE_TEST_SUITE_P(
    CompareCPU, ConvApproxTest,
    ::testing::Values(
        // Filter sampling
        ConvApproxParams(1, 1, 2, 0), ConvApproxParams(1, 1, 2, 1),
        // Row perforation
        ConvApproxParams(2, 1, 1, 0), ConvApproxParams(2, 1, 1, 1),
        ConvApproxParams(3, 1, 1, 0), ConvApproxParams(3, 1, 1, 1),
        ConvApproxParams(4, 1, 1, 3),
        // Col perforation
        ConvApproxParams(1, 2, 1, 0), ConvApproxParams(1, 2, 1, 1),
        ConvApproxParams(1, 3, 1, 0), ConvApproxParams(1, 3, 1, 1),
        ConvApproxParams(1, 4, 1, 3)));

class ConvApproxHalfTest : public ::testing::TestWithParam<ConvApproxParams> {
protected:
  ConvApproxParams params;
};

TEST_P(ConvApproxHalfTest, ConvApproxHalf) {
  android_util::ScopedTensorRtInit init;

  startMemTracking();

  int n = 16;
  int c = 16;
  int h = 16;
  int w = 16;

  int kn = 16;
  int kh = 3;
  int kw = 3;

  Tensor *x = random(n, c, h, w);
  Tensor *filter = random(kn, c, kh, kw);

  int pad_h = 1;
  int pad_w = 1;

  int stride_h = 1;
  int stride_w = 1;

  int row = GetParam().row;
  int col = GetParam().col;
  int skip_every = GetParam().skip_every;
  int offset = GetParam().offset;

  Tensor *out = reinterpret_cast<Tensor *>(
      tensorConvApproxHalf(x, filter, pad_h, pad_w, stride_h, stride_w, 1, 1,
                           row, col, skip_every, offset));
  Tensor *out_cpu = reinterpret_cast<Tensor *>(
      tensorConvApproxCPU(x, filter, pad_h, pad_w, stride_h, stride_w, 1, 1,
                          row, col, skip_every, offset));

  EXPECT_PRED3(pred_tensor_eq, *out, *out_cpu, 1e-2);

  freeBatchMemory();
}

INSTANTIATE_TEST_SUITE_P(
    CompareCPU, ConvApproxHalfTest,
    ::testing::Values(
        // Filter sampling
        ConvApproxParams(1, 1, 2, 0), ConvApproxParams(1, 1, 2, 1),
        // Row perforation
        ConvApproxParams(2, 1, 1, 0), ConvApproxParams(2, 1, 1, 1),
        ConvApproxParams(3, 1, 1, 0), ConvApproxParams(3, 1, 1, 1),
        // Col perforation
        ConvApproxParams(1, 2, 1, 0), ConvApproxParams(1, 2, 1, 1),
        ConvApproxParams(1, 3, 1, 0), ConvApproxParams(1, 3, 1, 1)));
