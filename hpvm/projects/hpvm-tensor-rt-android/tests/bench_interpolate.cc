#include <gtest/gtest.h>

#include <iostream>
#include <ostream>

#include "clblast.h"
#include "clblast_half.h"

#include "CL/cl.h"

#include "clblast_rt/utility.h"

#include "android_util.h"
#include "global_data.h"
#include "helpers.h"
#include "tensor_runtime.h"
#include "utilities/approxhpvm_util.hpp"

using namespace clblast_rt;

// Demonstrate some basic assertions.
TEST(Bench, Interpolate_0_2) {
  using namespace clblast;
  using T = float;
  using ApproxInfo = clblast::ApproxInfo;
  using ApproxMode = clblast::ApproxMode;

  android_util::ScopedTensorRtInit init;

  startMemTracking();

  cl_int status = CL_SUCCESS;

  auto w = 8;
  auto h = w;
  auto cn = 1024 * 64;

  ApproxInfo info{};
  info.mode = ApproxMode::PERF_ROW;
  info.perf_offset = 0;
  info.perf_stride = 2;

  auto in_w =
      info.mode == ApproxMode::PERF_COL
          ? approxhpvm_perf_out_size(w, info.perf_offset, info.perf_stride)
          : w;
  auto in_h =
      info.mode == ApproxMode::PERF_ROW
          ? approxhpvm_perf_out_size(h, info.perf_offset, info.perf_stride)
          : h;

  cl_mem in_d = clCreateBuffer(rtCtx->context(), CL_MEM_READ_WRITE,
                               in_w * in_h * cn * sizeof(T), nullptr, &status);
  clChk(status);
  cl_mem out_d = clCreateBuffer(rtCtx->context(), CL_MEM_READ_WRITE,
                                w * h * cn * sizeof(T), nullptr, &status);
  clChk(status);

  int n = 10;
  size_t total = 0;
  for (int i = 0; i < n; i++) {
    cl_event e;
    blastChk(clblast::ApproxInterpolate<T>(cn, h, w, in_d, 0, out_d, 0, info,
                                           &rtCtx->queue(), &e));
    clChk(clFinish(rtCtx->queue()));

    size_t start, end;
    clblast_rt::clChk(clGetEventProfilingInfo(e, CL_PROFILING_COMMAND_START,
                                              sizeof(size_t), &start, nullptr));
    clblast_rt::clChk(clGetEventProfilingInfo(e, CL_PROFILING_COMMAND_END,
                                              sizeof(size_t), &end, nullptr));

    total += (end - start);

    clReleaseEvent(e);
  }

  clReleaseMemObject(in_d);
  clReleaseMemObject(out_d);

  std::cout << (float)total / (float)n / 1e6 << "ms"
            << "\n";

  freeBatchMemory();
}