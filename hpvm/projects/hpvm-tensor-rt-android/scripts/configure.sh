#!/bin/bash
set -e

: ${ANDROID_NDK?"Use env-android.sh to set this variable."}
: ${ANDROID_API_LEVEL?"Use env-android.sh to set this variable."}
: ${ANDROID_ABI?"Use env-android.sh to set this variable."}
: ${ANDROID_APP_ROOT?"This should point to the root of https://github.com/MatevzFa/android-approxhpvm-demo"}

cmake \
    -DCMAKE_EXPORT_COMPILE_COMMANDS=YES \
    -DCMAKE_TOOLCHAIN_FILE=$ANDROID_NDK/build/cmake/android.toolchain.cmake \
    -DCMAKE_INSTALL_PREFIX="$ANDROID_APP_ROOT/app/src/main/cpp/bin/${ANDROID_ABI}" \
    -DANDROID_ABI=$ANDROID_ABI \
    -DANDROID_NATIVE_API_LEVEL=$ANDROID_API_LEVEL \
    -DBUILD_SHARED_LIBS="OFF" \
    -DANDROID_STL="c++_static" \
    -DTUNERS="OFF" -DTESTS="OFF" -DSAMPLES="OFF" \
    -DFMT_DEBUG_POSTFIX="" \
    "$@"
