#!/bin/bash
set -e

binary_host="$1"
binary_device=/data/local/tmp/$(basename "$binary_host")

shift
adb push "$binary_host" "$binary_device"
adb shell chmod +x "$binary_device"
adb shell sh -c "cd /data/local/tmp && $binary_device $@"
adb shell rm "$binary_device"
