// clang-format off
#include "clblast.h"
#include <CL/cl.h>
// clang-format on

#include "common.h"

#include "clblast_rt/op_convolution.h"
#include "clblast_rt/op_convolution_approx.h"
#include "clblast_rt/tensor.h"
#include "clblast_rt/utility.h"
#include "fmt/ostream.h"

#include <cstddef>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <ostream>
#include <type_traits>

using namespace benches;
using namespace clblast_rt;

size_t get_time_and_release(cl_event e) {
  if (e == nullptr) {
    return 0;
  }

  clWaitForEvents(1, &e);

  size_t start, end;
  clGetEventProfilingInfo(e, CL_PROFILING_COMMAND_START, sizeof(size_t), &start,
                          nullptr);
  clGetEventProfilingInfo(e, CL_PROFILING_COMMAND_END, sizeof(size_t), &end,
                          nullptr);

  clReleaseEvent(e);

  return end - start;
}

struct Info {
  size_t n;
  size_t c;
  size_t h;
  size_t w;
};

int main() {
  std::cout << "compare_convs" << std::endl;

  constexpr DataType dtype = DataType::FP16;

  CLContext ctx{};

  Info info{};
  {
    info.n = 32;
    info.c = 32;
    info.h = 16;
    info.w = info.h;
  }

  Tensor in(dtype, Shape(info.n, info.c, info.h, info.w));
  Tensor out(dtype, Shape(info.n, info.c, info.h, info.w));
  Tensor beta(dtype, Shape(1, info.c, 1, 1));
  Tensor gamma(dtype, Shape(1, info.c, 1, 1));
  Tensor mean(dtype, Shape(1, info.c, 1, 1));
  Tensor var(dtype, Shape(1, info.c, 1, 1));

  in.buf_dev().allocate(ctx.context());
  out.buf_dev().allocate(ctx.context());
  beta.buf_dev().allocate(ctx.context());
  gamma.buf_dev().allocate(ctx.context());
  mean.buf_dev().allocate(ctx.context());
  var.buf_dev().allocate(ctx.context());

  size_t total_ns = 0;
  for (size_t i = 0; i < 10; i++) {
    cl_event e;
    clblast::BatchNormalization<float>(
        in.shape().area(), info.c, in.shape().area() / info.n / info.c,
        in.buf_dev().raw(), 0, out.buf_dev().raw(), 0, mean.buf_dev().raw(), 0,
        var.buf_dev().raw(), 0, beta.buf_dev().raw(), 0, gamma.buf_dev().raw(),
        0, 1.0, &ctx.queue(), &e);

    total_ns += get_time_and_release(e);
  }

  in.buf_dev().release();
  out.buf_dev().release();
  beta.buf_dev().release();
  gamma.buf_dev().release();
  mean.buf_dev().release();
  var.buf_dev().release();

  {
    using namespace std;

    fmt::print("{:10} {:12}\n", "normal", total_ns);

    fmt::print("details\n");
    fmt::print(" {:10} {:12}\n", "batchnorm", total_ns);
  }

  return EXIT_SUCCESS;
}
