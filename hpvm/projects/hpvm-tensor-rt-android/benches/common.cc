#include "common.h"

// clang-format off
#include "clblast.h"
#include <CL/cl.h>
// clang-format on

#include "clblast_rt/utility.h"

using namespace clblast_rt;

benches::CLContext::CLContext() {
  cl_int status;

  clChk(clGetPlatformIDs(1, &_platform, nullptr));
  clChk(clGetDeviceIDs(_platform, CL_DEVICE_TYPE_GPU, 1, &_device, nullptr));

  cl_context_properties ctx_props[] = {
      CL_CONTEXT_PLATFORM,
      reinterpret_cast<cl_context_properties>(_platform),
      0,
  };
  _context = clCreateContext(ctx_props, 1, &_device, nullptr, nullptr, &status);
  clChk(status);

  _queue = clCreateCommandQueue(_context, _device, CL_QUEUE_PROFILING_ENABLE,
                                &status);
  clChk(status);
}

benches::CLContext::~CLContext() {
  clFlush(_queue);
  clFinish(_queue);

  clReleaseCommandQueue(_queue);
  clReleaseContext(_context);
}

cl_context &benches::CLContext::context() { return _context; }

cl_command_queue &benches::CLContext::queue() { return _queue; }
