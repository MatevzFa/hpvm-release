// clang-format off
#include "clblast.h"
#include <CL/cl.h>
// clang-format on

#include "common.h"

#include "clblast_rt/op_convolution.h"
#include "clblast_rt/op_convolution_approx.h"
#include "clblast_rt/tensor.h"
#include "clblast_rt/utility.h"
#include "fmt/core.h"
#include "fmt/ostream.h"

#include <cstdlib>
#include <functional>
#include <iomanip>
#include <ios>
#include <iostream>
#include <ostream>
#include <type_traits>

using namespace benches;
using namespace clblast_rt;

size_t get_time_and_release(cl_event e) {
  if (e == nullptr) {
    return 0;
  }

  clWaitForEvents(1, &e);

  size_t start, end;
  clGetEventProfilingInfo(e, CL_PROFILING_COMMAND_START, sizeof(size_t), &start,
                          nullptr);
  clGetEventProfilingInfo(e, CL_PROFILING_COMMAND_END, sizeof(size_t), &end,
                          nullptr);

  clReleaseEvent(e);

  return end - start;
}

struct NormalTime {
  size_t im2col;
  size_t gemm;

  NormalTime() : im2col(0), gemm(0) {}

  NormalTime(cl_event eim2col, cl_event egemm)
      : im2col(get_time_and_release(eim2col)),
        gemm(get_time_and_release(egemm)) {}

  NormalTime operator+(const NormalTime &other) {
    NormalTime out;

    out.im2col = im2col + other.im2col;
    out.gemm = gemm + other.gemm;

    return out;
  }

  size_t total() { return im2col + gemm; }
};

struct ApproxTime {
  size_t im2col;
  size_t kn2row;
  size_t gemm;
  size_t interpolate;

  ApproxTime() : im2col(0), kn2row(0), gemm(0), interpolate(0){};

  ApproxTime(cl_event eim2col, cl_event ekn2row, cl_event egemm,
             cl_event einterpolate)
      : im2col(get_time_and_release(eim2col)),
        kn2row(get_time_and_release(ekn2row)),
        gemm(get_time_and_release(egemm)),
        interpolate(get_time_and_release(einterpolate)) {}

  ApproxTime operator+(const ApproxTime &other) {
    ApproxTime out;

    out.im2col = im2col + other.im2col;
    out.kn2row = kn2row + other.kn2row;
    out.gemm = gemm + other.gemm;
    out.interpolate = interpolate + other.interpolate;

    return out;
  }

  size_t total() { return im2col + kn2row + gemm + interpolate; }
};

struct Info {
  size_t n;
  size_t c;
  size_t h;
  size_t w;
  size_t kn;
  size_t kh;
  size_t kw;

  size_t stride_h;
  size_t stride_w;

  size_t pad_h;
  size_t pad_w;
};

template <const DataType Dtype>
NormalTime bench_normal(size_t repeat, CLContext &ctx, Tensor &in,
                        Tensor &filter, Tensor &out, const Info &info) {
  using T = decltype(convolution);
  constexpr bool use_half = Dtype == DataType::FP16;

  std::function<T> conv = use_half ? convolution_half : convolution;

  size_t workspace_size;
  conv(ctx.queue(), in, filter, out, nullptr, info.stride_w, info.stride_h,
       info.pad_w, info.pad_h, &workspace_size, nullptr, nullptr);

  BufferDev workspace(workspace_size);
  workspace.allocate(ctx.context());

  // Warmup
  conv(ctx.queue(), in, filter, out, workspace.raw(), info.stride_w,
       info.stride_h, info.pad_w, info.pad_h, nullptr, nullptr, nullptr);

  NormalTime time;

  for (size_t i = 0; i < repeat; i++) {
    cl_event im2col = nullptr, gemm = nullptr;

    conv(ctx.queue(), in, filter, out, workspace.raw(), info.stride_w,
         info.stride_h, info.pad_w, info.pad_h, nullptr, &im2col, &gemm);

    clFlush(ctx.queue());
    clFinish(ctx.queue());
    time = time + NormalTime(im2col, gemm);
  }

  workspace.release();

  return time;
}

template <const DataType Dtype>
ApproxTime bench_approx(size_t repeat, CLContext &ctx, Tensor &in,
                        Tensor &filter, Tensor &out, const Info &info,
                        const ApproxInfo &approx_info) {
  using T = decltype(convolution_approx);
  constexpr bool use_half = Dtype == DataType::FP16;

  std::function<T> conv =
      use_half ? convolution_approx_half : convolution_approx;

  size_t workspace_size;
  conv(ctx.queue(), in, filter, out, nullptr, info.stride_w, info.stride_h,
       info.pad_w, info.pad_h, approx_info, &workspace_size, nullptr, nullptr,
       nullptr, nullptr);

  BufferDev workspace(workspace_size);
  workspace.allocate(ctx.context());

  // warmup
  conv(ctx.queue(), in, filter, out, workspace.raw(), info.stride_w,
       info.stride_h, info.pad_w, info.pad_h, approx_info, nullptr, nullptr,
       nullptr, nullptr, nullptr);

  ApproxTime time;

  for (size_t i = 0; i < repeat; i++) {

    cl_event kn2row = nullptr, im2col = nullptr, gemm = nullptr,
             interpolate = nullptr;

    conv(ctx.queue(), in, filter, out, workspace.raw(), info.stride_w,
         info.stride_h, info.pad_w, info.pad_h, approx_info, nullptr, &kn2row,
         &im2col, &gemm, &interpolate);
    clFlush(ctx.queue());
    clFinish(ctx.queue());

    time = time + ApproxTime(im2col, kn2row, gemm, interpolate);
  }

  workspace.release();

  return time;
}

void print_info_csv_header() {
  std::cout << //
      "info.n"
            << "," << //
      "info.c"
            << "," << //
      "info.h"
            << "," << //
      "info.w"
            << "," << //
      "info.kn"
            << "," << //
      "info.kh"
            << "," << //
      "info.kw"
            << "," << //
      "info.stride_h"
            << "," << //
      "info.stride_w"
            << "," << //
      "info.pad_h"
            << "," << //
      "info.pad_w"
            << "," << //
      "approx_info.mode"
            << "," << //
      "approx_info.perf_offset"
            << "," << //
      "approx_info.perf_stride"
            << ","
            <<

      "approx.gemm"
            << "," << //
      "approx.im2col"
            << "," << //
      "approx.interpolate"
            << "," << //
      "approx.kn2row"
            << "," << //

      "normal.gemm"
            << "," << //
      "normal.im2col" << std::endl;
}

void print_info_csv(const Info &info, const ApproxInfo &approx_info,
                    const NormalTime &normal, const ApproxTime &approx) {
  std::cout <<                                     //
      info.n << "," <<                             //
      info.c << "," <<                             //
      info.h << "," <<                             //
      info.w << "," <<                             //
      info.kn << "," <<                            //
      info.kh << "," <<                            //
      info.kw << "," <<                            //
      info.stride_h << "," <<                      //
      info.stride_w << "," <<                      //
      info.pad_h << "," <<                         //
      info.pad_w << "," <<                         //
      static_cast<int>(approx_info.mode) << "," << //
      approx_info.perf_offset << "," <<            //
      approx_info.perf_stride << "," <<

      approx.gemm << "," <<        //
      approx.im2col << "," <<      //
      approx.interpolate << "," << //
      approx.kn2row << "," <<      //

      normal.gemm << "," << //
      normal.im2col << std::endl;
}

void search_space() {
  print_info_csv_header();

  constexpr DataType dtype = DataType::FP16;

  CLContext ctx{};

  std::vector<size_t> ns = {32};
  std::vector<size_t> cs = {16, 32};
  std::vector<size_t> whs = {8, 16, 32};
  std::vector<size_t> kns = {8, 16, 32};

  size_t run = 0;

  for (auto n : {1, 16}) {
    for (auto c : {16, 32}) {
      for (auto h : {8, 16, 32}) {
        for (auto kn : {16, 32}) {
          for (auto kh : {1, 3, 5}) {
            Info info{};
            {
              info.n = n;
              info.c = c;
              info.h = h;
              info.w = info.h;
              info.kn = kn;
              info.kh = kh;
              info.kw = info.kh;

              info.stride_h = 1;
              info.stride_w = info.stride_h;

              info.pad_h = 1;
              info.pad_w = info.pad_h;
            }

            size_t out_h =
                conv_size(info.h, info.kh, 2 * info.pad_h, info.stride_h);
            size_t out_w =
                conv_size(info.w, info.kw, 2 * info.pad_w, info.stride_w);

            Tensor in(dtype, Shape(info.n, info.c, info.h, info.w));
            Tensor filter(dtype, Shape(info.kn, info.c, info.kh, info.kw));
            Tensor out(dtype, Shape(info.n, info.kn, out_h, out_w));

            if constexpr (dtype == DataType::FP32) {
              in.buf_dev().allocate(ctx.context());
              filter.buf_dev().allocate(ctx.context());
              out.buf_dev().allocate(ctx.context());
            } else {
              in.buf_dev_half().allocate(ctx.context());
              filter.buf_dev_half().allocate(ctx.context());
              out.buf_dev_half().allocate(ctx.context());
            }

            size_t repeat = 10;
            auto normal =
                bench_normal<dtype>(repeat, ctx, in, filter, out, info);

            for (auto perf_mode : {ApproxMode::PERF_COL, ApproxMode::PERF_ROW,
                                   ApproxMode::SAMP_KERNEL}) {
              for (auto perf_offset : {0, 1, 2}) {
                for (auto perf_stride : {2, 3, 4}) {

                  ApproxInfo approx_info(perf_mode, perf_offset, perf_stride);
                  auto approx = bench_approx<dtype>(repeat, ctx, in, filter,
                                                    out, info, approx_info);
                  run++;

                  print_info_csv(info, approx_info, normal, approx);
                }
              }
            }

            if constexpr (dtype == DataType::FP32) {
              in.buf_dev().release();
              filter.buf_dev().release();
              out.buf_dev().release();
            } else {
              in.buf_dev_half().release();
              filter.buf_dev_half().release();
              out.buf_dev_half().release();
            }
          }
        }
      }
    }
  }
}

int main(int argc, char **argv) {

  if (argc >= 2 && std::string(argv[1]) == "search") {
    search_space();
    return EXIT_SUCCESS;
  }

  std::cout << "compare_convs" << std::endl;

  constexpr DataType dtype = DataType::FP16;

  CLContext ctx{};

  Info info{};
  {
    info.n = 32;
    info.c = 32;
    info.h = 16;
    info.w = info.h;
    info.kn = 32;
    info.kh = 3;
    info.kw = info.kh;

    info.stride_h = 1;
    info.stride_w = info.stride_h;

    info.pad_h = 1;
    info.pad_w = info.pad_h;
  }

  ApproxMode perf_mode = ApproxMode::PERF_ROW;
  size_t perf_offset = 0;
  size_t perf_stride = 3;

  ApproxInfo approx_info(perf_mode, perf_offset, perf_stride);

  size_t out_h = conv_size(info.h, info.kh, 2 * info.pad_h, info.stride_h);
  size_t out_w = conv_size(info.w, info.kw, 2 * info.pad_w, info.stride_w);

  Tensor in(dtype, Shape(info.n, info.c, info.h, info.w));
  Tensor filter(dtype, Shape(info.kn, info.c, info.kh, info.kw));
  Tensor out(dtype, Shape(info.n, info.kn, out_h, out_w));

  if constexpr (dtype == DataType::FP32) {
    in.buf_dev().allocate(ctx.context());
    filter.buf_dev().allocate(ctx.context());
    out.buf_dev().allocate(ctx.context());
  } else {
    in.buf_dev_half().allocate(ctx.context());
    filter.buf_dev_half().allocate(ctx.context());
    out.buf_dev_half().allocate(ctx.context());
  }

  size_t repeat = 10;
  auto normal = bench_normal<dtype>(repeat, ctx, in, filter, out, info);
  auto approx =
      bench_approx<dtype>(repeat, ctx, in, filter, out, info, approx_info);

  if constexpr (dtype == DataType::FP32) {
    in.buf_dev().release();
    filter.buf_dev().release();
    out.buf_dev().release();
  } else {
    in.buf_dev_half().release();
    filter.buf_dev_half().release();
    out.buf_dev_half().release();
  }

  {
    using namespace std;

    fmt::print("{:10} {:12}\n", "normal", normal.total());
    fmt::print("{:10} {:12}\n", "approx", approx.total());
    fmt::print("{:10} {:12}\n", "speedup",
               (float)normal.total() / (float)approx.total());

    fmt::print("details\n");
    fmt::print(" normal\n");
    fmt::print("  {:10} {:12}\n", "im2col", normal.im2col);
    fmt::print("  {:10} {:12}\n", "gemm", normal.gemm);
    fmt::print(" approx\n");
    fmt::print("  {:10} {:12}\n", "im2col", approx.im2col);
    fmt::print("  {:10} {:12}\n", "kn2row", approx.kn2row);
    fmt::print("  {:10} {:12}\n", "gemm", approx.gemm);
    fmt::print("  {:10} {:12}\n", "interp", approx.interpolate);
  }

  return EXIT_SUCCESS;
}
