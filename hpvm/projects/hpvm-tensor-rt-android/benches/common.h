#pragma once

#include <exception>
#include <sstream>

// clang-format off
#include "clblast.h"
#include <CL/cl.h>
// clang-format on

namespace benches {

class CLContext {
public:
  CLContext();
  ~CLContext();

  cl_context &context();

  cl_command_queue &queue();

private:
  cl_platform_id _platform;
  cl_device_id _device;
  cl_context _context;
  cl_command_queue _queue;
};

} // namespace benches
