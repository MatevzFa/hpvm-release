#!/bin/bash
set -e

: ${ANDROID_NDK?"Use env-android.sh to set this variable."}
: ${ANDROID_TARGET_TRIPLE?"Use env-android.sh to set this variable."}
: ${ANDROID_API_LEVEL?"Use env-android.sh to set this variable."}
: ${ANDROID_HPVM_RT_PATH?"Use env-android.sh to set this variable."}

ANDROID_NDK_LLVM_BIN="$ANDROID_NDK/toolchains/llvm/prebuilt/linux-x86_64/bin"

# If you want to specify additional CMake arguments (prefixed with "D"), you have to specify them
# before any other arguments that are forwarded to ./install.sh.
# For example:
# ./install-android.sh DCMAKE_BUILD_TYPE="MinSizeRel" --no-params -j8 --ninja ...
./install.sh \
    DXANDROID_HPVM_RT_PATH=$ANDROID_HPVM_RT_PATH \
    DXANDROID_NDK_LLVM_BIN=$ANDROID_NDK_LLVM_BIN \
    DXANDROID_TARGET_TRIPLE=$ANDROID_TARGET_TRIPLE \
    DXANDROID_API_LEVEL=$ANDROID_API_LEVEL \
    "$@"

# Install hpvm-profiler-android
(cd projects/hpvm-profiler-android && pip install -e .)
