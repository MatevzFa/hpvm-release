# Path to the Android NDK revision r21e (equally 21.4.7075529 in Android Studio)
# You can download it from https://github.com/android/ndk/wiki/Unsupported-Downloads#r21e
# or via Android Studio's SDK manager.
export ANDROID_NDK="$HOME/Android/Sdk/ndk/21.4.7075529"

# The LLVM target triple to use when building.
# For more information see https://developer.android.com/ndk/guides/other_build_systems
export ANDROID_TARGET_TRIPLE="aarch64-linux-android"

# The Android ABI. Required for building hpvm-tensor-rt-android.
# For more information see https://developer.android.com/ndk/guides/other_build_systems
export ANDROID_ABI="arm64-v8a"

# The minSdkVersion from your Android project
export ANDROID_API_LEVEL="26"

# See https://github.com/MatevzFa/hpvm-rt-android
export ANDROID_HPVM_RT_PATH="path/to/hpvm-rt-android/build.aarch64-linux-android26/hpvm-rt.bc"

# Path to the build directory where the hvpm-tensor-rt-android is built.
# WARNING: This variable has no effect during the build of ApproxHPVM.
#          However, it has to be set when invoking hpvm-clang to build a binary.
export ANDROID_HPVM_TENSOR_RT_BUILD_DIR="this/directory/hpvm/projects/hpvm-tensor-rt-android/build"
